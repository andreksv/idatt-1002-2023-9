**HOW TO RUN BUDGET BUDDY**

You can follow a guide an in depth guide in the "Installation Guide" on the applications WIKI or continue reading this README.

Make sure you have oracle java sdk17 and javafx installed! The version of JavaFX should preferably be 17 or 19.
After installing the two continue this README.

Java download link: https://www.oracle.com/cis/java/technologies/downloads/

JavaFX download link: https://openjfx.io


**INSTALLING THE .JAR FILE**

The BudgetBuddy application is run from a .jar file. You can download this file by clicking the "Download File" folder,
in the GitLab root folder, then clicking the "budgetbuddy.jar" file. 
Lastly, click on "Download". The file should be downloaded to your local "Downloads" folder.
Once the download is completed, try clicking or double clicking the icon. This should start up the application. If nothing
happens, follow the steps in SETUP.

By default, the application runs without any recipes. If you wish to add recipes, head to the bottom of this file
to the section named ADDING RECIPES



**SETUP**

1. Start by finding your javafx folder. It is often under "C:\Program Files\Java\sdk-19\" on PC and "Users/name/Library/Java/javafx-sdk-19\" on MAC.
    Library or Program Files are often hidden. You can fix this by pressing (VIEW -> Show -> Hidden Items) on PC and (cmd + shift + . ) on MAC
2. Find the "lib" folder in javafx folder and then copy its path.
    It should look like this: C:\Program Files\Java\sdk-19\lib on WINDOWS and /Users/name/Library/Java/javafx-sdk-19.0.2.1/lib on MAC
    As a good measure you should put " at the start and end of the path.
    It should now look something like this "C:\Program Files\Java\sdk-19\lib" or "/Users/name/Library/Java/javafx-sdk-19.0.2.1/lib". 
    A real example looks like this: Users/emil/Library/Java/javafx-sdk-19.0.2.1/lib".
    Copy this finished path and replace the placeholder text below labelled "REPLACE THIS WITH JAVAFX LIB PATH"
3. After replacing the placeholder text below you will be left with your execute text. 
    Copy the text and paste it into the terminal each time you start the app.

EXECUTE LINE:

COPY THIS: java --module-path REPLACE THIS WITH JAVAFX LIB PATH --add-modules=javafx.controls,javafx.fxml -jar idatt-1002-2023-9.jar



**HOW TO RUN THE APP**

1. Start by going into your terminal of choice. Preferably in administrator.
2. Navigate to the place you saved the JAR file by writing "cd" + the path to the jar
    If you saved the jarfile to your desktop this input should be "cd Desktop" on PC and "cd Desktop" on MAC
3. After the terminal shows the desktop in its path name you simply copy your execute line and press enter.
4. The file should now run.



**ADDING RECIPES**

As stated above, the default version of the BudgetBuddy application runs without recipes. In order to add recipes to your
application, head back to the “DownloadFile” folder on GitLab 
and download the “Recipes.register” file. Go
to “C:\users\username\BudgetBuddyFiles\recipes” and replace the current
“Recipes.register” file with the one you just downloaded.
Restart BudgetBuddy and you will now have recipes.
