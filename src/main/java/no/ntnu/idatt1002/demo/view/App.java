package no.ntnu.idatt1002.demo.view;

/**
 * The class from which the main application is called upon.
 *
 * @author Harry Linrui Xu
 * @since 11.03.2023
 */
public class App {

  /**
   * The main method that calls on the MyApp class.
   *
   * @param args String of arguments
   */
  public static void main(String[] args) {
    MyApp.main(args);
  }
}