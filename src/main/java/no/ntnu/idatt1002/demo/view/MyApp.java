package no.ntnu.idatt1002.demo.view;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This is the class that the main application is run from. When a user decides to run the
 * application, this class calls on the start method which starts the application.
 */
public class MyApp extends Application {

  /**
   * The start method which is where the application is opened.
   *
   * @param stage The stage or window at which the GUI is displayed.
   * @throws IOException If an occurs loading the first view.
   */
  @Override
  public void start(Stage stage) throws IOException {
    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FirstMenu.fxml"));
    Parent root = loader.load();
    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.setResizable(false);
    stage.show();
  }

  /**
   * The main method that calls on the start method.
   *
   * @param args String of arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }
}
