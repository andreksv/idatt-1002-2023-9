package no.ntnu.idatt1002.demo.data.economics;

import java.time.LocalDate;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The Item class represents a good or service purchased in real life. The item belongs to a
 * category and has an amount, description and a date. The description may be left blank, but the
 * Item must belong to a category and must have a price.
 *
 * @author HanneSofie
 * @since 02.03.2023
 */
public abstract class Item {

  /**
   * String property for description. Allows for binding.
   */
  private StringProperty description = new SimpleStringProperty("");

  /**
   * Double property for amount. Allows for binding.
   */
  private final DoubleProperty amount;

  /**
   * Boolean property for recurring. Allows for binding.
   */
  private final BooleanProperty recurring;

  /**
   * ObjectProperty property for date. Allows for binding.
   */
  private final ObjectProperty<LocalDate> date;      // Format example: 09.08.23

  /**
   * The constructor of a new Item object takes in an amount as a double. If the amount is left
   * blank, an IllegalArgumentException is thrown.
   *
   * @param amount    price of an Item as a float.
   * @param recurring Value informing if the item is recurring or not.
   * @param date      The date the item is created.
   */
  public Item(double amount, boolean recurring, LocalDate date) {
    if (amount <= 1.0f || date.toString().isBlank()) {
      throw new IllegalArgumentException("Both amount and date must be provided.");
    } else {
      this.amount = new SimpleDoubleProperty(amount);
      this.recurring = new SimpleBooleanProperty(recurring);
      this.date = new SimpleObjectProperty<>(date);
    }
  }

  /**
   * The constructor instantiates a new Item object with a category, a description and a price as
   * arguments. It overloads the first constructor to set the category and price and then sets the
   * description of the item. If either 0category or price is left blank, an
   * IllegalArgumentException is thrown.
   *
   * @param description A description of the item as a String.
   * @param amount      The price of the item as a float.
   * @param recurring   Value informing if the item is recurring or not.
   * @param date        The date the item is created.*
   */
  public Item(String description, double amount, boolean recurring, LocalDate date) {
    this(amount, recurring, date);
    this.description = new SimpleStringProperty(description);
  }

  /**
   * Gets the String Property of the description.
   *
   * @return String property of description.
   */
  public StringProperty descriptionProperty() {
    return this.description;
  }

  /**
   * The method returns the description of the given Item object as a String.
   *
   * @return The description of the Item as a String.
   */
  public String getDescription() {
    return description.get();
  }


  /**
   * The method sets the description of the Item object equal to the passed String. The String may
   * be empty.
   *
   * @param description The new description of the Item object as a String.
   */
  public void setDescription(String description) {
    this.description.set(description);
  }

  /**
   * Gets the Double Property of the amount.
   *
   * @return Double Property of amount.
   */
  public DoubleProperty amountProperty() {
    return this.amount;
  }

  /**
   * The method returns the price of the given Item object as a float.
   *
   * @return The amount of the Item as a float.
   */
  public double getAmount() {
    return amount.get();
  }

  /**
   * The method changes the price of the given Item to the passed price as a float.
   *
   * @param amount The new price of the Item as a float.
   */
  public void setAmount(double amount) {
    if (amount <= 1.0f) {
      throw new IllegalArgumentException("A positive amount must be provided");
    }
    this.amount.set(amount);
  }

  /**
   * The method returns the BooleanProperty of recurring.
   *
   * @return BooleanProperty whose value is either true or false.
   */
  public BooleanProperty recurringProperty() {
    return this.recurring;
  }

  /**
   * The method returns true if the transaction is recurring, false otherwise.
   *
   * @return True if the transaction is a recurring one, false otherwise.
   */
  public boolean isRecurring() {
    return recurring.get();
  }

  /**
   * The method changes the boolean value of the 'recurring' field of the Item object.
   *
   * @param recurring A boolean value being true if the Item is recurring and false otherwise.
   */
  public void setRecurring(boolean recurring) {
    this.recurring.set(recurring);
  }

  /**
   * The method returns the ObjectProperty of date.
   *
   * @return The date property.
   */
  public ObjectProperty<LocalDate> dateProperty() {
    return this.date;
  }

  /**
   * The method returns the date of the item object (income/expense).
   *
   * @return The date of the transaction.
   */
  public LocalDate getDate() {
    return this.date.get();
  }

  /**
   * Sets the date field of the Item object to a new LocalDate provided as an argument.
   *
   * @param newDate The new date with which to record the Item.
   */
  public void setDate(LocalDate newDate) {
    this.date.set(newDate);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Item item)) {
      return false;
    }
    return Double.compare(item.amount.get(), amount.get()) == 0
        && item.recurring.get() == recurring.get() && Objects.equals(description.get(),
        item.description.get()) && Objects.equals(date.get(), item.date.get());
  }

  @Override
  public int hashCode() {
    return Objects.hash(description, amount, recurring, date);
  }

  /**
   * Returns a String that represents the Item.
   *
   * @return The Item as a String.
   */
  @Override
  public String toString() {
    String isRecurring;
    if (recurring.get()) {
      isRecurring = "Recurring";
    } else {
      isRecurring = "Not recurring";
    }
    if (!description.get().isBlank()) {
      return "date=" + date.get() + "\ndescription=" + description.get() + "\namount="
          + amount.get() + "\nisRecurring=" + isRecurring + "\n";
    } else {
      return "date=" + date.get() + "\namount=" + amount.get() + "\nisRecurring=" + isRecurring
          + "\n";
    }
  }
}


