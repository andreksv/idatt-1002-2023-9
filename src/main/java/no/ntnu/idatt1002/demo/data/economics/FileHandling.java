package no.ntnu.idatt1002.demo.data.economics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;


/**
 * FileHandling is a class for writing and reading ItemRegister-objects to and from a file.
 *
 * @author andreas
 */
public class FileHandling {

  private static final String jarFilePath = "src/main/resources/";

  private static final String path = System.getProperty("user.home");
  private static final String filePath = path + "/BudgetBuddyFiles/";

  private static final String fileType = ".register";
  private static final String date = "date=";
  private static final String description = "description=";
  private static final String amount = "amount=";
  private static final String isRecurring = "isRecurring=";
  private static final String category = "category=";

  /**
   * Method for writing (adding) an ItemRegister to a file.
   *
   * @param <T>          Subclass of item, either expense or income.
   * @param itemRegister the ItemRegister you want to write to a file.
   * @param fileDestination    the path and name of the file you want to check
   * @throws IOException if an input or output exception occurred.
   */

  public static <T extends Item> void writeItemRegisterToFile(final ItemRegister<T> itemRegister,
      String fileDestination) throws IOException {
    try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileDestination + fileType))) {
      if (itemRegister.isEmpty()) {
        bw.write("");
      } else {
        bw.write(itemRegister.toString());
      }
    } catch (IOException ex) {
      throw new IOException("Error writing story to file: " + ex.getMessage());
    }
  }

  /**
   * Method for checking if a .register file is empty.
   *
   * @param fileDestination the path and name of the file you want to check
   * @return true or false depending on if file is empty.
   * @throws IOException if an input or output exception occurred.
   */
  public static boolean isEmpty(String fileDestination) throws IOException {
    try (BufferedReader br = new BufferedReader(new FileReader(fileDestination + fileType))) {
      return br.readLine() == null;
    }
  }

  /**
   * Method for reading (getting) an IncomeRegister from a file.
   *
   * @param fileDestination the name of the file you want to read from.
   * @return the IncomeRegister from the file.
   * @throws IOException if an input or output exception occurred.
   */
  public static IncomeRegister readIncomeRegisterFromFile(String fileDestination)
      throws IOException {
    IncomeRegister incomeRegister = new IncomeRegister();
    LocalDate date = null;
    String description = "";
    double amount = 0;
    boolean reoccuring = false;
    IncomeCategory incomeCategory = null;
    try (BufferedReader br = new BufferedReader(new FileReader(fileDestination + fileType))) {
      String line;
      String nextLine = br.readLine();
      while ((line = nextLine) != null) {
        nextLine = br.readLine();
        if (line.startsWith(FileHandling.date)) {
          date = LocalDate.parse(line.replace(FileHandling.date, ""));
        } else if (line.startsWith(FileHandling.description)) {
          description = line.replace(FileHandling.description, "");
        } else if (line.startsWith(FileHandling.amount)) {
          amount = Double.parseDouble(line.replace(FileHandling.amount, ""));
        } else if (line.startsWith(FileHandling.isRecurring)) {
          reoccuring = line.replace(FileHandling.isRecurring, "").equals("Recurring");
        } else if (line.startsWith(FileHandling.category)) {
          line = line.replace(FileHandling.category, "");
          incomeCategory = switch (line) {
            case "GIFT" -> IncomeCategory.GIFT;
            case "STUDENT_LOAN" -> IncomeCategory.STUDENT_LOAN;
            default -> IncomeCategory.SALARY;
          };
        }
        if (line.isEmpty() || (nextLine == null)) {
          if (description.equals("")) {
            incomeRegister.addItem(new Income(amount, reoccuring, incomeCategory, date));
          } else {
            incomeRegister.addItem(
                new Income(description, amount, reoccuring, incomeCategory, date));
          }
          description = "";
        }
      }
    }
    return incomeRegister;
  }

  /**
   * Method for reading (getting) an ExpenseRegister from a file.
   *
   * @param fileDestination the name of the file you want to read from.
   * @return the ExpenseRegister from the file.
   * @throws IOException if an input or output exception occurred
   */
  public static ExpenseRegister readExpenseRegisterFromFile(String fileDestination)
      throws IOException {
    ExpenseRegister expenseRegister = new ExpenseRegister();
    LocalDate date = null;
    String description = "";
    double amount = 0;
    boolean reoccuring = false;
    ExpenseCategory expenseCategory = null;
    try (BufferedReader br = new BufferedReader(new FileReader(fileDestination + fileType))) {
      String line;
      String nextLine = br.readLine();
      while ((line = nextLine) != null) {
        nextLine = br.readLine();
        if (line.startsWith(FileHandling.date)) {
          date = LocalDate.parse(line.replace(FileHandling.date, ""));
        } else if (line.startsWith(FileHandling.description)) {
          description = line.replace(FileHandling.description, "");
        } else if (line.startsWith(FileHandling.amount)) {
          amount = Double.parseDouble(line.replace(FileHandling.amount, ""));
        } else if (line.startsWith(FileHandling.isRecurring)) {
          reoccuring = line.replace(FileHandling.isRecurring, "").equals("Recurring");
        } else if (line.startsWith(FileHandling.category)) {
          line = line.replace(FileHandling.category, "");
          expenseCategory = switch (line) {
            case "FOOD" -> ExpenseCategory.FOOD;
            case "RENT" -> ExpenseCategory.RENT;
            case "CLOTHES" -> ExpenseCategory.CLOTHES;
            case "BOOKS" -> ExpenseCategory.BOOKS;
            case "HEALTH" -> ExpenseCategory.HEALTH;
            case "ACTIVITIES" -> ExpenseCategory.ACTIVITIES;
            default -> ExpenseCategory.OTHER;
          };
        }
        if (line.isEmpty() || (nextLine == null)) {
          if (description.equals("")) {
            expenseRegister.addItem(new Expense(amount, reoccuring, expenseCategory, date));
          } else {
            expenseRegister.addItem(
                new Expense(description, amount, reoccuring, expenseCategory, date));
          }
          description = "";
        }
      }
    }
    return expenseRegister;
  }
}
