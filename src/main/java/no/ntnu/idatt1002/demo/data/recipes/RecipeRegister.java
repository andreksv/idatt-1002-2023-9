package no.ntnu.idatt1002.demo.data.recipes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * The RecipeRegister class holds a collection of Recipes in a list and provides methods to access
 * the recipes, add recipes to the register as well as to pick a number of recipe's best fitting a
 * list of food types provided as a IngredientsAtHand object. The register is instantiated using the
 * default constructor.
 *
 * @author hannesofie
 */
public class RecipeRegister {

  private final ArrayList<Recipe> recipes = new ArrayList<>();


  /**
   * The method returns the list of recipes in the Register.
   *
   * @return A list of recipes that belong to this register.
   */
  public ArrayList<Recipe> getRecipes() {
    return this.recipes;
  }

  /**
   * The method takes in a Recipe object. If the recipe is already in the register, the old recipe
   * will be replaced. If the recipe is null, an IllegalArgumentException is thrown. Otherwise, the
   * new recipe is added to the register's collection of recipes.
   *
   * @param recipe A new recipe to add to the register.
   */
  public void addRecipe(Recipe recipe) {
    if (this.recipes.contains(recipe)) {
      recipes.remove(getRecipe(recipe.getName()));
      recipes.add(recipe);
    } else if (recipe == null) {
      throw new NullPointerException("The recipe cannot be null.");
    } else {
      this.recipes.add(recipe);
    }
  }


  /**
   * The method takes in a string with the name of a recipe and returns that recipe object if it is
   * in the recipe register. If it is not found, null is returned instead.
   *
   * @param name The name of a recipe as a String.
   * @return The recipe matching the provided name if it is present in the recipe register.
   */
  public Recipe getRecipe(String name) {
    return recipes.stream()
        .filter((recipe) -> recipe.getName().equals(name))
        .findFirst().orElse(null);
  }

  /**
   * The method takes in a number and an IngredientsAtHand object and returns an ArrayList
   * containing that number of recipes that, based on the ingredients at hand, require the fewest
   * extra ingredients apart from the ones at hand. If the number of recipes in the register is
   * fewer than the provided number, the list will contain as many as the register can provide. The
   * resulting list is sorted by the number of ingredients required apart from those ingredients at
   * hand. If zero recipes are requested, and empty ArrayList is returned.
   *
   * @param number Number of recipes to pick from the recipe register.
   * @param atHand A IngredientsAtHand object providing a collection of the food types at hand to
   *               the user.
   * @return A list of recipes of sorted by available ingredients and limited to the provided number
   *         or the number of recipes in the register.
   */
  public ArrayList<Recipe> pickBestFits(int number, IngredientsAtHand atHand) {
    if (number == 0) {
      return new ArrayList<>();
    }
    ArrayList<Recipe> recipes;
    this.recipes.forEach(r -> r.updateIngredientStatus(atHand));

    recipes = this.recipes.stream()
        .sorted(Comparator.comparingInt(Recipe::getMissingIngredients))
        .limit(number).collect(Collectors.toCollection(ArrayList::new));
    return recipes;
  }

}
