package no.ntnu.idatt1002.demo.data.recipes;

/**
 * The RecipeIngredient class is an extension of the Ingredient class  used as part of recipes that
 * also stored the boolean value 'atHand'. This value can be set to true whenever the given
 * ingredient, being part of a recipe, is also at hand to the user. Ingredients that the user has
 * available are stored in the IngredientsAtHand class.
 *
 * @author hannesofie
 */
public class RecipeIngredient extends Ingredient {

  private boolean atHand = false;

  /**
   * The constructor of a RecipeIngredient object inherits from the superclass Ingredient. The
   * additional 'atHand' field is set to false ar default.
   *
   * @param ingredient What type of food the ingredient is.
   * @param amount     The amount of the ingredient.
   * @param unit       The unit of measure of the given amount of the ingredient.
   */
  public RecipeIngredient(FoodItem ingredient, double amount, MeasuringUnit unit) {
    super(ingredient, amount, unit);
  }

  /**
   * The method returns the boolean value of the atHand field for the ingredient. If the ingredient
   * being part of a recipe is also contained in the collection of ingredients at hand to the user,
   * true is returned, if not, false is returned.
   *
   * @return True of the current recipe ingredient is available to the user.
   */
  public boolean isAtHand() {
    return atHand;
  }

  /**
   * The method sets the value of the atHand field for the ingredient to either true or false.
   *
   * @param atHand A boolean value to set the 'atHand' status of the ingredient.
   */
  public void setAtHand(boolean atHand) {
    this.atHand = atHand;
  }

  /**
   * The method returns a String representation of a Recipe Ingredient object, listing its type,
   * amount, unit and whether it is at hand or not.
   *
   * @return A String representation of the recipe ingredient object.
   */
  @Override
  public String toString() {
    return "Ingredient{"
        + "foodType=" + this.getFoodType().label
        + ", amount=" + this.getAmount()
        + ", unit=" + this.getUnit().label
        + ", at hand=" + this.isAtHand()
        + '}';
  }
}
