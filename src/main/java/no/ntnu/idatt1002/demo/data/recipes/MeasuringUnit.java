package no.ntnu.idatt1002.demo.data.recipes;

/**
 * The enum class defines a set of valid units of measurements related to the ingredients and
 * recipes. The label property of each constant provides a lower-case representation of each unit
 * that can be used in presentation to the user.
 *
 * @author hannesofie
 * @since 13.03.2023
 */

public enum MeasuringUnit {

  /**
   * Deciliter.
   */
  DL("dl."),

  /**
   * Liter.
   */
  L("l."),

  /**
   * Teaspoon.
   */
  TSP("tsp."),

  /**
   * Tablespoon.
   */
  TBS("tbs."),

  /**
   * Gram.
   */
  GR("gr."),

  /**
   * Kilogram.
   */
  KG("kg."),

  /**
   * Pieces.
   */
  PC("pieces"),

  /**
   * Can.
   */
  CAN("can"),

  /**
   * Slice.
   */
  SLICE("slice"),

  /**
   * Pack.
   */
  PKG("pack"),

  /**
   * Cubes.
   */
  CB("cubes");

  /**
   * Label of the categories. The label formats the string value of the categories two string in
   * lower caps.
   */
  public final String label;

  /**
   * The constructor of each enum constant takes in a string representation of the constant that is
   * more suited for presentation at the frontend.
   *
   * @param label Lower case representation of the unit of measure.
   */
  MeasuringUnit(String label) {
    this.label = label;
  }
}
