package no.ntnu.idatt1002.demo.data.budget;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class that holds the responsibility of any action relating to the SelectedBudget.current file
 * which holds the budget name of the budget that currently is viewed.
 *
 * @author Harry Linrui Xu
 * @since 19.04.2023
 */
public class FileHandlingSelectedBudget {

  private static final String selectedBudgetFileType = ".current";
  private static final String registerFileType = ".register";
  private static final String budgetFileType = ".budget";

  /**
   * Reads the name of the currently selected budget.
   *
   * @param fileDestination The file destination in resources.
   * @return A string of the budget name.
   * @throws IOException if an input or output exception occurred.
   */
  public static String readSelectedBudget(String fileDestination) throws IOException {

    try (BufferedReader br = new BufferedReader(new FileReader(fileDestination
        + selectedBudgetFileType))) {
      return br.readLine();
    } catch (IOException ioException) {
      throw new IOException("File: SelectedBudget.current does not exist", ioException);
    }
  }

  /**
   * Writes the currently used budget name to file, essentially updating the file.
   *
   * @param budgetName      The name of the budget.
   * @param fileDestination File destination in resources.
   * @throws IOException if an input or output exception occurred.
   */
  public static void updateSelectedBudget(String budgetName, String fileDestination)
      throws IOException {

    try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileDestination
        + selectedBudgetFileType))) {
      bw.write(budgetName);
    } catch (IOException ex) {
      throw new IOException("Error writing to file: " + "SelectedBudget.current");
    }
  }

  /**
   * Clears the entire file that holds the current budget. In case a budget is deleted, this method
   * prevents the file of containing a budget name that no longer exists.
   *
   * @param fileDestination The title of the file.
   * @throws IOException if an input or output exception occurred.
   */
  public static void clearSelectedBudget(String fileDestination) throws IOException {
    try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileDestination
        + selectedBudgetFileType))) {
      bw.write("");
    } catch (IOException ex) {
      throw new IOException("Error writing to file: " + "SelectedBudget.current");
    }
  }

  /**
   * Checks if the file that holds the selected budget contains any budget. In other words, the
   * method checks if this file is empty.
   *
   * @param fileDestination The title of the file.
   * @return True, if only "null" is read from the file. Else, returns false.
   * @throws IOException if an input or output exception occurred.
   */
  public static boolean isSelectedBudgetEmpty(String fileDestination) throws IOException {
    try (BufferedReader br = new BufferedReader(
        new FileReader(fileDestination + selectedBudgetFileType))) {
      return br.readLine() == null;
    }
  }

  /**
   * Method for creating a new directory for a newly created budget.
   *
   * @param budgetId The name of the directory that stores all the data for a given budget.
   * @return True, if a directory is successfully created. Else, returns false.
   */
  public static boolean createBudgetDirectory(String budgetId) {
    File f = new File(budgetId);
    return f.mkdirs();
  }

  /**
   * Method for deleting a budget directory which holds all budget, income and expense data for a
   * particular budget. This class holds this responsibility, as any budgets that should be removed
   * should have their directory deleted.
   *
   * @param budgetDestination The destination of the budget directory that holds all the data for a
   *                          given budget.
   * @return True, if the directory is successfully deleted. Else, returns false.
   */
  public static boolean deleteBudgetDirectory(String budgetDestination) {
    File targetDirectory = new File(budgetDestination);

    String[] entries = targetDirectory.list();
    assert entries != null;
    for (String file : entries) {
      File currentFile = new File(targetDirectory.getPath(), file);
      currentFile.delete();
    }
    return targetDirectory.delete();
  }

  /**
   * Method for creating a file holding all income data.
   *
   * @param budgetDestination The destination of the budget directory that holds all the data for a
   *                          given budget.
   * @param incomeFileTitle   The name of the income file. True, if the file was created, else
   *                          returns false.
   * @throws IOException if an input or output exception occurred.
   */
  public static boolean createNewIncomeFile(String budgetDestination, String incomeFileTitle)
      throws IOException {
    File incomeFile = new File(
        budgetDestination + "/" + incomeFileTitle + registerFileType);
    return incomeFile.createNewFile();
  }

  /**
   * Method for creating a file holding all expense data.
   *
   * @param budgetDestination The destination of the budget directory that holds all the data for a
   *                          given budget.
   * @param expenseFileTitle  The name of the expense file.
   * @return True, if the file was created, else returns false.
   * @throws IOException if an input or output exception occurred.
   */

  public static boolean createNewExpenseFile(String budgetDestination, String expenseFileTitle)
      throws IOException {
    File expenseFile = new File(
        budgetDestination + "/" + expenseFileTitle + registerFileType);
    return expenseFile.createNewFile();
  }

  /**
   * Method for creating a file holding all income data.
   *
   * @param budgetDestination The destination of the budget directory that holds all the data for a
   *                          given budget.
   * @param budgetFileTitle   The name of the budget file.
   * @return True, if the file was created, else returns false.
   * @throws IOException if an input or output exception occurred.
   */
  public static boolean createNewBudgetFile(String budgetDestination, String budgetFileTitle)
      throws IOException {
    File budgetFile = new File(
        budgetDestination + "/" + budgetFileTitle + budgetFileType);
    return budgetFile.createNewFile();
  }
}
