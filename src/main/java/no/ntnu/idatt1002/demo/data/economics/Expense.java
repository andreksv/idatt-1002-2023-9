package no.ntnu.idatt1002.demo.data.economics;

import java.time.LocalDate;
import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * This class represents an expense. An expense is an object consisting of four mandatory parameters
 * being its amount, whether it is recurring, a category, a date it was created and the optional
 * description. Expense inherits from the abstract Item class where the category parameter is
 * unique.
 *
 * @author Hanne-Sofie Søreide
 * @since 05.03.2023
 */
public class Expense extends Item {

  private final ObjectProperty<ExpenseCategory> category;

  /**
   * This constructor uses the super constructor to set the fields for 'amount' and 'recurring' and
   * then sets the category. A IllegalArgumentException from this constructor if the category is
   * left blank. The description field is left to the default blank string.
   *
   * @param amount    The amount of the current Expense object as a double.
   * @param recurring True if the current Expense repeats at regular intervals.
   * @param category  The category to which the Expense belongs to, provided as a value of
   *                  ExpenseCategory.
   * @param date      The date of the Expense at format "dd.mm.yy".
   */
  public Expense(double amount, boolean recurring, ExpenseCategory category, LocalDate date) {
    super(amount, recurring, date);

    if (category == null) {
      throw new IllegalArgumentException("The income must belong to a category.");
    }
    this.category = new SimpleObjectProperty<>(category);
  }

  /**
   * This constructor uses the super constructor to set the fields for 'amount', 'description' and
   * 'recurring' and then sets the category. A IllegalArgumentException from this constructor if the
   * category is left blank.
   *
   * @param description A description of the Expense as a String.
   * @param amount      The amount of the current Expense object as a double.
   * @param recurring   True if the current income repeats at regular intervals.
   * @param category    The category to which the Expense belongs to, provided as a value of
   *                    ExpenseCategory
   * @param date        The date of the Expense at format "dd.mm.yy".
   */
  public Expense(String description, double amount, boolean recurring, ExpenseCategory category,
      LocalDate date) {
    super(description, amount, recurring, date);
    if (category == null) {
      throw new IllegalArgumentException("The income must belong to a category.");
    }
    this.category = new SimpleObjectProperty<>(category);
  }

  /**
   * Gets an Object Property of the Expense Category.
   *
   * @return Expense Category property.
   */
  public ObjectProperty<ExpenseCategory> expenseCategoryObjectProperty() {
    return this.category;
  }

  /**
   * The method returns the category to which the Expense belongs as a value of the ExpenseCategory
   * enum class.
   *
   * @return The category the Expense belongs to as a value of the ExpenseCategory enum class.
   */
  public ExpenseCategory getCategory() {
    return category.get();
  }

  /**
   * The method sets the category of an expense to a value of the ExpenseCategory enum class.
   *
   * @param expenseCategory The category to which the expense belongs as a value of the
   *                        ExpenseCategory enum.
   */
  public void setCategory(ExpenseCategory expenseCategory) {
    if (expenseCategory == null) {
      throw new IllegalArgumentException("The income must belong to a category.");
    }
    this.category.set(expenseCategory);
  }

  /**
   * Returns a String that represents the Expense. Also includes the ExpenseCategory
   *
   * @return The Expense as a String
   */
  @Override
  public String toString() {
    return super.toString() + "category=" + category.get() + "\n\n";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Expense expense)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    return category.get() == expense.category.get();
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), category);
  }
}
