package no.ntnu.idatt1002.demo.data.economics;

/**
 * This class represents various types of income that a user can choose from. While not covering
 * every kind of income they give a good range to work with.
 *
 * @author HanneSofie
 * @since 05.03.2023.
 */
public enum IncomeCategory {

  /**
   * The monthly salary of the user.
   */
  SALARY("salary"),

  /**
   * The monthly amount the user receives in student loan.
   */
  STUDENT_LOAN("student loan"),

  /**
   * Monetary gifts.
   */
  GIFT("gift");

  /**
   * Label of the categories. The label formats the string value of the categories two string in
   * lower caps.
   */
  public final String label;

  /**
   * The constructor of the enum constants takes in a string label and assigns it to its respective
   * constant. The label is used for representation in texts and lists at the frontend of the
   * application.
   *
   * @param label A lower-case and readable string representation of the enum constant.
   */
  IncomeCategory(String label) {
    this.label = label;
  }
}
