package no.ntnu.idatt1002.demo.data.economics;

import java.time.YearMonth;
import java.util.List;
import java.util.stream.Collectors;

/**
 * IncomeRegister is a class for storing Income. Subclass of ItemRegister.
 *
 * @author andreas
 */
public class IncomeRegister extends ItemRegister<Income> {

  /**
   * Class constructor that creates an empty List for storing Income.
   */
  public IncomeRegister() {
    super();
  }

  /**
   * Class constructor that takes in a List of Income as argument.
   *
   * @param income the List of Income you want to register.
   */
  public IncomeRegister(List<Income> income) {
    super(income);
  }

  /**
   * Method for getting every Income in a given IncomeCategory.
   *
   * @param category the IncomeCategory you want to get every Income of.
   * @return a new IncomeRegister of every Income with category.
   */
  public IncomeRegister getIncomeByCategory(IncomeCategory category) {
    return new IncomeRegister(items.stream()
        .filter(income -> income.getCategory().compareTo(category) == 0)
        .toList());
  }

  /**
   * Get the Income that are registered with a date corresponding to the given month and year.
   *
   * @param yearMonth month and year for which to withdraw T's=(Income or Expenses).
   * @return list of Income for a certain month and year.
   */
  public IncomeRegister getIncomeByMonth(YearMonth yearMonth) {
    return new IncomeRegister(items.stream()
        .filter(income -> YearMonth.from(income.getDate()).equals(yearMonth))
        .collect(Collectors.toList()));
  }

  /**
   * Get the Income that are recurring.
   *
   * @return a new IncomeRegister of recurring Income.
   */
  public IncomeRegister getRecurringIncome() {
    return new IncomeRegister(items.stream()
        .filter(Item::isRecurring).toList());
  }
}
