package no.ntnu.idatt1002.demo.data.budget;

/**
 * SavingsGoal is a specific goal that you are saving towards, for example a car or holiday.
 */
public class SavingsGoal {

  /**
   * A description of what the SavingsGoal is for.
   */
  private String description;
  /**
   * The monetary amount invested into the SavingsGoal.
   */
  private double amountInvested;

  /**
   * The monetary amount needed to achieve the SavingsGoal.
   */
  private double amountGoal;

  /**
   * Boolean to check if the SavingsGoal has been achieved. SavingsGoal has been achieved when
   * amount is zero.
   */
  private boolean achieved = false;

  /**
   * The class constructor for a SavingsGoal.
   *
   * @param description    a String the explains what the SavingsGoal is for.
   * @param amountInvested the monetary amount invested into the SavingsGoal.
   * @param amountGoal     the monetary amount needed to achieve the SavingsGoal.
   */
  public SavingsGoal(String description, double amountInvested, double amountGoal)
      throws IllegalArgumentException {
    this.description = description;
    if (amountGoal < 0) {
      amountGoal = 0;
    }
    if (amountInvested < 0) {
      amountInvested = 0;
    }
    this.amountGoal = amountGoal;
    this.amountInvested = amountInvested;
    achieved = amountInvested >= amountGoal;

  }

  /**
   * The class constructor for a SavingsGoal, amountInvested is set to zero.
   *
   * @param description a String the explains what the SavingsGoal is for.
   * @param amountGoal  the monetary amount needed to achieve the SavingsGoal.
   */
  public SavingsGoal(String description, double amountGoal) {
    this.description = description;
    if (amountGoal < 0) {
      amountGoal = 0;
    }
    this.amountGoal = amountGoal;
    this.amountInvested = 0;
  }

  /**
   * Getting the description of the SavingsGoal.
   *
   * @return the SavingsGoal´s description.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Getting the amount needed to achieve the SavingsGoal.
   *
   * @return the SavingsGoal´s needed amount.
   */
  public double getAmountGoal() {
    return amountGoal;
  }

  /**
   * Getting the amount invested into the SavingsGoal.
   *
   * @return the SavingsGoal´s amount invested.
   */
  public double getAmountInvested() {
    return amountInvested;
  }

  /**
   * Check if the SavingsGoal is achieved.
   *
   * @return boolean
   */
  public boolean isAchieved() {
    return achieved;
  }

  /**
   * Setting the description of the SavingsGoal.
   *
   * @param description the new description you want to use.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Setting the amountGoal of the SavingsGoal.
   *
   * @param amount the new amount you want to use.
   */
  public void setAmountGoal(double amount) throws IllegalArgumentException {
    if (amount < 0) {
      this.amountGoal = 0;
    } else {
      this.amountGoal = amount;
    }
    achieved = amountInvested >= amountGoal;

  }

  /**
   * Adding an amount to the amountInvested of the SavingsGoal.
   *
   * @param amount the amount you want to add.
   */
  public void addAmountInvested(double amount) throws IllegalArgumentException {
    this.amountInvested += amount;
    achieved = amountInvested >= amountGoal;
  }
}
