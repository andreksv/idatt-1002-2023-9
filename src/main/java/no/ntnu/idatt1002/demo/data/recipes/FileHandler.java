package no.ntnu.idatt1002.demo.data.recipes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * The FileHandler class is a static class that handles reading and writing to the .register files
 * for storing dinner recipes and ingredients at hand.  Files of this class are stored at
 * src/main/resources/recipes.
 *
 * @author hannesofie
 */
public class FileHandler {

  private static final String fileType = ".register";

  /**
   * The method takes a RecipeRegister object and a String as parameters. The recipe register is
   * then written to a file named after the provided string, with the file-type ".register" in the
   * /main/recourses/recipes folder. The file is written at the following format and is only
   * sensitive to new-lines within the instructions-String: # Recipe name - Ingredient 1 | amount |
   * unit - Ingredient 2 | amount | unit - ... Instructions An IllegalArgumentException is thrown if
   * the recipe register is null. IOExceptions may occur upon writing to file, in which case the
   * stacktrace is printed to terminal.
   *
   * @param recipeRegister  A recipe register object that is to be written to file.
   * @param fileDestination The path and title by which to name the .register-file.
   */
  public static void writeRegister(RecipeRegister recipeRegister, String fileDestination) {
    if (recipeRegister == null) {
      throw new IllegalArgumentException("Only a valid register object can be written to file.");
    }

    try (FileWriter fileWriter = new FileWriter(fileDestination + fileType)) {
      recipeRegister.getRecipes().forEach((recipe) -> {
            try {
              fileWriter.write(formatRecipe(recipe).toString());
            } catch (IOException e) {
              throw new RuntimeException(e);
            }
          }
      );

    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * The method supports the method for writing a recipe register to file by taking in a single
   * recipe object and return it as a String at the correct format for storage.
   *
   * @param recipe A recipe object to format into a String.
   * @return A String representation of the recipe at the specified format for file storage.
   */
  public static StringBuilder formatRecipe(Recipe recipe) {
    StringBuilder sb = new StringBuilder();
    sb.append("# ")
        .append(recipe.getName())
        .append("\n")
        .append(formatIngredientList(recipe.getIngredientList()))
        .append("\n")
        .append(recipe.getInstructions())
        .append("\n\n");
    return sb;
  }

  /**
   * The method supports the 'formatRecipe' method by receiving a list of ingredient objects and
   * returning a String at the correct format for the writing to file.
   *
   * @param ingredientList A list of ingredients to be formatted into a String.
   * @return A String of the ingredients at the correct format for writing to file.
   */
  public static StringBuilder formatIngredientList(List<RecipeIngredient> ingredientList) {
    StringBuilder sb = new StringBuilder();

    ingredientList.forEach((ingredient) -> sb.append("- ")
        .append(ingredient.getFoodType())
        .append(" | ")
        .append(ingredient.getAmount())
        .append(" | ")
        .append(ingredient.getUnit())
        .append("\n"));
    return sb;
  }

  /**
   * The method reads a recipe register from file and returns the recipe register object. The title
   * of the file is provided as a String. If the file doesn't exist, a message is written to the
   * terminal and null is returned instead of a recipe register. Each recipe is separated by a
   * '#'-sign and passed on to the method 'readRecipe' that reads and returns each Recipe object for
   * the register.
   *
   * @param fileDestination Title of the .register file at which the recipe register is saved.
   * @return A recipe register object read from file.
   */
  public static RecipeRegister readRecipeRegister(String fileDestination) throws IOException {
    File file = new File(fileDestination + fileType);

    //TODO: Provide 'starter' for recipes

    RecipeRegister register = new RecipeRegister();

    try (Scanner sc = new Scanner(file)) {
      sc.useDelimiter("#");
      String line;

      while (sc.hasNext()) {
        line = sc.next();
        if (!line.isBlank()) {
          register.addRecipe(readRecipe(line));
        }
      }
    } catch (FileNotFoundException e) {
      return null;
    }
    return register;
  }

  /**
   * The method supports the readRecipeRegister method by receiving a string containing the
   * information needed to create one specific recipe object. The method reads the needed
   * information from this String and returns the Recipe object based on this. The beginning of each
   * ingredient line is recognized by a hyphen ('-'), while the instructions are all lines,
   * including internal new-lines, that do not start with a hyphen.
   *
   * @param fileDestination A String representation of the path and recipe to read from file.
   * @return A recipe object based on the provided string representation.
   */
  public static Recipe readRecipe(String fileDestination) {
    Scanner sc = new Scanner(fileDestination);

    Recipe recipe;
    String instructions = "None";
    String recipeName = sc.nextLine().strip();
    StringBuilder sb = new StringBuilder();

    String line;
    recipe = new Recipe(recipeName, instructions);

    while (sc.hasNextLine()) {
      line = sc.nextLine();

      if (line.startsWith("-")) {
        String[] ingredientParts = line.split("\\|");

        FoodItem ingredientType = FoodItem.valueOf(
            ingredientParts[0].replaceFirst("-", "").strip());
        double ingredientAmount = Double.parseDouble(ingredientParts[1].strip());
        MeasuringUnit ingredientUnit = MeasuringUnit.valueOf(ingredientParts[2].strip());

        recipe.addIngredient(ingredientType, ingredientAmount, ingredientUnit);
      } else {
        sb.append(line).append("\n");
      }
    }
    recipe.setInstructions(String.valueOf(sb).strip());
    return recipe;
  }


  /**
   * The method takes in an IngredientsAtHand object and writes it to a .register-file with the
   * provided String as title. The file contains no other information than a simple list of FoodItem
   * constants separated by new-line. The resulting file is stored at /main/recourses/recipes/. An
   * IllegalArgumentException is thrown if the ingredients at hand object is null. IOExceptions may
   * occur upon writing to file, in which case the stacktrace is printed to terminal.
   *
   * @param ingredientsAtHand An IngredientsAtHand object that holds a collection of constants of
   *                          the FoodItem enum class.
   * @param fileDestination   The path and title by which to name the file that the ingredients at
   *                          hand are written to.
   * @throws IOException if an input or output error occurs.
   */
  public static void writeIngredientsAtHand(IngredientsAtHand ingredientsAtHand,
      String fileDestination) throws IOException {
    StringBuilder sb = new StringBuilder();

    try (FileWriter fileWriter = new FileWriter(fileDestination + fileType)) {
      if (ingredientsAtHand == null) {
        fileWriter.write("");
      } else {
        ingredientsAtHand.getIngredientsAtHand()
            .forEach((ingredient) -> sb.append(ingredient).append("\n"));
        try {
          fileWriter.write(String.valueOf(sb));
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

  /**
   * The method reads an IngredientsAtHand object from the .register file with the name provided as
   * a parameter and stored at /main/recourses/recipes/. If the file is not found, a
   * FileNotFoundException is thrown and null is returned instead of a IngredientsAtHand object.
   *
   * @param fileDestination Path and title of the file to read the IngredientsAtHand object from.
   * @return An IngredientsAtHand object based on the provided .register file.
   */
  public static IngredientsAtHand readIngredientsAtHand(String fileDestination) throws IOException {
    File file = new File(fileDestination + fileType);
    IngredientsAtHand ingredientsAtHand = new IngredientsAtHand();

    if (!file.exists()) {
      file.createNewFile();
    }

    try (Scanner sc = new Scanner(file)) {
      String line;

      while (sc.hasNext()) {
        line = sc.next();
        if (!line.isBlank()) {
          ingredientsAtHand.addIngredient(FoodItem.valueOf(line));
        }
      }
    } catch (FileNotFoundException e) {
      return null;
    }
    return ingredientsAtHand;
  }
}