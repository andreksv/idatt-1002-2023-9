package no.ntnu.idatt1002.demo.data.recipes;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The recipe class represents a dinner recipe for the user. It has a name, a textual instruction
 * and holds a collection of recipe ingredient items. Each recipe ingredient has a food type,
 * amount, unit of measure and a status(true/false) of whether it is at hand or not. Apart for
 * methods to get and alter fields, the class provides the following specialized methods: -
 * updateIngredientStatus: takes in a list of food types at hand and updates the 'atHand' property
 * of each of its ingredients accordingly. - getMissingIngredients: returns the number of
 * ingredients in the recipe that are not at hand. - getMissingList: returns an ArrayList of the
 * FoodItem labels (String) that are not at hand. specific food type When the Recipe has not been
 * compared to a collection of ingredients at hand, the number of missing ingredients is set to 0
 * and the list of missing ingredients is set to an empty ArrayList.
 *
 * @author hannesofie
 */
public class Recipe {

  private String name;
  private final ArrayList<RecipeIngredient> ingredientList = new ArrayList<>();
  private String instructions;

  private int missingIngredients = 0;
  private ArrayList<String> missingList = new ArrayList<>();

  /**
   * The constructor method creates a new Recipe object by setting the name and instructions as
   * Strings.
   *
   * @param name         Name of the recipe.
   * @param instructions The instructions of the recipe.
   */
  public Recipe(String name, String instructions) {
    if (name.isBlank() | instructions.isBlank()) {
      throw new IllegalArgumentException("The recipe must have a name and a description.");
    }
    this.name = name;
    this.instructions = instructions;
  }

  /**
   * The method returns the name of the recipe.
   *
   * @return The name of the recipe as a String.
   */
  public String getName() {
    return name;
  }

  /**
   * The method takes in a String to which the name of the recipe is set. If a blank String is
   * given, an IllegalArgumentException is thrown.
   *
   * @param name New name for the recipe.
   */
  public void setName(String name) {
    if (name.isBlank()) {
      throw new IllegalArgumentException("The recipe name cannot be left blank.");
    }
    this.name = name;
  }

  /**
   * The method returns the list of RecipeIngredients that the recipe consists of.
   *
   * @return A list of RecipeIngredients belonging to the recipe.
   */
  public List<RecipeIngredient> getIngredientList() {
    return ingredientList;
  }

  /**
   * The method returns the instructions of the recipe.
   *
   * @return The instructions of the recipe as a String.
   */
  public String getInstructions() {
    return instructions;
  }

  /**
   * The method takes a String as a parameter to which the recipe instructions are set, provided
   * that the String is not blank, in which case an IllegalArgumentException is thrown instead.
   *
   * @param instructions The new instructions of the recipe as a String.
   */
  public void setInstructions(String instructions) {
    if (instructions.isBlank()) {
      throw new IllegalArgumentException("The recipe instructions cannot be left blank.");
    }
    this.instructions = instructions;
  }

  /**
   * The method takes in a constant of the FoodItem enum class and searches for it among the
   * recipe's ingredients. If it is found, the recipe ingredient object is returned, if it is not
   * found, null is returned.
   *
   * @param ingredientType A constant value defined by the FoodItem enum class.
   * @return The recipe ingredient if it is present in the recipe ingredient list, null otherwise.
   */
  public RecipeIngredient getIngredient(FoodItem ingredientType) {
    if (ingredientType == null) {
      return null;
    }
    return this.getIngredientList().stream()
        .filter((ingredient) -> ingredient.getFoodType() == ingredientType)
        .findFirst().orElse(null);
  }

  /**
   * The method adds an ingredient to the recipe if it is not already in the recipe, in which case
   * the existing ingredient of the same food type is updated with the parameters provided to this
   * method. The parameters are a constant of the FoodItem enum class to provide the food type, a
   * double value representing the amount of the ingredient and the measuring unit given as a
   * constant of the MeasuringUnit enum class. If the ingredient is neither added nor updated
   * successfully, false is returned, otherwise true is returned.
   *
   * @param ingredientType The type of food the ingredient belongs to.
   * @param amount         The amount of the ingredient when part of this recipe.
   * @param unit           The measuring unit of the ingredient part of this recipe.
   * @return True if the ingredient is successfully added or altered, otherwise false.
   */
  public boolean addIngredient(FoodItem ingredientType, double amount, MeasuringUnit unit) {
    if (ingredientList.stream()
        .anyMatch((ingredient) -> ingredient.getFoodType() == ingredientType)) {
      try {
        getIngredient(ingredientType).setAmount(amount);
        getIngredient(ingredientType).setUnit(unit);

      } catch (IllegalArgumentException e) {
        return false;
      }
    } else {
      try {
        this.ingredientList.add(new RecipeIngredient(ingredientType, amount, unit));
      } catch (IllegalArgumentException e) {
        return false;
      }
    }
    return true;
  }

  /**
   * The method takes in an object of the IngredientsAtHand class which defines which foods the user
   * has at hand. If the IngredientsAtHand object is null, and IllegalArgumentException is thrown.
   * Otherwise, the recipe's ingredient list is iterated through, and the 'atHand' property of each
   * ingredient is updated to true if it is present in the ingredients at hand collection, or false
   * if it is not. In addition, the recipe's parameters "missingIngredients" and "missingList" are
   * updated with the final number of ingredients in the recipe that are not at hand and the name of
   * those recipes as Strings in an ArrayList, respectively.
   *
   * @param ingredientsAtHand An IngredientsAtHand object holding a collection of the food types
   *                          that the user has available.
   */
  public void updateIngredientStatus(IngredientsAtHand ingredientsAtHand) {
    if (ingredientsAtHand == null) {
      throw new NullPointerException("The ingredients at hand object must exist");
    } else {
      missingList = new ArrayList<>();
      missingIngredients = (int) ingredientList.stream()
          .filter((inRecipe) -> !ingredientsAtHand.atHand(inRecipe.getFoodType())).count();

      ingredientList.forEach((inRecipe) -> {
        inRecipe.setAtHand(ingredientsAtHand.atHand(inRecipe.getFoodType()));
        if (!ingredientsAtHand.atHand(inRecipe.getFoodType())) {
          missingList.add(inRecipe.getFoodType().label);
        }
      });
    }
  }


  /**
   * The method returns the property 'missingIngredients' of the recipe. This is the number of
   * ingredients as an int, contained in teh recipe, that is not part of the collection of
   * ingredients at hand last updated for.
   *
   * @return The number of ingredients in the recipe that are not currently at hand.
   */
  public int getMissingIngredients() {
    return missingIngredients;
  }

  /**
   * The method returns an ArrayList of Strings representing each of the food types that the recipe
   * is missing relative to the collection of ingredients at hand last updated for.
   *
   * @return The ingredients in this recipe that are not currently at hand as a list of Strings.
   */
  public ArrayList<String> getMissingList() {
    return missingList;
  }

  /**
   * The method returns a String representation of the recipe, listing its name, ingredients and
   * instructions.
   *
   * @return A String representation of the recipe.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(this.name).append("\n");
    this.getIngredientList().forEach((ingredient) -> sb.append(ingredient).append("\n"));
    sb.append(this.getInstructions());

    return String.valueOf(sb);
  }

  /**
   * The method takes in another Object and checks whether it is equal to the current recipe object.
   * It first checks if the object reference is the same, in which case true is returned. If not, it
   * checks that the object is not null and whether its class is dissimilar, for which false is also
   * returned. Finally, the object is cast as a Recipe and compared by name. If the two recipe's
   * names are the same, they are considered the same.
   *
   * @param o A general Object.
   * @return True if the object is equal to the current recipe, false otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Recipe recipe)) {
      return false;
    }
    return Objects.equals(name, recipe.name);
  }

  /**
   * The method returns a standard hash-code based on the recipe's name, ingredient list and
   * instructions.
   *
   * @return A hash-code as an integer for the recipe.
   */
  @Override
  public int hashCode() {
    return Objects.hash(name, ingredientList, instructions);
  }
}
