package no.ntnu.idatt1002.demo.data.budget;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for containing and organizing budget names. A budget name is the name that is given to a
 * budget when it is created.
 *
 * @author Harry Linrui Xu
 * @since 19.04.2023
 */
public class BudgetRegister {

  private final List<String> budgetNames;

  /**
   * Constructor for the register. Instantiates the arraylist that contains all budget names.
   */
  public BudgetRegister() {
    budgetNames = new ArrayList<>();
  }

  /**
   * Gets a list of the budget names contained in the budget register.
   *
   * @return Deep copied list of budget names.
   */
  public List<String> getBudgetNames() {
    return new ArrayList<>(budgetNames);
  }

  /**
   * Method for adding a budget name to the register.
   *
   * @param name The name of the budget.
   * @throws IllegalArgumentException If name is null, blank or already exists within the register.
   */
  public void addBudgetName(String name) throws IllegalArgumentException {
    if (name == null) {
      throw new IllegalArgumentException("Name cannot be null");
    }
    if (name.isBlank()) {
      throw new IllegalArgumentException("Name cannot be blank");
    }
    if (budgetNames.contains(name)) {
      throw new IllegalArgumentException("Duplicate entry");
    }
    budgetNames.add(name);
  }

  /**
   * Method for removing a budget name from the register.
   *
   * @param name The name of the budget.
   * @throws IllegalArgumentException If name is null or blank.
   */
  public void removeBudgetName(String name) throws IllegalArgumentException {
    if (name == null) {
      throw new IllegalArgumentException("Name cannot be null");
    }
    if (name.isBlank()) {
      throw new IllegalArgumentException("Name cannot be blank");
    }
    budgetNames.remove(name);
  }

  @Override
  public String toString() {
    StringBuilder namesToString = new StringBuilder();
    budgetNames.forEach(name -> namesToString.append(name).append("\n"));
    return namesToString.toString();
  }
}
