package no.ntnu.idatt1002.demo.data.budget;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;

/**
 * Class that represents GeneralBudget.
 *
 * @author Adele
 */
public class GeneralBudget {

  /**
   * The period the budget is going to last for. Is always the amount of days left in the current
   * month.
   */
  private final int budgetPeriod = getDaysLeftOfBudgetPeriod();
  /**
   * A List of BudgetItems.
   */
  private final List<BudgetItem> budgetItems;
  /**
   * The max amount of the budget.
   */
  private final double maxAmount;

  /**
   * The savings of budget.
   */
  private final Savings savings;

  /**
   * Constructor for a general budget, which is an entire budget project, set for a given month.
   *
   * @param budgetItems List of the budget items in the project.
   * @param maxAmount   Money at disposable to allocate.
   * @param savings     Unused money from last month.
   */
  public GeneralBudget(List<BudgetItem> budgetItems, double maxAmount, Savings savings) {
    if (maxAmount < 0) {
      throw new IllegalArgumentException("The max amount of the budget cant be less than zero");
    } else {
      this.maxAmount = maxAmount;
      this.budgetItems = budgetItems;
      this.savings = savings;
    }
  }

  /**
   * Class constructor.
   *
   * @param budgetItems a List of BudgetItems.
   * @param maxAmount   the max amount of the budget.
   */
  public GeneralBudget(List<BudgetItem> budgetItems, double maxAmount) {
    if (maxAmount < 0) {
      throw new IllegalArgumentException("The max amount of the budget cant be less than zero");
    } else {
      this.maxAmount = maxAmount;
      this.budgetItems = budgetItems;
      this.savings = new Savings();
    }
  }

  /**
   * Class constructor that creates an empty ArrayList for storing BudgetItems.
   *
   * @param maxAmount the max amount of the budget.
   */
  public GeneralBudget(double maxAmount) {
    if (maxAmount < 0) {
      throw new IllegalArgumentException("The max amount of the budget cant be less than zero");
    } else {
      this.maxAmount = maxAmount;
      this.budgetItems = new ArrayList<>();
      this.savings = new Savings();
    }
  }

  /**
   * Returns the amount of days the budget will last for.
   *
   * @return amount of days the budget lasts.
   */
  public int getBudgetPeriod() {
    return budgetPeriod;
  }

  /**
   * Method for getting the maxAmount of the GeneralBudget.
   *
   * @return the GeneralBudget´s
   */
  public double getMaxAmount() {
    return maxAmount;
  }

  /**
   * Method for getting budgetItems.
   *
   * @return an ArrayList of every BudgetItem
   * @throws IllegalArgumentException if budgetItems is empty
   */
  public List<BudgetItem> getBudgetItems() throws IllegalArgumentException {
    return budgetItems;
  }

  /**
   * Method for getting Savings.
   *
   * @return savings of Budget.
   */
  public Savings getSavings() {
    return savings;
  }

  /**
   * Method for getting a Specific BudgetItem from budgetItems.
   *
   * @param expenseCategory the ExpenseCategory of the BudgetItem you want to get
   * @return the BudgetItem with the expenseCategory
   * @throws IllegalArgumentException if there is no BudgetItem with the given ExpenseCategory
   */
  public BudgetItem getBudgetItem(ExpenseCategory expenseCategory) throws IllegalArgumentException {
    for (BudgetItem budgetItem1 : budgetItems) {
      if (budgetItem1.getBudgetCategory().equals(expenseCategory)) {
        return budgetItem1;
      }
    }
    throw new IllegalArgumentException("No BudgetItem for this ExpenseCategory");
  }

  /**
   * Returns the amount of days left in the budget period as a long.
   *
   * @return the amount of days left in the budget period
   */
  public int getDaysLeftOfBudgetPeriod() {
    Month thisMonth = LocalDate.now().getMonth();
    int dayOfTheMonth = LocalDate.now().getDayOfMonth();

    return thisMonth.maxLength() - dayOfTheMonth;
  }

  /**
   * This method adds a budgetItem to the list of budgetItems in the generalBudget.
   *
   * @param budgetItem the budgetItem you want to add.
   */
  public void addToBudgetBudgetItem(BudgetItem budgetItem) {
    if (totalSum() + budgetItem.getBudgetAmount() > maxAmount) {
      throw new IllegalArgumentException(
          "Amount to be added goes over the max value of the budget");
    }

    if (!hasBudgetCategory(budgetItem.getBudgetCategory())) {
      budgetItems.add(budgetItem);
    } else {
      throw new IllegalArgumentException("There is already a budget with this category");
    }
  }

  /**
   * This method takes in a double budgetAmount, String description and a String category to create.
   * and add a BudgetItem to budgetItems.
   *
   * @param budgetAmount The amount of budget as a double
   * @param description  A description of the budget as a String
   * @param category     A category from ExpenseCategory
   */
  public void addToBudget(double budgetAmount, String description, ExpenseCategory category) {
    if ((totalSumAndSavings() + budgetAmount) > maxAmount) {
      throw new IllegalArgumentException(
          "Amount to be added goes over the max value of the budget");
    }
    if (!hasBudgetCategory(category)) {
      budgetItems.add(new BudgetItem(budgetAmount, description, category));
    } else {
      throw new IllegalArgumentException("There is already a budget with this category");
    }
  }

  /**
   * This method checks if the list in the generalBudget already contains a budgetItem with a
   * specified category.
   *
   * @param category A category from ExpenseCategory
   * @return True if the list contains a budgetItem with this category and false if it does not
   */
  public boolean hasBudgetCategory(ExpenseCategory category) {
    return budgetItems.stream()
        .anyMatch(budgetItem -> budgetItem.getBudgetCategory().equals(category));
  }

  /**
   * Method that gets the chosen budget categories. Returns the chosen categories in a list.
   *
   * @return A list of the chosen budget categories.
   */
  public List<ExpenseCategory> getChosenBudgetCategories() {
    return Arrays.stream(ExpenseCategory.values()).toList()
        .stream().filter(this::hasBudgetCategory).toList();
  }

  /**
   * This method checks if there are unused categories in the budget.
   *
   * @return True, if the list contains unused categories. Else returns false.
   */
  private boolean hasUnusedCategories() {
    return budgetItems.size() != ExpenseCategory.values().length;
  }

  /**
   * Method that adds the remaining, unused categories to the budget list. Useful, as unused
   * categories can be automatically added without user input.
   */
  public void addUnusedCategories() {
    if (hasUnusedCategories()) {
      Arrays.stream(ExpenseCategory.values())
          .filter(category -> !hasBudgetCategory(category))
          .forEach(category -> budgetItems.add(new BudgetItem(0, "", category)));
    }
  }

  /**
   * Returns the totalSum of all budgetItems in the generalBudget.
   *
   * @return the sum of the budgetsItems as a double.
   */
  public double totalSum() {
    double sum = 0;
    for (BudgetItem budgetItem : budgetItems) {
      sum += budgetItem.getBudgetAmount();
    }
    return sum;
  }

  /**
   * This method deletes a budgetItem from the list in the GeneralBudget.
   *
   * @param category A category from ExpenseCategory
   */
  public void deleteItemFromBudget(ExpenseCategory category) {
    budgetItems.removeIf(item -> item.getBudgetCategory() == category);
  }

  /**
   * Method returns totalSum of budgetItems and totalSum of savings.
   *
   * @return totalSum of budgetItems and totalSum of savings.
   */
  public double totalSumAndSavings() {
    return totalSum() + savings.getTotalSavings();
  }

  /**
   * Assign an amount to savings. The amount plus other assigned amounts cannot be more than the
   * maxAmount.
   *
   * @param amount the amount you want to assign to savings.
   * @throws IllegalArgumentException if amount plus totalSumAndSavings is more than maxAmount
   */
  public void addToSavings(double amount) {
    if ((totalSumAndSavings() + amount) > maxAmount) {
      throw new IllegalArgumentException(
          "Amount to be added goes over the max value of the budget");
    }
    savings.addToSavings(amount);
  }

  @Override
  public String toString() {
    StringBuilder budgetItemsString = new StringBuilder();
    for (BudgetItem budgetItem : budgetItems) {
      budgetItemsString.append(budgetItem).append("\n\n");
    }
    if (budgetItemsString.isEmpty()) {
      return "maxAmount=" + getMaxAmount() + "\n\n";
    } else {
      return "maxAmount=" + getMaxAmount() + "\n\n" + budgetItemsString;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof GeneralBudget that)) {
      return false;
    }
    return budgetPeriod == that.budgetPeriod && Double.compare(that.maxAmount, maxAmount) == 0
        && Objects.equals(budgetItems, that.budgetItems);
  }

  @Override
  public int hashCode() {
    return Objects.hash(budgetPeriod, budgetItems, maxAmount);
  }
}
