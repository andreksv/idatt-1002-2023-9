package no.ntnu.idatt1002.demo.data.economics;

import no.ntnu.idatt1002.demo.data.budget.GeneralBudget;

/**
 * Class for getting an overview on your total revenue.
 *
 * @author andreas
 */
public class Overview {

  private final IncomeRegister incomeRegister;
  private final ExpenseRegister expenseRegister;
  private double balance;
  private final GeneralBudget budget;

  /**
   * Class constructor that take in a GeneralBudget and creates an empty incomeRegister,
   * expenseRegister and set totalRevenue to zero.
   *
   * @param budget the GeneralBudget you want to overview.
   */
  public Overview(GeneralBudget budget) {
    this.incomeRegister = new IncomeRegister();
    this.expenseRegister = new ExpenseRegister();
    this.balance = 0;
    this.budget = budget;
  }

  /**
   * Class constructor that takes in an incomeRegister and expenseRegister and calculates total
   * balance based on them.
   *
   * @param incomeRegister  the incomeRegister you want to overview.
   * @param expenseRegister the expenseRegister you want to overview.
   * @param budget          the GeneralBudget you want to overview.
   */
  public Overview(IncomeRegister incomeRegister, ExpenseRegister expenseRegister,
      GeneralBudget budget) {
    this.incomeRegister = incomeRegister;
    this.expenseRegister = expenseRegister;
    this.budget = budget;
    updateBalance();
  }

  /**
   * Get the IncomeRegister of the Overview.
   *
   * @return IncomeRegister
   */
  public IncomeRegister getIncomeRegister() {
    return incomeRegister;
  }

  /**
   * Get the ExpenseRegister of the Overview.
   *
   * @return ExpenseRegister
   */
  public ExpenseRegister getExpenseRegister() {
    return expenseRegister;
  }

  /**
   * Get the GeneralBudget of the Overview.
   *
   * @return GeneralBudget
   */
  public GeneralBudget getBudget() {
    return budget;
  }

  /**
   * Get the total revenue of overview.
   *
   * @return the overview´s total revenue.
   */
  public double getTotalBalance() {
    return balance;
  }

  /**
   * Method for updating the total balance by taking the max amount of the budget minus the total
   * sum of the expense register.
   */
  public void updateBalance() {
    balance = budget.getMaxAmount() - expenseRegister.getTotalSum();
  }

  /**
   * Method for getting a percentage of the total revenue.
   *
   * @param percentage the percentage you want to get.
   * @return a percentage of total revenue.
   */
  public double getPercentageOfTotalBalance(double percentage) {
    return Math.round(balance * (percentage / 100));
  }

  /**
   * Method for getting Expense subtracted from a BudgetItem for a specific ExpenseCategory.
   *
   * @param category the ExpenseCategory you want to use.
   * @return Expense subtracted from BudgetItem for category.
   * @throws IllegalArgumentException if there is no Budget for budget category.
   */
  public double getBudgetItemMinusExpense(ExpenseCategory category)
      throws IllegalArgumentException {
    if (budget.hasBudgetCategory(category)) {
      return budget.getBudgetItem(category).getBudgetAmount()
          - expenseRegister.getExpenseByCategory(category).getTotalSum();
    } else {
      throw new IllegalArgumentException("There is no Budget for the budget category");
    }
  }

  /**
   * Method for checking if you have used up a specific BudgetItem.
   *
   * @param category the ExpenseCategory you want to check.
   * @return true or false depending on if you have used it up.
   */
  public boolean hasUsedUpBudgetItem(ExpenseCategory category) {
    return (getBudgetItemMinusExpense(category) <= 0);
  }
}
