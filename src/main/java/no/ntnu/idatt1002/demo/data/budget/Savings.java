package no.ntnu.idatt1002.demo.data.budget;

import java.util.ArrayList;
import java.util.List;

/**
 * Savings is a class that has an amount that is given to it, which can further can be given to
 * different savingsGoals.
 */
public class Savings {

  /**
   * List of SavingsGoals.
   */
  private final List<SavingsGoal> savingsGoals;
  /**
   * An amount not used on any SavingsGoal.
   */
  private double amount;

  /**
   * Class constructor that sets savingsGoals to an empty ArrayList and amount to zero.
   */
  public Savings() {
    this.savingsGoals = new ArrayList<>();
    this.amount = 0;
  }

  /**
   * Class constructor that sets savingsGoals to an empty ArrayList and amount to a given double.
   *
   * @param amount the double you want to assign amount
   */
  public Savings(double amount) {
    this.savingsGoals = new ArrayList<>();
    this.amount = amount;
  }

  /**
   * Class constructor that sets savingsGoals to a given ArrayList and amount to zero.
   *
   * @param savingsGoals the ArrayList you want to assign savingsGoals
   */
  public Savings(ArrayList<SavingsGoal> savingsGoals) {
    this.savingsGoals = savingsGoals;
    this.amount = 0;
  }

  /**
   * Class constructor that sets savingsGoals to a given ArrayList and amount to a given double.
   *
   * @param savingsGoals the ArrayList you want to assign savingsGoals
   * @param amount       The amount of the saving.
   */
  public Savings(ArrayList<SavingsGoal> savingsGoals, double amount) {
    this.savingsGoals = savingsGoals;
    this.amount = amount;
  }

  /**
   * Get List of SavingsGoals.
   *
   * @return List of SavingsGoals
   */
  public List<SavingsGoal> getSavingsGoals() {
    return savingsGoals;
  }

  /**
   * Get the amount.
   *
   * @return savings amount
   */
  public double getAmount() {
    return amount;
  }

  /**
   * Get the total amount invested into SavingsGoals and the amount that is unused.
   *
   * @return amount invested into SavingsGoals and amount not used.
   */
  public double getTotalSavings() {
    double totalSavings = 0;
    for (SavingsGoal savingsGoal : savingsGoals) {
      totalSavings += savingsGoal.getAmountInvested();
    }
    totalSavings += getAmount();
    return totalSavings;
  }

  /**
   * Get a specific SavingsGoal from savingsGoals.
   *
   * @param savingsGoal the SavingsGoal you want to get
   * @return the SavingsGoal you wanted to find if it exists
   * @throws IllegalArgumentException if the SavingsGoal is not in the List
   */
  public SavingsGoal getSavingsGoal(SavingsGoal savingsGoal) {
    for (SavingsGoal goal : savingsGoals) {
      if (savingsGoal == goal) {
        return goal;
      }
    }
    throw new IllegalArgumentException("The SavingsGoal is not in the List");
  }

  /**
   * Method to check if savingsGoals contains a specific SavingsGoal.
   *
   * @param savingsGoal the SavingsGoal you want to check.
   * @return boolean depending on if savingsGoals contains the savingsGoal.
   */
  public boolean hasSavingsGoal(SavingsGoal savingsGoal) {
    return savingsGoals.contains(savingsGoal);
  }

  /**
   * A List of all SavingsGoal that are achieved. A SavingsGoal is achieved if its amount is zero.
   *
   * @return List of achieved SavingsGoal
   */
  public List<SavingsGoal> getAchievedSavingsGoal() {
    List<SavingsGoal> achievedSavingsGoals = new ArrayList<>();
    for (SavingsGoal goal : savingsGoals) {
      if (goal.isAchieved()) {
        achievedSavingsGoals.add(goal);
      }
    }
    return achievedSavingsGoals;
  }

  /**
   * Method for adding an amount to a SavingsGoal.
   *
   * @param amount      the amount you want to add
   * @param savingsGoal the SavingsGoal you want to add to
   * @throws IllegalArgumentException if the amount is higher than the Savings amount.
   * @throws IllegalArgumentException if SavingsGoal is not in savingsGoal.
   */
  public void addToSavingsGoal(int amount, SavingsGoal savingsGoal) {
    if (amount > this.amount) {
      throw new IllegalArgumentException("The amount cannot be higher than the Savings amount");
    }
    if (!hasSavingsGoal(savingsGoal)) {
      throw new IllegalArgumentException("The SavingsGoal is not in savingsGoals");
    }
    getSavingsGoal(savingsGoal).addAmountInvested(amount);
    this.amount -= amount;
  }

  /**
   * Add an amount to Savings.
   *
   * @param amount the amount you want to add.
   */
  public void addToSavings(double amount) {
    this.amount += amount;
  }

  /**
   * Add a SavingsGoal to Savings.
   *
   * @param savingsGoal the SavingsGoal you want to add.
   * @throws IllegalArgumentException if the SavingsGoal already exists in the Savings.
   */
  public void addSavingsGoal(SavingsGoal savingsGoal) {
    if (savingsGoals.contains(savingsGoal)) {
      throw new IllegalArgumentException("SavingsGoal already in Savings");
    } else {
      savingsGoals.add(savingsGoal);
    }
  }
}