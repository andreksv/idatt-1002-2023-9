package no.ntnu.idatt1002.demo.data.recipes;

/**
 * The Ingredient class represents an ingredient that can be part of a recipe in real life and/or be
 * available to the user in real life. When the ingredient is part of a recipe, the subclass called
 * RecipeIngredient is used. An ingredient belongs to a food type among the values of the FoodItem
 * enum class. Examples are onion, tomato, spaghetti, chicken etc. The ingredient is provided in a
 * specific amount and unit of measure.
 *
 * @author hannesofie
 */
public class Ingredient {

  private FoodItem foodType;
  private double amount;
  private MeasuringUnit unit;

  /**
   * The constructor of a new Ingredient object takes in three mandatory fields; ingredient type as
   * defined in the enum class FoodItem, an amount and a unit of measure defined in the enum class
   * MeasuringUnit. The amount must be a positive number.
   *
   * @param ingredient What type of food the ingredient is.
   * @param amount     The amount of the ingredient.
   * @param unit       The unit of measure of the given amount of the ingredient.
   */
  public Ingredient(FoodItem ingredient, double amount, MeasuringUnit unit) {
    if (ingredient == null | amount <= 0.0f | unit == null) {
      throw new IllegalArgumentException(
          "The ingredient must have a type, amount and measuring unit.");
    }
    this.foodType = ingredient;
    this.amount = amount;
    this.unit = unit;
  }

  /**
   * The method returns the food type that the ingredient belongs to among the valid types in the
   * FoodItem enum class.
   *
   * @return What type of food the ingredient is.
   */
  public FoodItem getFoodType() {
    return foodType;
  }

  /**
   * The method takes in a value of the FoodItem enum and sets the ingredient's foodType field equal
   * to this constant. If null is given an IllegalArgumentException is thrown.
   *
   * @param foodType The type of food to assign the ingredient to.
   */
  public void setFoodType(FoodItem foodType) {
    if (foodType == null) {
      throw new IllegalArgumentException("The food type must be set to a valid value of FoodItem.");
    }
    this.foodType = foodType;
  }

  /**
   * The method returns the amount of an ingredient as the datatype double.
   *
   * @return The amount of an ingredient as double.
   */
  public double getAmount() {
    return amount;
  }

  /**
   * The method takes in a new amount as a double and sets the ingredient's amount field equal to
   * this value. If the provided value is not a positive double, an IllegalArgumentException is
   * thrown.
   *
   * @param amount The amount of an ingredient as double.
   */
  public void setAmount(double amount) {
    if (amount <= 0.0f) {
      throw new IllegalArgumentException("The amount of an ingredient cannot be zero or negative.");
    }
    this.amount = amount;
  }

  /**
   * The method returns the unit of measure of the ingredient as a value of the MeasuringUnit enum
   * class.
   *
   * @return The unit of measure of the given amount of the ingredient.
   */
  public MeasuringUnit getUnit() {
    return unit;
  }

  /**
   * The method takes in a value of the MeasuringUnit enum class and sets the ingredient's unit
   * equal to this value.
   *
   * @param unit The unit of measure of the given amount of the ingredient.
   */
  public void setUnit(MeasuringUnit unit) {
    if (unit == null) {
      throw new IllegalArgumentException(
          "The food's measuring unit must be set to a valid value of MeasuringUnit.");
    }
    this.unit = unit;
  }

  /**
   * The method returns a String representation of an Ingredient object, listing its type, amount
   * and unit.
   *
   * @return A String representation of the ingredient object.
   */
  @Override
  public String toString() {
    return "Ingredient{"
        + "foodType=" + foodType.label
        + ", amount=" + amount
        + ", unit=" + unit.label
        + '}';
  }

  /**
   * The method checks if a given object is equal to the ingredient object. If the object is of the
   * same class as well as having the same value for each class field, the object is equal to the
   * Ingredient.
   *
   * @param o An Object to which the Ingredient should be compared.
   * @return True if the object is equivalent to the Ingredient, false otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Ingredient that)) {
      return false;
    }
    return Double.compare(that.amount, amount) == 0 && foodType == that.foodType
        && unit == that.unit;
  }

}
