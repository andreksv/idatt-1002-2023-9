package no.ntnu.idatt1002.demo.data.economics;

import java.util.ArrayList;
import java.util.List;

/**
 * ItemRegister is a generic class used for storing either Income or Expense.
 *
 * @param <T> Income or Expense
 * @author andreas
 */
public abstract class ItemRegister<T extends Item> {

  List<T> items;

  /**
   * Class constructor that creates an empty List for storing item´s.
   */
  public ItemRegister() {
    this.items = new ArrayList<>();
  }

  /**
   * Class constructor that takes in a List of T=(Income or Expense) as argument.
   *
   * @param items the List of T=(Income or Expense) you want to register.
   */
  public ItemRegister(List<T> items) {
    this.items = items;
  }

  /**
   * Get a List of every item.
   *
   * @return item List.
   */
  public List<T> getItems() {
    return items;
  }

  /**
   * Add a new item to the register.
   *
   * @param newItem the item you want to add.
   * @throws IllegalArgumentException if newItem is already in the register.
   */
  public void addItem(T newItem) throws IllegalArgumentException {
    if (items.contains(newItem)) {
      throw new IllegalArgumentException("This item is already registered");
    }
    items.add(newItem);
  }

  /**
   * Remove an item from the register.
   *
   * @param itemWantRemoved the item you want to remove.
   * @throws IllegalArgumentException if the itemWantRemoved is not in the register.
   */
  public void removeItem(T itemWantRemoved) throws IllegalArgumentException {
    if (!items.remove(itemWantRemoved)) {
      throw new IllegalArgumentException("The item is not in the register");
    }
  }

  /**
   * Check if items is empty.
   *
   * @return true or false depending on if items is empty.
   */
  public boolean isEmpty() {
    return this.items.isEmpty();
  }

  /**
   * Get the sum of all T´s=(Income or Expenses) in items.
   *
   * @return sum of all the T´s=(Income or Expenses).
   */
  public double getTotalSum() {
    return items.stream().map(Item::getAmount).mapToDouble(Double::doubleValue).sum();
  }


  @Override
  public String toString() {
    StringBuilder stringItems = new StringBuilder();
    for (Item item : items) {
      stringItems.append(item.toString());
    }
    if (stringItems.isEmpty()) {
      return "The register is Empty";
    } else {
      return stringItems.toString();
    }
  }
}
