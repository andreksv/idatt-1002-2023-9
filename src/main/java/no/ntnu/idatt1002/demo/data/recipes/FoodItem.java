package no.ntnu.idatt1002.demo.data.recipes;

/**
 * The FoodItem enum class defines a set of food types that may be serving as ingredients in recipes
 * and that the user may, or may not, have available in the cupboard, fridge or freezer. The label
 * is a strict lower case version of the constant value where the underscore is replaced by a space.
 * This value is used at the front-end to represent each enum value.
 *
 * @author hannesofie
 * @since 13.03.2023
 */
public enum FoodItem {

  /**
   * Onion.
   */
  ONION("onion"),

  /**
   * Minced meat.
   */
  MINCED_MEAT("minced meat"),

  /**
   * Potato.
   */
  POTATO("potato"),

  /**
   * Yellow cheese.
   */
  YELLOW_CHEESE("yellow cheese"),

  /**
   * Wheat flour.
   */
  WHEAT_FLOUR("wheat flour"),

  /**
   * Milk.
   */
  MILK("milk"),

  /**
   * Tomato.
   */
  TOMATO("tomato"),

  /**
   * Orange.
   */
  ORANGE("orange"),

  /**
   * Lemon.
   */
  LEMON("lemon"),

  /**
   * Salsa sauce.
   */
  SALSA_SAUCE("salsa sauce"),

  /**
   * Cucumber.
   */
  CUCUMBER("cucumber"),

  /**
   * Salad.
   */
  SALAD("salad"),

  /**
   * Spinach.
   */
  SPINACH("spinach"),

  /**
   * Spring roll.
   */
  SPRING_ROLL("spring roll"),

  /**
   * Bell pepper.
   */
  BELL_PEPPER("bell pepper"),

  /**
   * Chickpeas.
   */
  CHICKPEAS("chickpeas"),

  /**
   * Spaghetti.
   */
  SPAGHETTI("spaghetti"),

  /**
   * Pasta.
   */
  PASTA("pasta"),

  /**
   * Cream.
   */
  CREAM("cream"),

  /**
   * Honey.
   */
  HONEY("honey"),

  /**
   * Vinegar.
   */
  VINEGAR("vinegar"),

  /**
   * Tomato paste.
   */
  TOMATO_PASTE("tomato paste"),

  /**
   * Chili.
   */
  CHILLI("chilli"),

  /**
   * Egg.
   */
  EGG("egg"),

  /**
   * Olive oil.
   */
  OLIVE_OIL("olive oil"),

  /**
   * Ham.
   */
  HAM("ham"),

  /**
   * Parmesan.
   */
  PARMESAN("parmesan"),

  /**
   * Snap pea.
   */
  SNAP_PEA("snap pea"),

  /**
   * Macaroni.
   */
  MACARONI("macaroni"),

  /**
   * Salmon.
   */
  SALMON("salmon"),

  /**
   * Fish.
   */
  FISH("fish"),

  /**
   * Carrot.
   */
  CARROT("carrot"),

  /**
   * Butter.
   */
  BUTTER("butter"),

  /**
   * Leek.
   */
  LEEK("leek"),

  /**
   * Bread crumbs.
   */
  BREADCRUMBS("breadcrumbs"),

  /**
   * Oil.
   */
  OIL("oil"),

  /**
   * Summer cutlet.
   */
  SUMMER_CUTLET("summer cutlet"),

  /**
   * Red onion.
   */
  RED_ONION("red onion"),

  /**
   * Avocado.
   */
  AVOCADO("avocado"),

  /**
   * Lemon juice.
   */
  LEMON_JUICE("lemon juice"),

  /**
   * Dry thyme.
   */
  DRY_THYME("dry thyme"),

  /**
   * Fresh yeast.
   */
  FRESH_YEAST("fresh yeast"),

  /**
   * Garlic clove.
   */
  GARLIC_CLOVE("garlic clove"),

  /**
   * Ginger.
   */
  GINGER("ginger"),

  /**
   * Canned tomato.
   */
  CANNED_TOMATO("canned tomato"),

  /**
   * Dry basil.
   */
  DRY_BASIL("dry basil"),

  /**
   * Fresh basil.
   */
  FRESH_BASIL("fresh basil"),

  /**
   * Celery.
   */
  CELERY("celery"),

  /**
   * Broth.
   */
  BROTH("broth"),

  /**
   * Bay leaf.
   */
  BAY_LEAF("bay leaf"),

  /**
   * Chili beans.
   */
  CHILLI_BEANS("chilli beans"),

  /**
   * Chili powder.
   */
  CHILLI_POWDER("chilli powder"),

  /**
   * Cumin powder.
   */
  CUMIN_POWDER("cumin powder"),

  /**
   * Pie dough.
   */
  PIE_DOUGH("pie dough"),

  /**
   * Broccoli.
   */
  BROCCOLI("broccoli"),

  /**
   * Lam.
   */
  LAM("lam"),

  /**
   * Sugar.
   */
  SUGAR("sugar"),

  /**
   * Shallot.
   */
  SHALLOT("shallot"),

  /**
   * Red wine.
   */
  RED_WINE("red wine"),

  /**
   * White bananas.
   */
  WHITE_BEANS("white beans"),

  /**
   * Frozen green peas.
   */
  FROZEN_GREEN_PEAS("frozen green peas"),

  /**
   * Sausage.
   */
  SAUSAGE("sausage"),

  /**
   * Dry oregano.
   */
  DRY_OREGANO("dry oregano");

  /**
   * Label of the categories. The label formats the string value of the categories two string in
   * lower caps.
   */
  public final String label;

  /**
   * The constructor of the enum constants takes in a string label and assigns it to its respective
   * constant. The label is used for representation in texts and lists at the frontend of the
   * application.
   *
   * @param label A lower-case and readable string representation of the enum constant.
   */
  FoodItem(String label) {
    this.label = label;
  }
}
