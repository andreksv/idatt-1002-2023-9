package no.ntnu.idatt1002.demo.data.recipes;

import java.util.ArrayList;
import java.util.List;

/**
 * The class holds a collection of FoodItem constants in an ArrayList that represents the groceries
 * that the user has available in real life. The class provides methods to add and remove food types
 * from the collection as well as checking if a given food type is available, i.e. is part of the
 * collection.
 *
 * @author hannesofie
 */
public class IngredientsAtHand {

  private final ArrayList<FoodItem> ingredientsAtHand = new ArrayList<>();

  /**
   * The method returns the collection of ingredients at hand as an arraylist of FoodItem enum
   * constants.
   *
   * @return The collection of food types at hand to the user.
   */
  public List<FoodItem> getIngredientsAtHand() {
    return ingredientsAtHand;
  }

  /**
   * The method takes in an ingredient object and adds it to the collection of ingredients at hand.
   *
   * @param ingredient The food type to add to the collection of ingredients at hand.
   */
  public void addIngredient(FoodItem ingredient) {
    if (!this.atHand(ingredient) && ingredient != null) {
      this.ingredientsAtHand.add(ingredient);
    }
  }

  /**
   * The method takes in a constant of the FoodType enum class and checks if it is present in the
   * collection. If it is present, true is returned, otherwise false.
   *
   * @param foodItem A constant value of the FoodItem enum class to check for in the collection of
   *                 ingredients at hand.
   * @return True if the food type is at hand, otherwise false.
   */
  public boolean atHand(FoodItem foodItem) {
    return ingredientsAtHand.stream().anyMatch((in) -> in.equals(foodItem));
  }


  /**
   * The method takes in a value of the FoodItem enum as a parameter and removes it from the
   * collection of ingredients at hand if it exists and returns true. If no ingredient of the given
   * type was found in the collection, false is returned.
   *
   * @param ingredientType What type of food to remove from the list of ingredients at hand.
   * @return True if the ingredient was found among the ingredients at hand and removed, false
   *         otherwise.
   */
  public boolean removeIngredient(FoodItem ingredientType) {
    return ingredientsAtHand.remove(ingredientType);
  }

}
