package no.ntnu.idatt1002.demo.data.budget;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class that handles the reading and writing to the budget archive file, which contains all
 * existing budget projects. New budgets are added to the archive through this class.
 *
 * @author Harry Linrui Xu
 * @since 19.04.2023
 */
public class FileHandlingBudgetArchive {

  private static final String fileType = ".archive";

  /**
   * Method for writing (adding) a budget register to the archive.
   *
   * @param budgetNames     The budget register.
   * @param fileDestination The file destination in resources.
   * @throws IOException if an input or output exception occurred.
   */
  public static void writeBudgetRegisterToArchive(BudgetRegister budgetNames,
      String fileDestination) throws IOException {

    try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileDestination + fileType))) {
      bw.write(budgetNames.toString());
    } catch (IOException ioe) {
      throw new IOException("Could not write to file: Archive", ioe);
    }
  }

  /**
   * Checks if the budget register in the archive contains any budget names. In other words, if it
   * is empty.
   *
   * @param fileDestination The file destination in resources.
   * @return True, if only "null" is read. Else, returns false.
   * @throws IOException if an input or output exception occurred.
   */
  public static boolean isBudgetRegisterEmpty(String fileDestination) throws IOException {
    try (BufferedReader br = new BufferedReader(new FileReader(fileDestination + fileType))) {
      return br.readLine() == null;
    }
  }

  /**
   * Method for reading the budget register from the archive.
   *
   * @param fileDestination The file destination in resources.
   * @return The budget register that is read from file.
   * @throws IOException if an input or output exception occurred.
   */
  public static BudgetRegister readBudgetArchive(String fileDestination) throws IOException {
    BudgetRegister budgetRegister = null;
    String budgetName;
    String line;

    try (BufferedReader br = new BufferedReader(
        new FileReader(fileDestination + fileType))) {

      String nextLine = br.readLine();
      while ((line = nextLine) != null) {
        nextLine = br.readLine();
        budgetName = line;

        if (budgetRegister == null) {
          budgetRegister = new BudgetRegister();
        }
        budgetRegister.addBudgetName(budgetName);
      }
    }
    return budgetRegister;
  }
}
