package no.ntnu.idatt1002.demo.data.economics;

import java.time.LocalDate;
import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * The Income class inherits from the Item class. The Item class additionally has a private field
 * for an enum value of IncomeCategory.
 *
 * @author Hanne-Sofie
 */
public class Income extends Item {

  private final ObjectProperty<IncomeCategory> category;

  /**
   * This constructor uses the super constructor to set the fields for 'amount' and 'recurring' and
   * then sets the category. A IllegalArgumentException from this constructor if the category is
   * left blank. The description field is left to the default blank string.
   *
   * @param amount    The amount of the current income object as a double.
   * @param recurring True if the current income repeats at regular intervals.
   * @param category  The category to which the Income belongs to, provided as a value of
   *                  IncomeCategory.
   * @param date      The date of the Income at format "dd.mm.yy".
   */
  public Income(double amount, boolean recurring, IncomeCategory category, LocalDate date) {
    super(amount, recurring, date);

    if (category == null) {
      throw new IllegalArgumentException("The income must belong to a category.");
    }
    this.category = new SimpleObjectProperty<>(category);
  }

  /**
   * This constructor uses the super constructor to set the fields for 'amount', 'description' and
   * 'recurring' and then sets the category. A IllegalArgumentException from this constructor if the
   * category is left blank.
   *
   * @param description A description of the income as a String.
   * @param amount      The amount of the current income object as a double.
   * @param recurring   True if the current income repeats at regular intervals.
   * @param category    The category to which the income belongs to, provided as a value of
   *                    IncomeCategory.
   * @param date        The date of the Income at format "dd.mm.yy".
   */
  public Income(String description, double amount, boolean recurring, IncomeCategory category,
      LocalDate date) {
    super(description, amount, recurring, date);
    if (category == null) {
      throw new IllegalArgumentException("The income must belong to a category.");
    }
    this.category = new SimpleObjectProperty<>(category);

  }

  /**
   * Gets the ObjectProperty of the category value.
   *
   * @return The property of the income category.
   */
  public ObjectProperty<IncomeCategory> incomeCategoryObjectProperty() {
    return this.category;
  }

  /**
   * The method returns the category to which the Income belongs as a value of the IncomeCategory
   * enum.
   *
   * @return The category to which the Income belongs to as a value of the IncomeCategory enum.
   */
  public IncomeCategory getCategory() {
    return category.get();
  }

  /**
   * The method sets the category of an item to a value of the Category enum class.
   *
   * @param category The category to which the Income belongs to as a value of the IncomeCategory
   *                 enum.
   */
  public void setCategory(IncomeCategory category) {
    if (category == null) {
      throw new IllegalArgumentException("The income must belong to a category.");
    }
    this.category.set(category);
  }

  /**
   * Returns a String that represents the Income. Also includes the IncomeCategory
   *
   * @return The Income as a String
   */
  @Override
  public String toString() {
    return super.toString() + "category=" + category.get() + "\n\n";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Income income)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    return Objects.equals(category.get(), income.category.get());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), category);
  }
}
