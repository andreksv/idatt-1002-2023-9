package no.ntnu.idatt1002.demo.data.economics;

/**
 * This class represents various types of expenditures that a user can choose from. While not
 * covering every kind of expense they give a good range to work with.
 *
 * @author Andreas Kluge Svensrud.
 * @since 02.03.2023.
 */
public enum ExpenseCategory {

  /**
   * Expenditures on food, such as groceries.
   */
  FOOD("food"),

  /**
   * Expenditures on rent.
   */
  RENT("rent"),

  /**
   * Expenditures on clothes.
   */
  CLOTHES("clothes"),

  /**
   * Expenditures on books.
   */
  BOOKS("books"),

  /**
   * Expenditures on health.
   */
  HEALTH("health"),

  /**
   * Expenditures on activities, such as a bowling night.
   */
  ACTIVITIES("activities"),

  /**
   * Expenditures on everything not listed above.
   */
  OTHER("other");

  /**
   * Label of the categories. The label formats the string value of the categories two string in
   * lower caps.
   */
  public final String label;

  /**
   * The constructor of the enum constants takes in a string label and assigns it to its respective
   * constant. The label is used for representation in texts and lists at the frontend of the
   * application.
   *
   * @param label A lower-case and readable string representation of the enum constant.
   */
  ExpenseCategory(String label) {
    this.label = label;
  }

}
