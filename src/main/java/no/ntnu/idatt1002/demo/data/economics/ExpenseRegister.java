package no.ntnu.idatt1002.demo.data.economics;

import java.time.YearMonth;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ExpenseRegister is a class for storing Expenses. Subclass of ItemRegister.
 *
 * @author andreas
 */
public class ExpenseRegister extends ItemRegister<Expense> {

  /**
   * Class constructor that creates an empty List for storing Expense.
   */
  public ExpenseRegister() {
    super();
  }

  /**
   * Class constructor that takes in a List of Expense as argument.
   *
   * @param expenses the List of Expense you want to register.
   */
  public ExpenseRegister(List<Expense> expenses) {
    super(expenses);
  }

  /**
   * Method for getting every Expense in a given ExpenseCategory.
   *
   * @param category the ExpenseCategory you want to get every Expense of.
   * @return a new ExpenseRegister of every Expense with category.
   */
  public ExpenseRegister getExpenseByCategory(ExpenseCategory category) {
    return new ExpenseRegister(
        this.items.stream().filter(expense -> expense.getCategory().compareTo(category) == 0)
            .toList());
  }

  /**
   * Get the Expenses that are registered with a date corresponding to the given month and year.
   *
   * @param yearMonth month and year for which to withdraw Expenses.
   * @return list of Expenses for a certain month and year.
   */
  public ExpenseRegister getExpenseByMonth(YearMonth yearMonth) {
    return new ExpenseRegister(items.stream()
        .filter(income -> YearMonth.from(income.getDate()).equals(yearMonth))
        .collect(Collectors.toList()));
  }

  /**
   * Get the Expenses that are recurring.
   *
   * @return a new ExpenseRegister of recurring Expenses.
   */
  public ExpenseRegister getRecurringExpense() {
    return new ExpenseRegister(items.stream()
        .filter(Item::isRecurring).toList());
  }
}
