package no.ntnu.idatt1002.demo.data.budget;

import java.util.Objects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;

/**
 * This class represents a budgetItem. This is essentially a budget category
 * that user can add to their budgeting.
 *
 * @author Adele
 */
public class BudgetItem {

  private final DoubleProperty budgetAmount;
  private final ObjectProperty<ExpenseCategory> budgetCategory;
  private final StringProperty budgetDescription;

  /**
   * The constructor of a new Budgetitem.
   *
   * @param budgetAmount The amount of budget as a double
   * @param description  A description of the budget as a String
   * @param category     A category from ExpenseCategory
   * @throws IllegalArgumentException if the budgetAmount is less than zero.
   */
  public BudgetItem(double budgetAmount, String description, ExpenseCategory category) {
    if (budgetAmount < 0) {
      throw new IllegalArgumentException("The amount of the budget item must be more than zero");
    }
    this.budgetAmount = new SimpleDoubleProperty(budgetAmount);
    this.budgetDescription = new SimpleStringProperty(description);
    this.budgetCategory = new SimpleObjectProperty<>(category);
  }

  /**
   * This method gets the BudgetAmount.
   *
   * @return the budgetAmount as a double
   */
  public double getBudgetAmount() {
    return budgetAmount.get();
  }

  /**
   * Gets the amount property of its corresponding amount value.
   *
   * @return The amount property.
   */
  public DoubleProperty getAmountProperty() {
    return budgetAmount;
  }

  /**
   * Sets the amount to the value of the argument.
   *
   * @param amount The value to which the amount will change.
   */
  public void setBudgetAmount(double amount) {
    this.budgetAmount.set(amount);
  }

  /**
   * This method gets the category.
   *
   * @return the category as one of the categories in ExpenseCategory
   */
  public ExpenseCategory getBudgetCategory() {
    return budgetCategory.get();
  }

  /**
   * Gets the category property of its corresponding category value.
   *
   * @return The category property.
   */
  public ObjectProperty<ExpenseCategory> getCategoryProperty() {
    return budgetCategory;
  }

  /**
   * Sets the budget category to the value of the argument.
   *
   * @param category Category to which the budget item will change.
   */
  public void setBudgetCategory(ExpenseCategory category) {
    this.budgetCategory.set(category);
  }

  /**
   * This method gets the description.
   *
   * @return the description as a String.
   */
  public String getBudgetDescription() {
    return budgetDescription.get();
  }

  /**
   * Gets the description property of its corresponding description value.
   *
   * @return The description property.
   */
  public StringProperty getDescriptionProperty() {
    return budgetDescription;
  }

  /**
   * Sets the budget description to the value of the argument.
   *
   * @param description Description to which the budget description will change.
   */
  public void setBudgetDescription(String description) {
    this.budgetDescription.set(description);
  }


  @Override
  public String toString() {
    return "budgetAmount=" + getBudgetAmount()
        + "\nbudgetCategory=" + getBudgetCategory()
        + "\nbudgetDescription=" + getBudgetDescription();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof BudgetItem that)) {
      return false;
    }
    return Objects.equals(budgetAmount.get(), that.budgetAmount.get()) && Objects.equals(
        budgetCategory.get(), that.budgetCategory.get()) && Objects.equals(budgetDescription.get(),
        that.budgetDescription.get());
  }

  @Override
  public int hashCode() {
    return Objects.hash(budgetAmount, budgetCategory, budgetDescription);
  }
}
