package no.ntnu.idatt1002.demo.data.budget;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;

/**
 * FileHandling is a class for writing and reading Budgets to and from a file.
 *
 * @author andreas
 */

public class FileHandlingBudget {

  private static final String fileType = ".budget";
  private static final String maxAmount = "maxAmount=";
  private static final String budgetAmount = "budgetAmount=";
  private static final String budgetCategory = "budgetCategory=";
  private static final String budgetDescription = "budgetDescription=";

  /**
   * Method for writing (adding) a budget to a file.
   *
   * @param generalBudget the budget you want to write to a file.
   * @param fileDestination    the path and name of the file you want to check
   * @throws IOException if an input or output exception occurred.
   */
  public static void writeGeneralBudgetToFile(String fileDestination, GeneralBudget generalBudget)
      throws IOException {
    try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileDestination + fileType))) {
      bw.write(generalBudget.toString());
    } catch (IOException ex) {
      throw new IOException("Error writing to file: " + fileDestination);
    }
  }

  /**
   * Method for checking if a .budget file is empty.
   *
   * @param fileDestination the path and name of the file you want to check
   * @return true or false depending on if file is empty.
   * @throws IOException if an input or output exception occurred.
   */
  public static boolean isEmpty(String fileDestination) throws IOException {
    try (BufferedReader br = new BufferedReader(new FileReader(fileDestination + fileType))) {
      return br.readLine() == null;
    }
  }


  /**
   * Method for checking if a .budget file is new (no categories) or old (has budget categories).
   *
   * @param fileDestination The path and name of the file
   * @return True, if the budget is new. Else, returns false
   * @throws IOException if an input or output exception occurred.
   */
  public static boolean isNewBudget(String fileDestination) throws IOException {
    try (BufferedReader br = new BufferedReader(
        new FileReader(fileDestination + fileType))) {

      for (int i = 0; i < 2; ++i) {
        br.readLine();
      }

      if (br.readLine() == null) {
        return true;
      }
    }
    return false;
  }

  /**
   * Method for writing the disposable income, the "max amount" of the budget to file.
   *
   * @param fileDestination The file destination that max amount will be written into.
   * @param maxAmount       The disposable income of the budget.
   * @throws IOException if an input or output exception occurred.
   */
  public static void writeMaxAmountToFile(String fileDestination, String maxAmount)
      throws IOException {
    try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileDestination + fileType))) {
      bw.write("maxAmount=" + maxAmount);
    } catch (IOException ex) {
      throw new IOException("Error writing to file: " + fileDestination);
    }
  }

  /**
   * Method for reading (getting) a Budget from a file.
   *
   * @param fileDestination the name of the file you want to read from.
   * @return the GeneralBudget from the file.
   * @throws IOException if an input or output exception occurred.
   */
  public static GeneralBudget readGeneralBudgetFromFile(String fileDestination) throws IOException {
    GeneralBudget generalBudget = null;
    double maxAmount = 0;
    double budgetAmount = 0;
    ExpenseCategory expenseCategory = null;
    String budgetDescription = null;

    try (BufferedReader br = new BufferedReader(new FileReader(fileDestination + fileType))) {
      String line;
      String nextLine = br.readLine();
      while ((line = nextLine) != null) {
        nextLine = br.readLine();
        if (line.startsWith(FileHandlingBudget.maxAmount)) {
          maxAmount = Double.parseDouble(line.replace(FileHandlingBudget.maxAmount, ""));
        } else if (line.startsWith(FileHandlingBudget.budgetAmount)) {
          budgetAmount = Double.parseDouble(line.replace(FileHandlingBudget.budgetAmount, ""));
        } else if (line.startsWith(FileHandlingBudget.budgetCategory)) {
          line = line.replace(FileHandlingBudget.budgetCategory, "");
          expenseCategory = switch (line) {
            case "FOOD" -> ExpenseCategory.FOOD;
            case "RENT" -> ExpenseCategory.RENT;
            case "CLOTHES" -> ExpenseCategory.CLOTHES;
            case "BOOKS" -> ExpenseCategory.BOOKS;
            case "HEALTH" -> ExpenseCategory.HEALTH;
            case "ACTIVITIES" -> ExpenseCategory.ACTIVITIES;
            default -> ExpenseCategory.OTHER;
          };
        } else if (line.startsWith(FileHandlingBudget.budgetDescription)) {
          budgetDescription = line.replace(FileHandlingBudget.budgetDescription, "");
        }
        if (line.isEmpty() || (nextLine == null)) {
          if (generalBudget == null) {
            generalBudget = new GeneralBudget(maxAmount);
          } else {
            generalBudget.addToBudget(budgetAmount, budgetDescription, expenseCategory);
          }
        }
      }
    }
    return generalBudget;
  }
}
