package no.ntnu.idatt1002.demo.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.recipes.FileHandler;
import no.ntnu.idatt1002.demo.data.recipes.FoodItem;
import no.ntnu.idatt1002.demo.data.recipes.IngredientsAtHand;
import no.ntnu.idatt1002.demo.data.recipes.Recipe;
import no.ntnu.idatt1002.demo.data.recipes.RecipeRegister;

/**
 * The controller manages the view SuggestRecipes.fxml that displays a list of food types that the
 * user has available, called 'fridge' and four tiles showing the four recipes that requires the
 * lowest number of ingredients that are currently not at hand. These 'recipe tiles' are dynamically
 * loaded and update whenever food types are removed or added to the 'fridge'. Food types are
 * removed from the fridge by selecting it from the fridge-list and pressing 'Remove' and added by
 * pressing 'Add to Fridge' upon which a new window is opened. If the ingredients at hand list is
 * empty or the recipe register is empty or not existing, the page will still load, but the 'fridge'
 * will be empty and no recipe suggestions will appear.
 *
 * @author hannesofie
 */
public class SuggestRecipesController implements Initializable {

  IngredientsAtHand ingredientsAtHand;
  RecipeRegister recipeRegister;

  @FXML
  private Button showAllBtn;

  @FXML
  private Button goBackBtn;

  @FXML
  private ListView<String> fridgeList;

  @FXML
  private GridPane recipeGrid;

  @FXML
  private Label missingList;

  private final ArrayList<VBox> currentRecipeTiles = new ArrayList<>(4);

  private String filePath;


  /**
   * The initialize method of the controller takes in a URL (location) and ResourceBundle(resources)
   * to initialize the controller once its root element has been processed. The method also provides
   * the controller with an up do date IngredientsAtHand object by calling 'readIngredientsAtHand',
   * as well as an up-to-date RecipeRegister by reading it from file. Once this data is in place,
   * the method calls 'setRecipeTiles()' to populate the grid with tiles representing the four
   * recipes requiring the least ingredients not at hand.
   *
   * @param url            The location to resolve the relative paths to the root object.
   * @param resourceBundle Resources used to localize the root object.
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    String path = System.getProperty("user.home");
    filePath = path + "/BudgetBuddyFiles/recipes/";

    File atHand = new File(filePath + "Fridge.register");
    if (!atHand.exists()) {
      try {
        atHand.createNewFile();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    File recipesFile = new File(filePath + "Recipes.register");
    if (!recipesFile.exists()) {
      try {
        recipesFile.createNewFile();

        recipeRegister = FileHandler.readRecipeRegister("src/main/resources/recipes/Recipes");
        FileHandler.writeRegister(recipeRegister, filePath + "Recipes");
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    try {
      readIngredientsAtHand();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    try {
      recipeRegister = FileHandler.readRecipeRegister(filePath + "Recipes");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    if (recipeRegister == null) {
      recipeRegister = new RecipeRegister();
    }

    missingList.setVisible(false);
    setRecipeTiles();
  }

  /**
   * The addIngredient method fires when the user presses the 'Add to Fridge' button of the UI. It
   * then loads a pop-up dialog that appear over the current window. The dialog is managed by a
   * different controller that adds and stores new food types in the fridge. Once the dialog is
   * excited, this method updates the ingredientsAtHand object and the suggested recipes of this
   * controller.
   *
   * @throws IOException If the method is unable to set the location based on the provided path of
   *                     the dialog pane.
   */
  @FXML
  private void addIngredient(ActionEvent event) throws IOException {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(getClass().getResource("/view/AddIngredient.fxml"));
    DialogPane addIngredientPane = loader.load();

    Dialog<ButtonType> dialog = new Dialog<>();
    dialog.setDialogPane(addIngredientPane);
    dialog.setTitle("Add ingredient to fridge");

    //Position popup left, allowing for quick comparison between fridge and add list
    Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
    dialog.setX((bounds.getWidth() - dialog.getWidth()) / 3);
    dialog.setY((bounds.getWidth() - dialog.getWidth()) / 2);

    Optional<ButtonType> clickedButton = dialog.showAndWait();

    if (clickedButton.isPresent() && clickedButton.get() == ButtonType.CLOSE) {
      readIngredientsAtHand();
      setRecipeTiles();
    }
  }

  /**
   * The method fires once the user presses the 'Remove' button of the UI. It then validates that a
   * recipe in the fridge-list was selected before going ahead and removing if from the
   * IngredientsAtHand object. If it has removed an ingredient, it calls 'storeIngredientsAtHand'
   * and 'setRecipeTiles' to store the change to file and update the recipe suggestions.
   *
   * @throws IOException If writing the altered IngredientsAtHand object to file fails.
   */
  @FXML
  private void removeFromFridge() throws IOException {
    String toRemove = fridgeList.getSelectionModel().getSelectedItem();
    if (toRemove != null) {
      ingredientsAtHand.removeIngredient(
          FoodItem.valueOf(toRemove.replace(" ", "_").toUpperCase()));
      storeIngredientsAtHand();
      setRecipeTiles();
    }
  }


  /**
   * The method calls the 'pickBestFits' method of the recipe register object with 4 as requested
   * number of recipes and the ingredients at hand object as inputs. The resulting recipes are
   * stored in an ArrayList. The recipes in the list are iterated through, and for each, a VBox
   * object as defined in the view RecipeTile.fxml is created and filled with each recipe's data by
   * calling 'setData' of the controller of recipeTile.fxml. Each VBox is equipped with some
   * hover-effects by calling 'setHoverEffect' and then added to the grid of SuggestRecipes.fxml.
   * The current tiles(VBox) are stored in an ArrayList to make sure to delete these VBox-object
   * upon updating to prevent then from layering on top of each other.
   */
  private void setRecipeTiles() {
    int numberOfTiles = 4;
    ArrayList<Recipe> recipes = recipeRegister.pickBestFits(numberOfTiles, ingredientsAtHand);

    int i = 0;
    int j = 0;
    int counter = 0;

    for (Recipe r : recipes) {
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(getClass().getResource("/view/RecipeTile.fxml"));

      if (i > 1) {
        j++;
        i = 0;
      }

      try {
        VBox vbox = loader.load();
        RecipeTileController recipeTileController = loader.getController();
        recipeTileController.setData(r);

        if (currentRecipeTiles.size() < recipes.size()) {
          currentRecipeTiles.add(vbox);
        } else {
          recipeGrid.getChildren().remove(currentRecipeTiles.get(counter));
          currentRecipeTiles.set(counter, vbox);
        }

        setHoverEffect(vbox, r);
        recipeGrid.add(vbox, i, j);

      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      i++;
      counter++;
    }
  }


  /**
   * This method is used to equip a VBox with a hover effect that displays a label at the bottom of
   * the view that lists the missing ingredients of the recipe associated with the VBox whenever the
   * mouse hovers over the VBox. If no ingredients are missing no label should be shown.
   *
   * @param vbox   A VBox object to which the hover effect is attached.
   * @param recipe The recipe associated with the VBox and therefore source of the content of the
   *               label displayed upon hovering.
   */
  private void setHoverEffect(VBox vbox, Recipe recipe) {

    vbox.setOnMouseEntered(event -> {
      if (recipe.getMissingIngredients() == 0) {
        missingList.setText("");
        missingList.setVisible(false);
      } else {
        missingList.setText("Missing: " + String.join(", ", recipe.getMissingList()));
        missingList.setVisible(true);
      }
    });

    vbox.setOnMouseExited(event -> {
      missingList.setText("");
      missingList.setVisible(false);
    });
  }


  /**
   * The method fires when one of the scene-switching buttons are clicked by the user. It then uses
   * the identity of the clicked button to set the location of the scene switch between 'All
   * Recipes' and 'Main Menu', and loads this scene.
   *
   * @param event The event that causes the method to be called.
   * @throws IOException If the method is unable to load the locations at the provided paths.
   */
  @FXML
  private void switchScene(ActionEvent event) throws IOException {
    FXMLLoader loader = new FXMLLoader();
    if (event.getSource() == showAllBtn) {
      loader.setLocation(getClass().getResource("/view/AllRecipes.fxml"));
    } else if (event.getSource() == goBackBtn) {
      loader.setLocation(getClass().getResource("/view/MainMenuNew.fxml"));
    }
    Parent root = loader.load();
    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.setResizable(false);
    stage.show();
  }

  /**
   * The method reads and creates an ingredientsAtHand object by reading from the file with title
   * "Fridge" by using the static class recipe.FileHandler. If the file is non-existent or empty, a
   * new and empty IngredientsAtHand object is created and the fridge list is empty. Otherwise, the
   * fridge list is filled with the food types at hand sorted alphabetically by calling the method
   * 'updateFridge'.
   */
  private void readIngredientsAtHand() throws IOException {
    ingredientsAtHand = FileHandler.readIngredientsAtHand(filePath + "Fridge");
    if (ingredientsAtHand == null) {
      ingredientsAtHand = new IngredientsAtHand();
      fridgeList.setItems(FXCollections.observableArrayList(new ArrayList<>()));
    } else {
      updateFridge();
    }
  }

  /**
   * The method makes use of the static class 'recipe.FileHandler' to write the IngredientsAtHand
   * object of the controller to a file named 'Fridge.register'.  Once the object is written to
   * file, the fridge list is updated by calling 'updateFridge'.
   *
   * @throws IOException If the method fails to write the IngredientsAtHand object to file.
   */
  private void storeIngredientsAtHand() throws IOException {
    FileHandler.writeIngredientsAtHand(ingredientsAtHand, filePath + "Fridge");
    updateFridge();
  }

  /**
   * The method 'updateFridge' supports other methods by setting the fridge's content based on the
   * IngredientsAtHand object of the Controller and adding them to the fridge list in alphabetical
   * order.
   */
  private void updateFridge() {
    ObservableList<String> fridge = FXCollections.observableArrayList(
        ingredientsAtHand.getIngredientsAtHand().stream().map(foodItem -> foodItem.label).toList());
    fridgeList.setItems(fridge.sorted());
  }
}
