package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingSelectedBudget;
import no.ntnu.idatt1002.demo.data.economics.Expense;

/**
 * Controller for the create budget scene in the application. This controller manages everything
 * related to creating a new budget, from creating new directories and incomes, to updating the
 * SelectedBudget.current file that stores the current budget.
 */
public class CreateBudgetController {

  private String currentMonth;

  private Expense expense;

  private String budgetName;

  /**
   * The ok button in the dialog window.
   */
  @FXML
  public Button okBtn;

  /**
   * The cancel button in the dialog window.
   */
  @FXML
  public Button cancelBtn;

  @FXML
  private TextField nameField;

  @FXML
  private Label errorMsg;

  private String filePath;

  /**
   * Initializes the window. Finds the current month, used to make the created budget's name. Adds
   * an event filter to the okBtn.
   */
  @FXML
  public void initialize() {

    String path = System.getProperty("user.home");
    filePath = path + "/BudgetBuddyFiles/budgets";

    currentMonth = String.valueOf(LocalDate.now().getMonth());
    okBtn.addEventFilter(
        ActionEvent.ACTION, event -> {
          if (nameField.getText().equals("")) {
            errorMsg.setOpacity(1);
            event.consume();
          }
        });
  }

  /**
   * Handles button presses on the OK button.
   *
   * @param event A button press on the OK button.
   */
  @FXML
  public void pressOkBtn(ActionEvent event) {
    String title = "Confirm name";
    String header = "Are you sure you to use this name?";
    String content = "The name cannot be changed later";

    //Create the budget name
    budgetName = nameField.getText();

    //Attempts to create a new directory and new files for the new budget
    if (!createNewFiles(budgetName)) {
      updateCurrentFile("", "");
      errorMsg.setText("A budget of the same name already exists");
      errorMsg.setOpacity(1);
      return;
    }

    //Requires confirmation for creating the new budget.
    Optional<ButtonType> isConfirmed = showConfirmationDialog(title, header, content);
    if (isConfirmed.isPresent() && isConfirmed.get() == ButtonType.OK) {
      updateCurrentFile(currentMonth, budgetName);
    } else {
      updateCurrentFile("", "");
    }

    final Node source = (Node) event.getSource();
    ((Stage) source.getScene().getWindow()).close();
  }

  /**
   * Handles presses on the cancel button.
   *
   * @param event A button press on the cancel button.
   */
  @FXML
  public void pressCancelBtn(ActionEvent event) {
    updateCurrentFile("", "");
    final Node source = (Node) event.getSource();
    ((Stage) source.getScene().getWindow()).close();
  }

  /**
   * Returns an optional, which is a popup alert box, asking for confirmation for deleting an
   * entry.
   *
   * @return An alert box, asking for confirmation for deleting the selected entry of the tableview.
   */
  @FXML
  private Optional<ButtonType> showConfirmationDialog(String title, String header, String content) {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);

    return alert.showAndWait();
  }

  /**
   * Displays an error message dialog box with a customizable title, header and content.
   *
   * @param title   The dialog title.
   * @param header  The dialog header.
   * @param content The dialog content.
   */
  private void showErrorMsgBox(String title, String header, String content) {
    Alert alert = new Alert(AlertType.ERROR);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);

    alert.showAndWait();
  }

  /**
   * Method for creating a new directory for the newly created budget, along with three files for
   * storing budget, income and expense data.
   *
   * @param budgetName The name of the budget.
   * @return True, if the directories are successfully created. Else, returns false.
   */
  public boolean createNewFiles(String budgetName) {
    boolean empty;
    try {

      String path = System.getProperty("user.home");
      filePath = path + "/BudgetBuddyFiles/budgets";

      //TODO: Fiks at måned kommer med i navnet.
      empty = FileHandlingSelectedBudget.createBudgetDirectory(
              filePath + "/" + currentMonth + budgetName);

      FileHandlingSelectedBudget.createNewIncomeFile(filePath + "/"  + currentMonth + budgetName,
          "Income");
      FileHandlingSelectedBudget.createNewExpenseFile(filePath + "/"  + currentMonth + budgetName,
          "Expense");
      FileHandlingSelectedBudget.createNewBudgetFile(filePath + "/"  + currentMonth + budgetName,
          "Budget");
    } catch (IOException ioe) {
      empty = false;
      showErrorMsgBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }
    return empty;
  }

  /**
   * Method for updating the currently selected budget in the file that stores which budget is
   * currently selected.
   *
   * @param currentMonth The month of the year.
   * @param budgetName   The name of the budget.
   */
  public void updateCurrentFile(String currentMonth, String budgetName) {
    try {

      FileHandlingSelectedBudget.updateSelectedBudget(currentMonth + budgetName,
          filePath + "/SelectedBudget");
    } catch (IOException ioe) {
      showErrorMsgBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }
  }
}
