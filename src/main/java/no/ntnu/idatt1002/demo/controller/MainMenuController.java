package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.budget.BudgetItem;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingBudget;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingSelectedBudget;
import no.ntnu.idatt1002.demo.data.budget.GeneralBudget;
import no.ntnu.idatt1002.demo.data.economics.ExpenseRegister;
import no.ntnu.idatt1002.demo.data.economics.FileHandling;
import no.ntnu.idatt1002.demo.data.economics.IncomeRegister;
import no.ntnu.idatt1002.demo.data.economics.Overview;

/**
 * Controller for the main menu of the application. Handles all inputs related to buttons and
 * actively updates the progress overview.
 *
 * @author Harry Linrui Xu
 * @since 29.03.2023
 */
public class MainMenuController {

  @FXML
  private Label balanceLbl;

  @FXML
  private Button budgetBtn;

  @FXML
  private DatePicker date;

  @FXML
  private Button foodBtn;

  @FXML
  private Button incomeExpenseBtn;

  @FXML
  private ProgressBar mainBar;

  @FXML
  private Button returnBtn;

  @FXML
  private Label title;

  @FXML
  private Label usageLbl;

  @FXML
  private Label daysLeftLbl;

  @FXML
  private VBox miniBarScroll;

  GeneralBudget generalBudget;

  ExpenseRegister expenseRegister;

  IncomeRegister incomeRegister;

  Overview overview;

  private String filePath;

  /**
   * Initializes the registers and overviews in addition to all dynamic labels and progress bars.
   */
  @FXML
  public void initialize() {

    String path = System.getProperty("user.home");
    filePath = path + "/BudgetBuddyFiles/budgets/";

    //Initialize all registers + overview
    //TODO: instantiate sub-directories?
    try {
      incomeRegister = loadIncomeDataFromFile(
          filePath + FileHandlingSelectedBudget
              .readSelectedBudget(filePath + "SelectedBudget") + "/Income");
      expenseRegister = loadExpenseDataFromFile(
          filePath + FileHandlingSelectedBudget
              .readSelectedBudget(filePath + "SelectedBudget") + "/Expense");
      generalBudget = loadBudgetDataFromFile(
          filePath + FileHandlingSelectedBudget
              .readSelectedBudget(filePath + "SelectedBudget") + "/Budget");
      overview = new Overview(incomeRegister, expenseRegister, generalBudget);
    } catch (IOException | IllegalArgumentException ioe) {
      showErrorDialogBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }

    mainBar.setStyle("-fx-accent:  rgb(48,215,106);");

    //Ensures all budget categories have been added
    if (generalBudget.getBudgetItems().size() != 4) {
      generalBudget.addUnusedCategories();
    }
    //Set progress and balance
    refreshProgressBars();
    //Set values of labels
    refreshLabels();

    //Make calendar uneditable
    formatDatePicker();
    if (generalBudget.getDaysLeftOfBudgetPeriod() == 0) {
      daysLeftLbl.setOpacity(0);
      //Disable buttons once budget period has passed
      foodBtn.setDisable(true);
      incomeExpenseBtn.setDisable(true);
      budgetBtn.setDisable(true);
    }
    daysLeftLbl.setText("Days left of budget: " + generalBudget.getDaysLeftOfBudgetPeriod());
  }

  /**
   * Method for setting the values of the dynamic labels.
   */
  private void refreshLabels() {
    double maxAmount = generalBudget.getMaxAmount();
    double expenseSum = expenseRegister.getTotalSum();

    //Displaying month
    title.setText("BUDGET " + (LocalDate.now().getMonth()));
    double balance = maxAmount - expenseSum;
    //Set balance
    balanceLbl.setText("Balance: " + (balance));
    //Displaying how much of the monthly budget has been spent.
    usageLbl.setText("You have used " + expenseSum + " out of " + generalBudget.getMaxAmount());
  }

  /**
   * Sets the progress of the progress bars to their most updated data.
   */
  private void refreshProgressBars() {
    //mainBar.setProgress(expenseRegister.getTotalSum()/generalBudget.getMaxAmount());
    mainBar.setProgress(1 - expenseRegister.getTotalSum() / generalBudget.getMaxAmount());

    //double progress = 1+(leftovers-budgetItem.getBudgetAmount())/ budgetItem.getBudgetAmount();

    if (mainBar.getProgress() <= 0.0f) {
      mainBar.setStyle("-fx-accent: #fa5959;");
      mainBar.setProgress(1);
    } else if (mainBar.getProgress() <= 0.25f) {
      mainBar.setStyle("-fx-accent: #ffb000;");
    }

    ArrayList<BudgetItem> budgets = generalBudget.getBudgetItems().stream()
        .filter(budget -> budget.getBudgetAmount() > 0)
        .collect(Collectors.toCollection(ArrayList::new));

    for (BudgetItem bi : budgets) {
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(getClass().getResource("/view/BudgetBar.fxml"));

      try {
        HBox hbox = loader.load();

        BudgetBarController budgetBarController = loader.getController();
        double leftovers = overview.getBudgetItemMinusExpense(bi.getBudgetCategory());
        budgetBarController.setData(bi, leftovers);
        miniBarScroll.getChildren().add(hbox);

      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * Method that either reads data from a file with which it fills an income register, if older
   * changes exist, or instantiates an income register if the file is empty.
   *
   * @param fileDestination The name of the file that is being read from.
   * @return An object of type IncomeRegister.
   * @throws IOException If an error occurs while reading from the file.
   */
  public IncomeRegister loadIncomeDataFromFile(String fileDestination) throws IOException {
    //Instantiate incomeRegister
    if (FileHandling.isEmpty(fileDestination)) {
      incomeRegister = new IncomeRegister();
    } else { //Load previous income register
      try {
        incomeRegister = FileHandling.readIncomeRegisterFromFile(fileDestination);
      } catch (IOException e) {
        showErrorDialogBox("Loading error", "Could not load income register",
            "The income register could not be loaded");
      }
    }
    return incomeRegister;
  }

  /**
   * Method that either reads data from a file with which it fills an expense register, if older
   * changes exist, or instantiates an expense register if the file is empty.
   *
   * @param fileDestination The name of the file that is being read from.
   * @return An object of type IncomeRegister.
   * @throws IOException If an error occurs while reading from the file.
   */
  public ExpenseRegister loadExpenseDataFromFile(String fileDestination) throws IOException {
    //Instantiate expense register
    if (FileHandling.isEmpty(fileDestination)) {
      expenseRegister = new ExpenseRegister();
    } else {
      try {
        expenseRegister = FileHandling.readExpenseRegisterFromFile(fileDestination);
      } catch (IOException e) {
        showErrorDialogBox("Loading error", "Expense register could not be loaded",
            "The expense register failed to load");
      }
    }
    return expenseRegister;
  }

  /**
   * Method that either reads data from a file with which it fills a budget register, if this is an
   * old budget, or instantiates a budget register if this is a new budget.
   *
   * @param fileDestination The name of the file that is being read from.
   * @return An object of type GeneralBudget.
   * @throws IOException If an error occurs while reading from the file.
   */
  public GeneralBudget loadBudgetDataFromFile(String fileDestination) throws IOException {
    //Instantiate new budget
    if (FileHandlingBudget.isEmpty(fileDestination)) {
      generalBudget = new GeneralBudget(1000);
    } else { //Load previous budget
      try {
        generalBudget = FileHandlingBudget.readGeneralBudgetFromFile(fileDestination);
      } catch (IOException e) {
        showErrorDialogBox("Loading error", "The general budget"
            + "could not be loaded", "");
      }
    }
    return generalBudget;
  }


  /**
   * Method for disabling the date picker, yet having its opacity at max.
   */
  private void formatDatePicker() {
    date.setValue(LocalDate.now());
    date.setDisable(true);
    date.setStyle("-fx-opacity: 1");
    date.getEditor().setStyle("-fx-opacity: 1");
  }

  /**
   * Displays an error message dialog box with a customizable title, header and content.
   *
   * @param title   The dialog title.
   * @param header  The dialog header.
   * @param content The dialog content.
   */
  public void showErrorDialogBox(String title, String header, String content) {
    Alert alert = new Alert(AlertType.ERROR);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);
    alert.showAndWait();
  }

  /**
   * Returns an optional, which is a popup alert box, asking for confirmation for deleting an
   * entry.
   *
   * @param title   The dialog box title.
   * @param header  The dialog box header.
   * @param content The dialog box content.
   * @return An alert box, asking for confirmation for deleting the selected entry of the tableview.
   */
  public Optional<ButtonType> showConfirmationDialog(String title, String header, String content) {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);

    return alert.showAndWait();
  }

  /**
   * Switches the scenes from the main menu to a target scene.
   *
   * @param event A button press that takes on to another scene.
   * @throws IOException if an input or output exception occurs
   */
  @FXML
  private void switchScene(ActionEvent event) throws IOException {
    //Find new location
    FXMLLoader loader = new FXMLLoader();
    if (event.getSource() == foodBtn) {
      loader.setLocation(getClass().getResource("/view/SuggestRecipes.fxml"));
    } else if (event.getSource() == incomeExpenseBtn) {
      loader.setLocation(getClass().getResource("/view/IncomeAndExpenses.fxml"));
    } else if (event.getSource() == budgetBtn) {
      loader.setLocation(getClass().getResource("/view/BudgetNewest.fxml"));
    } else if (event.getSource() == returnBtn) {
      Optional<ButtonType> isConfirmed = showConfirmationDialog("Return to first menu",
          "Are you sure you want to return "
              + "to the start menu?", "Your changes will be saved");
      if (isConfirmed.isPresent() && isConfirmed.get() == ButtonType.OK) {
        loader.setLocation(getClass().getResource("/view/FirstMenu.fxml"));
      } else {
        return;
      }
    }
    //Load new location
    Parent root = loader.load();
    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    Scene scene = new Scene(root);
    stage.setResizable(false);
    stage.setScene(scene);

    //Centralize the new screen
    Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
    stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
    stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);

    stage.setResizable(false);
    stage.show();
  }
}

