package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.recipes.Recipe;
import no.ntnu.idatt1002.demo.data.recipes.RecipeIngredient;

/**
 * The RecipeController manages the view Recipe.fxml that displays one particular recipe in full
 * detail. It has the recipe's name as headline, lists the ingredients of the recipe in a scroll
 * pane to the left and the instructions in a scroll pane to the right.
 *
 * @author hannesofie
 */
public class RecipeController {

  @FXML
  private Label recipeName;

  @FXML
  private Text instructions;

  @FXML
  private VBox ingredientList;

  @FXML
  private Button toSuggestionsBtn;

  @FXML
  private Button allBtn;

  @FXML
  private ObservableList<RecipeIngredient> ingredients;

  /**
   * The setData method takes a Recipe object as parameter and sets the information of the view
   * accordingly. Specifically, the recipe name is set as headline, it's ingredients are stored in
   * an ObservableList and it's instructions are set as text in the scroll pane to the right.
   * Finally, the method 'setIngredientTiles' is called to fill the ingredient list with the
   * component IngredientTile for each ingredient.
   *
   * @param recipeOfInterest A Recipe object to present in full detail on the Recipe.fxml view.
   */
  public void setData(Recipe recipeOfInterest) {
    if (recipeOfInterest != null) {
      recipeName.setText(recipeOfInterest.getName());
      instructions.setText(recipeOfInterest.getInstructions());
      ingredients = FXCollections.observableArrayList(recipeOfInterest.getIngredientList());
    } else {
      throw new IllegalArgumentException("The recipe of interest is null");
    }
    setIngredientTiles();
  }


  /**
   * The setIngredientTiles method uses the ObservableList of RecipeIngredients and creates a pane
   * according to the component view IngredientTile.fxml for each and adds them to the ingredient
   * list of the Recipe.fxml view.
   */
  private void setIngredientTiles() {

    for (RecipeIngredient ri : ingredients) {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/IngredientTile.fxml"));

      try {
        Pane pane = loader.load();
        IngredientTileController ingredientTileController = loader.getController();
        ingredientTileController.setData(ri);

        ingredientList.getChildren().add(pane);

      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * The method fires when one of the scene-switching buttons are clicked by the user. It then uses
   * the identity of the clicked button to set the location of the scene switch between 'All
   * Recipes' and 'To Suggested Recipes', and loads this scene.
   *
   * @param event The event that causes the method to be called.
   * @throws IOException If the method is unable to load the locations at the provided paths.
   */
  @FXML
  private void switchScene(ActionEvent event) throws IOException {
    FXMLLoader loader = new FXMLLoader();

    if (event.getSource() == allBtn) {
      loader.setLocation(getClass().getResource("/view/AllRecipes.fxml"));
    } else if (event.getSource() == toSuggestionsBtn) {
      loader.setLocation(getClass().getResource("/view/SuggestRecipes.fxml"));
    }

    Parent root = loader.load();
    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.setResizable(false);
    stage.show();
  }
}
