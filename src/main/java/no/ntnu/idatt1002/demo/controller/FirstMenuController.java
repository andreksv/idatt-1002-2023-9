package no.ntnu.idatt1002.demo.controller;

import java.io.File;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Dialog;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingSelectedBudget;
import no.ntnu.idatt1002.demo.data.economics.Income;
import no.ntnu.idatt1002.demo.data.recipes.RecipeRegister;

/**
 * Controller for the FirstMenu view. Handles user input on the buttons in the scene.
 */
public class FirstMenuController {

  private String filePath;

  private RecipeRegister startRegister;

  //TODO:

  /**
   * Initializes as soon as the scene is loaded.
   */
  @FXML
  public void initialize() throws IOException {
    String path = System.getProperty("user.home");
    String buddyPath = path + "/BudgetBuddyFiles";
    filePath = buddyPath + "/budgets";

    File budget = new File(buddyPath + "/budgets");
    if (!budget.exists()) {
      budget.mkdirs();
    }

    File archive = new File(buddyPath + "/budgets/Archive.archive");
    if (!archive.exists()) {
      archive.createNewFile();
    }

    File economics = new File(buddyPath + "/economics");
    if (!economics.exists()) {
      economics.mkdirs();
    }
    File recipes = new File(buddyPath + "/recipes");
    if (!recipes.exists()) {
      recipes.mkdirs();
    }
  }

  /**
   * Brings up popup for creating a new budget.
   *
   * @param event A button press on the OK button.
   */
  @FXML
  public void createBudget(ActionEvent event) {

    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(getClass().getResource("/view/CreateBudget.fxml"));

    String dialogTitle = "Create budget";

    Dialog<String> dialog = new Dialog<>();
    dialog.initModality(Modality.APPLICATION_MODAL);

    try {
      // Set the Dialog's content to the loaded FXML file
      dialog.getDialogPane().setContent(loader.load());
    } catch (IOException e) {
      showErrorDialogBox("Loading", "Error in loading dialog box", "Could not load"
          + "the AddExpense window");
    }

    // Get the controller for the loaded FXML file
    CreateBudgetController createBudgetController = new CreateBudgetController();
    // Show the Dialog and wait for the user to close it
    dialog.setTitle(dialogTitle);

    //Wait for user input
    dialog.showAndWait();

    try {
      //Only proceeds to next scene if there is a budget selected
      if (FileHandlingSelectedBudget
          .readSelectedBudget(filePath + "/SelectedBudget") != null) {
        switchNext(event);
      }
    } catch (IOException ioe) {
      showErrorDialogBox("", "", "");
    }
  }

  /**
   * Switches to the next scene after creating a new budget.
   *
   * @param event A button press.
   * @throws IOException if an input or output error occurred
   */
  private void switchNext(ActionEvent event) throws IOException {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(FirstMenuController.class.getResource("/view/dualList.fxml"));
    Parent root = loader.load();
    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.show();
  }

  /**
   * Displays an error message dialog box with a customizable title, header and content.
   *
   * @param title   The dialog title.
   * @param header  The dialog header.
   * @param content The dialog content.
   */
  private void showErrorDialogBox(String title, String header, String content) {
    Alert alert = new Alert(AlertType.ERROR);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);

    alert.showAndWait();
  }

  /**
   * Method that brings up a popup, allowing users to choose which budget to select. The scene is
   * then switched to the main menu of that budget.
   *
   * @param event A button press.
   */
  public void switchMainMenu(ActionEvent event) {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(FirstMenuController.class.getResource("/view/SelectBudget.fxml"));

    String dialogTitle = "Select budget";
    // Load the FXML file for your dialog box
    Dialog<Income> dialog = new Dialog<>();

    try {
      // Set the Dialog's content to the loaded FXML file
      dialog.getDialogPane().setContent(loader.load());
    } catch (IOException e) {
      showErrorDialogBox("Loading error", "Error in loading dialog box", "Could not load"
          + "the AddIncome window");
    }

    //Sets the title of the dialog box
    dialog.setTitle(dialogTitle);
    // Show the Dialog and wait for the user to close it
    dialog.showAndWait();

    try {
      //Only switches scenes if there is a budget that can be selected.
      if (FileHandlingSelectedBudget
          .readSelectedBudget(filePath + "/SelectedBudget") != null) {
        switchChosenBudget(event);
      }
    } catch (IOException ioe) {
      showErrorDialogBox("Loading error", "Could not switch to main menu", "");
    }
  }

  /**
   * Method for switching to the main menu of the chosen budget.
   *
   * @param event A button press.
   * @throws IOException if an input or output error occurred
   */
  private void switchChosenBudget(ActionEvent event) throws IOException {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(FirstMenuController.class.getResource("/view/MainMenuNew.fxml"));
    Parent root = loader.load();
    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    Scene scene = new Scene(root);
    stage.setResizable(false);
    stage.setScene(scene);

    //Centralize the new screen
    Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
    stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
    stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);

    stage.show();
  }

  /**
   * Method that closes the first menu.
   *
   * @param actionEvent Button press.
   */
  public void closeButton(ActionEvent actionEvent) {
    final Node source = (Node) actionEvent.getSource();
    final Stage stage = (Stage) source.getScene().getWindow();
    stage.close();
  }
}
