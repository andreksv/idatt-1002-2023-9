package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.budget.BudgetRegister;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingBudgetArchive;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingSelectedBudget;

/**
 * Class for selecting a budget from the archive.
 *
 * @author Harry Linrui Xu
 * @since 19.04.2023
 */
public class SelectBudgetController {

  @FXML
  Button okBtn;

  @FXML
  private Label errorMsg;

  @FXML
  private ListView<String> budgetListView;

  private BudgetRegister budgetRegister;
  private String filePath;

  /**
   * Initializes the view when it is loaded. Prepares the view by adding event filters,
   * instantiating the budget register and setting the list view.
   */
  @FXML
  public void initialize() {
    String path = System.getProperty("user.home");
    filePath = path + "/BudgetBuddyFiles/budgets";

    //Prevent users from choosing nothing
    okBtn.addEventFilter(
        ActionEvent.ACTION, event -> {
          if (budgetListView.getSelectionModel().getSelectedItem() == null) {
            errorMsg.setOpacity(1);
            event.consume();
          }
        });

    //Load budget register from file.
    try {
      if (FileHandlingBudgetArchive.isBudgetRegisterEmpty(filePath + "/Archive")) {
        budgetRegister = new BudgetRegister();
      } else {
        budgetRegister = FileHandlingBudgetArchive.readBudgetArchive(filePath + "/Archive");
      }
    } catch (IOException ioe) {
      showErrorDialogBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }
    ObservableList<String> budgetList = FXCollections.observableList(
        budgetRegister.getBudgetNames());
    budgetListView.setItems(budgetList);

    //Double clicking the entry also chooses it
    budgetListView.setOnMouseClicked(mouseEvent -> {
      if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
        if (mouseEvent.getClickCount() == 2) {
          selectBudget(mouseEvent);
        }
      }
    });
  }

  /**
   * Selects the budget in the budget list view and stores this value as the currently selected
   * budget.
   *
   * @param event Double mouseclick or a click on the ok button.
   */
  @FXML
  public void selectBudget(Event event) {
    try {
      String name = budgetListView.getSelectionModel().getSelectedItem();
      FileHandlingSelectedBudget.updateSelectedBudget(name, filePath + "/SelectedBudget");
    } catch (IOException ioe) {
      showErrorDialogBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }

    final Node source = (Node) event.getSource();
    ((Stage) source.getScene().getWindow()).close();
  }

  /**
   * Closes the dialog box and clears the currently selected budget.
   *
   * @param event Button press on cancel button
   */
  @FXML
  public void exitWindow(ActionEvent event) {
    try {
      FileHandlingSelectedBudget.clearSelectedBudget(filePath + "/SelectedBudget");
    } catch (IOException ioe) {
      showErrorDialogBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }
    final Node source = (Node) event.getSource();
    ((Stage) source.getScene().getWindow()).close();
  }

  /**
   * Displays an error message dialog box with a customizable title, header and content.
   *
   * @param title   The dialog title.
   * @param header  The dialog header.
   * @param content The dialog content.
   */
  public void showErrorDialogBox(String title, String header, String content) {
    Alert alert = new Alert(AlertType.ERROR);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);
    alert.showAndWait();
  }
}

