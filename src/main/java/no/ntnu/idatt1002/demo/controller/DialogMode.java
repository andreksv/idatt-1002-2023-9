package no.ntnu.idatt1002.demo.controller;

/**
 * Enum for distinguishing between adding or editing an item in a tableview.
 *
 * @author Harry Linrui Xu
 * @since 11.3.2023
 */
enum DialogMode {
  ADD, EDIT
}
