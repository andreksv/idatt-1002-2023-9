package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.recipes.FileHandler;
import no.ntnu.idatt1002.demo.data.recipes.Recipe;
import no.ntnu.idatt1002.demo.data.recipes.RecipeRegister;


/**
 * The RecipeTileController manages the tile components that are used to dynamically display a set
 * of recommended recipes in terms of their name and number of missing ingredients. Each tile is a
 * VBox containing this information.
 *
 * @author hannesofie
 */
public class RecipeTileController implements Initializable {

  @FXML
  private Label nameTag;

  @FXML
  private Label missingTag;

  private RecipeRegister recipeRegister;
  private String filePath;

  /**
   * The initialize method of the controller takes in a URL (location) and ResourceBundle(resources)
   * to initialize the controller once its root element has been processed. The method reads and
   * creates an instance of the RecipeRegister class from file and makes sure that the name of the
   * recipe will be able to wrap on the tile if it is longer than the tile width.
   *
   * @param url            The location to resolve the relative paths to the root object.
   * @param resourceBundle Resources used to localize the root object.
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {

    String path = System.getProperty("user.home");
    filePath = path + "/BudgetBuddyFiles/recipes/";

    try {
      recipeRegister = FileHandler.readRecipeRegister(filePath + "Recipes");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    nameTag.setWrapText(true);
  }

  /**
   * The tileClick method is triggered by an Action event fired once the user presses the tile. The
   * event is handled by accessing the Recipe object represented by the pressed tile and setting the
   * loader location to the Recipe.fxml view. It then loads the controller of the Recipe.fxml view
   * and calls it's 'setData' method with the recipe as parameter. The scene is then changed to
   * Recipe.fxml that shows the details of that particular recipe.
   *
   * @param event The action event triggered when the user presses a recipe tile.
   * @throws IOException If the loader fails to set the location to the given path for Recipe.fxml.
   */
  @FXML
  private void tileClick(ActionEvent event) throws IOException {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(getClass().getResource("/view/Recipe.fxml"));

    String recipeName = this.nameTag.getText();
    Recipe recipeOfInterest = recipeRegister.getRecipe(recipeName);

    Parent root = loader.load();

    RecipeController recipeController = loader.getController();
    recipeController.setData(recipeOfInterest);

    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.show();
  }

  /**
   * The setData method takes in a Recipe object and sets the labels of the Recipe tiles according
   * to that recipe's name and number of missing ingredients.
   *
   * @param recipe A recipe object to represent as a recipe tile(RecipeTile.fxml)
   */
  public void setData(Recipe recipe) {
    nameTag.setText(recipe.getName());
    missingTag.setText(Integer.toString(recipe.getMissingIngredients()));
  }
}
