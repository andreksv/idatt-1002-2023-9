package no.ntnu.idatt1002.demo.controller;

import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.economics.Expense;
import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;

/**
 * Class that represents the popup dialog box that appears
 * whenever an expense is to be added or edited.
 * The dialog contains various fields that are used to
 * create a new expense or edit an existing expense.
 *
 * @author Harry Linrui Xu
 * @since 13.3.2023
 */
public class AddExpenseController {
  Expense newExpense = null; //the expense that is chosen when editing
  // or the expense that is created when adding

  Expense chosenExpense = null; //an expense that is meant to track
  // the old state of an expense before editing, in case cancel button is clicked

  @FXML
  private Button okBtn;

  @FXML
  private Text errorMsg;

  @FXML
  private DatePicker datePicker;

  @FXML
  private TextField descriptionField;

  @FXML
  private TextField amountField;

  @FXML
  private ComboBox<ExpenseCategory> categoryBox;

  @FXML
  private ComboBox<Boolean> recurringBox;

  /**
   * Initializes the category and recurring drop boxes by filling
   * them with all the values from the ExpenseCategory enum,
   * and the boolean values respectively, and the datepicker calendar.
   * It then sets them to the default values of GIFT, false and today respectively.
   */
  @FXML
  public void initialize() {
    //Set the possible values in a list.
    ObservableList<ExpenseCategory> expenseCategories = FXCollections.observableArrayList(
        ExpenseCategory.values());
    //Set the values inside the dropbox
    categoryBox.setItems(expenseCategories);
    //Set default value
    categoryBox.setValue(ExpenseCategory.FOOD);

    //Set the recurring field options
    ObservableList<Boolean> recurring = FXCollections.observableArrayList(true, false);
    recurringBox.setItems(recurring);
    recurringBox.setValue(false);

    //Set date to today
    datePicker.setValue(LocalDate.now());

    //Adding event filter to okBtn
    addEventFilters();
  }

  /**
   * Gets the value of the category box.

   * @return The chosen expense category.
   */
  public ExpenseCategory getCategory() {
    return categoryBox.getValue();
  }

  /**
   * Gets the value from the recurring box.

   * @return The chosen recurring boolean value.
   */
  public boolean isRecurring() {
    return recurringBox.getValue();
  }

  /**
   * Gets the newly created expense.

   * @return The new expense.
   */
  public Expense getNewExpense() {
    return this.newExpense;
  }

  /**
   * Binds the expense that is taken in as the argument with an expense
   * declared in this class. The expense of this class is instantiated
   * as a deep copy of the argument. Each attribute of their
   * attributes are then bounded. The text fields and category boxes
   * in the dialog window are then set to the values of the chosen expense,
   * as to not display empty values.

   * @param expense The expense that is chosen to be edited.
   */
  public void setExpense(Expense expense) {
    //Deep copying expense and then binding the two expenses
    chosenExpense = new Expense(expense.getDescription(),
        expense.getAmount(), expense.isRecurring(), expense.getCategory(), expense.getDate());
    chosenExpense.descriptionProperty().bindBidirectional(expense.descriptionProperty());
    chosenExpense.amountProperty().bindBidirectional(expense.amountProperty());
    chosenExpense.recurringProperty().bindBidirectional(expense.recurringProperty());
    chosenExpense.expenseCategoryObjectProperty()
        .bindBidirectional(expense.expenseCategoryObjectProperty());
    chosenExpense.dateProperty().bindBidirectional(expense.dateProperty());

    //Set the values of the input fields of the dialog box
    descriptionField.textProperty().set(expense.getDescription());
    amountField.textProperty().setValue(String.valueOf(expense.getAmount()));
    recurringBox.setValue(expense.isRecurring());
    datePicker.setValue(expense.getDate());
    categoryBox.setValue(expense.getCategory());
  }

  /**
   * Adds a new to the tableview or edits an existing entry in table if the OK button is pressed.
   * An entry is edited as the selected entry of the table is bounded
   * to another expense in this class. If this expense
   * is altered, the expense in the tableview will automatically respond with the same changes.

   * @param event If the OK button is pressed.
   */
  @FXML
  public void pressOkBtn(ActionEvent event) {
    //Instantiates a new expense
    if (newExpense == null) {
      LocalDate date = datePicker.getValue();
      double amount = Double.parseDouble(amountField.getText());
      String description = descriptionField.getText();
      ExpenseCategory category = getCategory();
      boolean recurring = isRecurring();
      newExpense = new Expense(description, amount, recurring, category, date);
    }
    //Sets the value of the expense(chosen) that is bounded to
    // the chosen expense (not chosenExpense) in the tableview
    if (chosenExpense != null) {
      chosenExpense.setDescription((descriptionField.getText()));
      chosenExpense.setAmount(Double.parseDouble(amountField.getText()));
      chosenExpense.setRecurring(recurringBox.getValue());
      chosenExpense.setCategory(categoryBox.getValue());
      chosenExpense.setDate(datePicker.getValue());
    }

    final Node source = (Node) event.getSource();
    ((Stage) source.getScene().getWindow()).close();
  }

  /**
   * Closes the dialog box and cancels any pending changes.

   * @param event A button click on the cancel button.
   */
  @FXML
  public void pressCancelBtn(ActionEvent event) {
    final Node source = (Node) event.getSource();
    final Stage stage = (Stage) source.getScene().getWindow();

    stage.close();
  }

  /**
   * Adds event filter to okBtn. If the expense is invalid the event is consumed and
   * an error message is displayed.
   */
  private void addEventFilters() {
    okBtn.addEventFilter(
        ActionEvent.ACTION, event -> {
          try {
            validateInputs();
          } catch (IllegalArgumentException e) {
              event.consume();
              errorMsg.setOpacity(1);
          }
        });
  }

  /**
   * Input validation for an expense. Attempts to instantiate a new item
   * and passes only if the expense is valid.

   * @return True, only if the expense can be instantiated.
   * @throws IllegalArgumentException if the input fields contain invalid inputs.
   */
  private boolean validateInputs() {
    try {
      Expense expense = new Expense(
          Double.parseDouble(amountField.getText()), recurringBox.getValue(),
          categoryBox.getValue(), datePicker.getValue());
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Invalid input. Cannot instantiate expense", e);
    }
    return true;
  }
}