package no.ntnu.idatt1002.demo.controller;

import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.economics.Income;
import no.ntnu.idatt1002.demo.data.economics.IncomeCategory;

/**
 * Class that represents the popup dialog box that appears
 * whenever an income is to be added or edited.
 * The dialog contains various fields that are used to
 * create a new income or edit an existing income.
 *
 * @author Harry Linrui Xu
 * @since 24.3.2023
 */
public class AddIncomeController {

  Income newIncome = null;

  Income chosenIncome = null;
  @FXML
  private Button cancelBtn;

  @FXML
  private Button okBtn;

  @FXML
  private Text errorMsg;
  @FXML
  private DatePicker datePicker;

  @FXML
  private TextField descriptionField;

  @FXML
  private TextField amountField;

  @FXML
  private ComboBox<IncomeCategory> categoryBox;

  @FXML
  private ComboBox<Boolean> recurringBox;

  /**
   * Initializes the category and recurring drop boxes by filling
   * them with all the values from the IncomeCategory enum,
   * and the boolean values respectively, and the datepicker calendar.
   * It then sets them to the default values of GIFT, false and today respectively.
   */
  @FXML
  public void initialize() {
    //Set the possible values in a list.
    ObservableList<IncomeCategory> incomeCategories = FXCollections.observableArrayList(
        IncomeCategory.values());
    //Set the values inside the dropbox
    categoryBox.setItems(incomeCategories);
    //Set default value
    categoryBox.setValue(IncomeCategory.GIFT);

    //Set the possible values for the recurring combo box
    ObservableList<Boolean> recurring = FXCollections.observableArrayList(true, false);
    recurringBox.setItems(recurring);
    recurringBox.setValue(false);

    //Set date to today
    datePicker.setValue(LocalDate.now());

    addEventFilters();
  }

  /**
   * Gets the value of the category box.

   * @return The chosen income category.
   */
  public IncomeCategory getCategory() {
    return categoryBox.getValue();
  }

  /**
   * Gets the value from the recurring box.

   * @return The chosen recurring boolean value.
   */
  public boolean isRecurring() {
    return recurringBox.getValue();
  }

  /**
   * Gets the newly created income.

   * @return The new income.
   */
  public Income getNewIncome() {
    return this.newIncome;
  }

  /**
   * Binds the income that is taken in as the argument with an
   * income declared in this class. The income of this class is instantiated
   * as a deep copy of the argument. Each attribute of their
   * attributes are then bounded. The text fields and category boxes
   * in the dialog window are then set to the values of the
   * chosen income, as to not display empty values.

   * @param income The income that is chosen to be edited.
   */
  public void setIncome(Income income) {
    //Deep copying income and then binding the two incomes
    chosenIncome = new Income(income.getDescription(), income.getAmount(), income.isRecurring(),
        income.getCategory(), income.getDate());
    chosenIncome.descriptionProperty().bindBidirectional(income.descriptionProperty());
    chosenIncome.amountProperty().bindBidirectional(income.amountProperty());
    chosenIncome.recurringProperty().bindBidirectional(income.recurringProperty());
    chosenIncome.incomeCategoryObjectProperty()
        .bindBidirectional(income.incomeCategoryObjectProperty());
    chosenIncome.dateProperty().bindBidirectional(income.dateProperty());


    //Set the values of the input fields of the dialog box
    descriptionField.textProperty().set(income.getDescription());
    amountField.textProperty().setValue(String.valueOf(income.getAmount()));
    recurringBox.setValue(income.isRecurring());
    datePicker.setValue(income.getDate());
    categoryBox.setValue(income.getCategory());
  }

  /**
   * Adds a new to the tableview or edits an existing entry
   * in table if the OK button is pressed.
   * An entry is edited as the selected entry of the table
   * is bounded to another income in this class.
   * If this income is altered, the income in the tableview
   * will automatically respond with the same changes.

   * @param event If the OK button is pressed.
   */
  @FXML
  public void pressOkBtn(ActionEvent event) {
    //Instantiates a new income
    if (newIncome == null) {
      LocalDate date = datePicker.getValue();
      double amount = Double.parseDouble(amountField.getText());
      String description = descriptionField.getText();
      IncomeCategory category = getCategory();
      boolean recurring = isRecurring();
      newIncome = new Income(description, amount, recurring, category, date);
    }
    //Sets the value of the income(chosenIncome) that is bounded to the
    // chosen income (not chosenIncome) in the tableview
    if (chosenIncome != null) {
      chosenIncome.setDescription((descriptionField.getText()));
      chosenIncome.setAmount(Double.parseDouble(amountField.getText()));
      chosenIncome.setRecurring(recurringBox.getValue());
      chosenIncome.setCategory(categoryBox.getValue());
      chosenIncome.setDate(datePicker.getValue());
    }

    final Node source = (Node) event.getSource();
    ((Stage) source.getScene().getWindow()).close();
  }

  /**
   * Adds event filter to okBtn. If the expense is invalid the event is consumed and
   * an error message is displayed.
   */
  private void addEventFilters() {
    okBtn.addEventFilter(
        ActionEvent.ACTION, event -> {
          try {
            validateInputs();
          } catch (IllegalArgumentException e) {
            event.consume();
            errorMsg.setOpacity(1);
          }
        });
  }

  /**
   * Input validation for an income. Attempts to instantiate a new item
   * and passes only if the income is valid.

   * @return True, only if the income can be instantiated.
   * @throws IllegalArgumentException if the input fields contain invalid inputs.
   */
  private boolean validateInputs() {
    try {
      Income income = new Income(
          Double.parseDouble(amountField.getText()), recurringBox.getValue(),
          categoryBox.getValue(), datePicker.getValue());
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Invalid inputs. Cannot instantiate income", e);
    }
    return true;
  }

  /**
   * Closes the dialog box and cancels any pending changes.

   * @param event A button click on the cancel button.
   */
  @FXML
  public void pressCancelBtn(ActionEvent event) {
    final Node source = (Node) event.getSource();
    final Stage stage = (Stage) source.getScene().getWindow();

    stage.close();
  }
}