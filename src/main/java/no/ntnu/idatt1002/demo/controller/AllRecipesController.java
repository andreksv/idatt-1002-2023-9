package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.recipes.FileHandler;
import no.ntnu.idatt1002.demo.data.recipes.IngredientsAtHand;
import no.ntnu.idatt1002.demo.data.recipes.Recipe;
import no.ntnu.idatt1002.demo.data.recipes.RecipeRegister;

/**
 * The AllRecipesController manages the view named AllRecipes.fxml that displays a scrollable list
 * of all the recipes that are stored in the recipe register at
 * 'src/main/resources/recipes/Recipes.register'. The controller holds an instance of the
 * IngredientsAtHand class and the RecipeRegister class and combines the two to fill the list with
 * recipes sorted by the number of ingredients that are not currently in the 'fridge' of the user.
 * Each recipe is also lister with a percentage value that shows how many percent of the recipe's
 * ingredients are actually in the fridge. This information is included so that recipes with a lower
 * number of ingredients are not given an unfair advantage over others where the user may have a
 * significant number of the ingredients available. Each listed recipe may be clicked on by the user
 * to open it in a new view in full detail.
 *
 * @author hannesofie
 */
public class AllRecipesController implements Initializable {

  IngredientsAtHand ingredientsAtHand;
  RecipeRegister recipeRegister;

  @FXML
  private ListView<String> allList;

  private String selectedRecipeName;
  private String filePath;


  /**
   * The initialize method of the controller takes in a URL (location) and ResourceBundle(resources)
   * to initialize the controller once its root element has been processed. The method then reads
   * and creates an object of the IngredientsAtHand class and one of the RecipesRegister class. If
   * the recipe register exists, the list is filled with Strings for each recipe at the format:
   * Recipe name - X missing ingredients (XX%) Finally, a MouseClick event and handler is attached
   * to the list that gets the recipe name of the clicked list item and runs the method showRecipe()
   * with that name as input to show the recipe in detail in the view Recipe.fxml.
   *
   * @param url            The location to resolve the relative paths to the root object.
   * @param resourceBundle Resources used to localize the root object.
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {

    String path = System.getProperty("user.home");
    filePath = path + "/BudgetBuddyFiles/recipes/";

    try {
      ingredientsAtHand = FileHandler.readIngredientsAtHand(filePath + "Fridge");
      recipeRegister = FileHandler.readRecipeRegister(filePath + "Recipes");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    ObservableList<String> recipes;

    if (recipeRegister == null) {
      recipes = FXCollections.observableArrayList(new ArrayList<>());
    } else {
      int numberOfRecipes = recipeRegister.getRecipes().size();

      ArrayList<Recipe> sortedRecipes = recipeRegister
          .pickBestFits(numberOfRecipes, ingredientsAtHand);

      recipes = FXCollections.observableArrayList(sortedRecipes.stream()
          .map(recipe -> String.format("# %s  -  %d missing ingredients (%2.0f %% covered)",
              recipe.getName(), recipe.getMissingIngredients(),
              percent(recipe.getIngredientList().size() - recipe.getMissingIngredients(),
                  recipe.getIngredientList().size()))).toList());
    }

    allList.setItems(recipes);

    allList.setOnMouseClicked(new EventHandler<>() {

      /**
       * The handler method takes a MouseEvent(Mouse Click)
       * and uses the list item that was subjected to the
       * mouse click and extracts the recipe name. That
       * name as a String is then passed to the method
       * 'showRecipe' that loads the view Recipe.fxml to
       * display this particular recipe in full detail.
       *
       * @param mouseEvent A mouse event, in this case a MouseClicked event.
       */
      @Override
      public void handle(MouseEvent mouseEvent) {
        selectedRecipeName = allList.getSelectionModel()
            .getSelectedItem().split("[-#]")[1].strip();
        try {
          showRecipe(selectedRecipeName);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    });
  }


  /**
   * The goBack method is fired whenever the user presses the button 'Back To Suggestions'. It loads
   * the location of the SuggestRecipes.fxml view and sets the new stage.
   *
   * @param event Action event that triggers this method, in this case a button click.
   * @throws IOException If the method fails to load the location at the given file path.
   */
  @FXML
  private void goBack(ActionEvent event) throws IOException {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(getClass().getResource("/view/SuggestRecipes.fxml"));

    Parent root = loader.load();
    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
    Scene scene = new Scene(root);
    stage.setResizable(false);
    stage.setScene(scene);
    stage.show();
  }

  /**
   * The showRecipe method takes in a recipe name as a String and navigates to the view Recipe.fxml
   * after having the RecipeController set the data of the view according to this particular
   * recipe.
   *
   * @param recipeName A case-sensitive string representation of the recipe to open in detail.
   * @throws IOException If the method fails to load the location of the Recipe.fxml view.
   */
  private void showRecipe(String recipeName) throws IOException {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(getClass().getResource("/view/Recipe.fxml"));

    Recipe recipeOfInterest = recipeRegister.getRecipe(recipeName);

    Parent root = loader.load();
    RecipeController recipeController = loader.getController();

    if (recipeOfInterest != null) {
      recipeController.setData(recipeOfInterest);
      Stage stage = (Stage) allList.getParent().getScene().getWindow();
      Scene scene = new Scene(root);
      stage.setScene(scene);
      stage.show();
    }
  }

  /**
   * The percent method takes in two integers(a, b) and performs float-division(a/b) on then and
   * multiplies the answer by 100 to get the percentage 'a' makes of the total 'b'. If either 'a' or
   * 'b' is zero, zero is returned. In the current context; 'a' is an int representing a number of
   * ingredients at hand and part of a recipe and 'b' is an int representing the total number of
   * ingredients in the same recipe.
   *
   * @param a An int to divide by b and multiply by 100.
   * @param b An int by which to divide 'a'.
   * @return A float value presenting the percentage value of 'a' out of 'b'.
   */
  private float percent(int a, int b) {
    if (b != 0 && a != 0) {
      return (a * 100f / b);
    } else {
      return 0;
    }
  }
}
