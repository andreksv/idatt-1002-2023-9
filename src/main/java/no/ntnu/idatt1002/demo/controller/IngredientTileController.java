package no.ntnu.idatt1002.demo.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import no.ntnu.idatt1002.demo.data.recipes.RecipeIngredient;


/**
 * The IngredientTileController manages a simple pane component view called IngredientTile.fxml that
 * is used to dynamically load ingredients into a view to present all details of a recipe. The data
 * to create a component of this kind is set by 'setData' and takes RecipeIngredient objects as
 * parameter.
 *
 * @author hannesofie
 */
public class IngredientTileController {

  @FXML
  private Label text;

  /**
   * The setData method takes an RecipeIngredient object as parameter and sets the text of the label
   * contained in the simple pane to a formatted string following this pattern: "# Ingredient name
   * XX.X YY" where XX.X is the amount of the ingredient in the recipe and YY is the unit of
   * measure.
   *
   * @param ingredient An RecipeIngredient object to format and create an Ingredient tile for.
   */
  public void setData(RecipeIngredient ingredient) {
    StringBuilder sb = new StringBuilder();
    sb.append("# ").append(ingredient.getFoodType().label.substring(0, 1).toUpperCase())
        .append(ingredient.getFoodType().label.substring(1)).append("  ")
        .append(ingredient.getAmount()).append(" ")
        .append(ingredient.getUnit().label);
    text.setText(String.valueOf(sb));
    text.setStyle("-fx-font-size: 16");
  }
}
