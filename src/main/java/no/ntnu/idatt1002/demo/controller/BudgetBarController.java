package no.ntnu.idatt1002.demo.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import no.ntnu.idatt1002.demo.data.budget.BudgetItem;


/**
 * The BudgetBarController manages the component view BudgetBar.fxml that may be dynamically loaded
 * to other views. The view consists of a HBox containing three elements; a label stating the name
 * of the budget post (BudgetItem class), a progress bar showing how much money is left at this post
 * according to the budget and the user's recorded expenses and a label stating how much money are
 * left on the particular budget.
 *
 * @author hannesofie
 */
public class BudgetBarController {

  @FXML
  private Label nameTag;

  @FXML
  private Label leftoverTag;

  @FXML
  private ProgressBar miniBar;

  /**
   * The setData method is called to fill the BudgetBar instance with information of a particular
   * Budget post (BudgetItem), that is the name of the budget, it's progress bar and money left. The
   * information needed for this are taken in as two parameters; the BudgetItem object to represent
   * and a double value representing the money left of this budget.
   *
   * @param budgetItem A BudgetItem object to represent in the BudgetBar instance.
   * @param leftovers  A double value representing the money the user has left on this particular
   *                   BudgetItem.
   */
  public void setData(BudgetItem budgetItem, double leftovers) {
    nameTag.setText(budgetItem.getBudgetCategory().label.substring(0, 1).toUpperCase()
        + budgetItem.getBudgetCategory().label.substring(1));
    leftoverTag.setText(String.format("Left: %.0f", leftovers));

    double progress = 1 + (leftovers - budgetItem.getBudgetAmount()) / budgetItem.getBudgetAmount();

    if (progress < 0.25) {
      miniBar.setStyle("-fx-accent: #ffb000;");
      if (progress < -0.00) {
        miniBar.setProgress(1.0);
        miniBar.setStyle("-fx-accent: #fa5959;");
      } else {
        miniBar.setProgress(progress);
      }
    } else {
      miniBar.setStyle("-fx-accent: rgba(48,222,109,0.8);");
      miniBar.setProgress(progress);
    }
  }
}
