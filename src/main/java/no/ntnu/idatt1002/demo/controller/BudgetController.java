package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.budget.BudgetItem;
import no.ntnu.idatt1002.demo.data.budget.BudgetRegister;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingBudget;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingBudgetArchive;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingSelectedBudget;
import no.ntnu.idatt1002.demo.data.budget.GeneralBudget;
import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;


/**
 * Controller for budget scene in the application. This controller manages all actions that relates
 * to the budget tableview (add, edit and delete), the switching of scenes from the budget scene to
 * another scene, and the saving of the table, whenever the user switches to another scene.
 *
 * @author Anders Emil Bergan
 * @since 24.3.2023
 */
public class BudgetController extends FinanceController {

  private GeneralBudget general;

  @FXML
  private Button addBtn;

  @FXML
  private Button editBtn;

  @FXML
  private Button returnToMainMenuBtn;

  @FXML
  private Button backBtn;

  @FXML
  private Button continueBtn;

  @FXML
  private TableColumn<BudgetItem, Double> amountCol;

  @FXML
  private TableView<BudgetItem> budgetTableView = new TableView<>();

  @FXML
  private TableColumn<BudgetItem, ExpenseCategory> categoryCol;

  @FXML
  private TableColumn<BudgetItem, String> descriptionCol;

  @FXML
  private DatePicker date;

  @FXML
  private ObservableList<BudgetItem> budgetList;

  @FXML
  private PieChart budgetPieChart;

  @FXML
  private Label disposableAmount;

  @FXML
  private Label amountLeft;

  BudgetRegister budgetRegister;
  private String filePath;

  /**
   * Initializes the budget register, the observable budget list and the tableview, along with the
   * values of the dropbox used for filtering the tableview.
   */
  @FXML
  public void initialize() {
    //Initialize table columns
    categoryCol.setCellValueFactory(new PropertyValueFactory<>("budgetCategory"));
    amountCol.setCellValueFactory(new PropertyValueFactory<>("budgetAmount"));
    descriptionCol.setCellValueFactory(new PropertyValueFactory<>("budgetDescription"));

    String path = System.getProperty("user.home");
    filePath = path + "/BudgetBuddyFiles/budgets";

    try {
      //Attempt to load budget from file
      general = loadBudgetDataFromFile(
          filePath + "/" + FileHandlingSelectedBudget
              .readSelectedBudget(filePath + "/SelectedBudget") + "/Budget");
      //Set observable list and table view to generalbudget
      budgetList = FXCollections.observableArrayList(general.getBudgetItems());
      budgetTableView.setItems(budgetList);

      //Instantiate budget register
      if (FileHandlingBudgetArchive.isBudgetRegisterEmpty(filePath + "/Archive")) {
        budgetRegister = new BudgetRegister();
      } else {
        budgetRegister = FileHandlingBudgetArchive
            .readBudgetArchive(filePath + "/Archive");
      }

      //Refresh pie charts only if the budget is old
      if (!FileHandlingBudget.isNewBudget(
          filePath + "/" + FileHandlingSelectedBudget
              .readSelectedBudget(filePath + "/SelectedBudget") + "/Budget")) {
        refreshPieChart();
      }
    } catch (IOException ioe) {
      showErrorDialogBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }

    double maxAmount = general.getMaxAmount();
    //Set calendar, disposable amount and amount left
    formatDatePicker();
    disposableAmount.setText(String.valueOf(maxAmount));
    amountLeft.setText(String.valueOf(maxAmount));

    //Prevent proceeding until all of budget has been used up
    continueBtn.addEventFilter(
        ActionEvent.ACTION, event -> {
          if (maxAmount - general.totalSum() != 0) {
            event.consume();
            showErrorDialogBox("Use up budget",
                "Please distribute the entire disposable amount",
                "The amount must be used up before proceeding");
          }
        }
    );
  }

  /**
   * Method for creating a list of data used for graphing budgets in a pie chart. Only data for
   * categories that have been added are included in this list.
   *
   * @return An observable list of the sum of expenditure on each category
   * @throws IllegalArgumentException if generalbudget attempts to retrieve a category has not been
   *                                  added to the budget.
   */
  public ObservableList<PieChart.Data> createPieChart() throws IllegalArgumentException {
    ObservableList<PieChart.Data> budgetData = FXCollections.observableArrayList();
    List<ExpenseCategory> chosenCategories = general.getChosenBudgetCategories();

    //Only adds the budget data for chosen categories to the pie chart
    for (ExpenseCategory category : chosenCategories) {
      budgetData.add(new Data(category.label,
          general.getBudgetItem(category).getBudgetAmount()));
    }
    return budgetData;
  }

  /**
   * Method for refreshing budget pie chart.
   */
  @Override
  public void refreshPieChart() {
    this.budgetPieChart.setLabelsVisible(true);
    this.budgetPieChart.setData(createPieChart());
  }

  /**
   * Method for disabling the date picker, yet having its opacity at max.
   */
  @Override
  public void formatDatePicker() {
    date.setValue(LocalDate.now());
    date.setDisable(true);
    date.setStyle("-fx-opacity: 1");
    date.getEditor().setStyle("-fx-opacity: 1");
  }

  /**
   * Method for handling button presses on the add button.
   *
   * @param event A button click on the add button.
   */
  @Override
  public void handleAddBtn(ActionEvent event) {
    handleEditBtn(event);
  }

  /**
   * Adds or edits a budget item, depending on what mode the DialogMode enum is at. The method
   * brings up a dialog box popup in which the user can fill and choose values that the budget item
   * will have. Open exiting the popup, the changes the saved to the tableview.
   *
   * @param event A button click on either the add or delete button.
   */
  @Override
  public void handleEditBtn(ActionEvent event) {
    BudgetItem item;
    String dialogTitle;
    DialogMode dialogMode;

    FXMLLoader loader = new FXMLLoader(FirstMenuController.class
        .getResource("/view/AddBudgetNew.fxml"));
    Dialog<BudgetItem> dialog = new Dialog<>();
    dialog.initModality(Modality.APPLICATION_MODAL);

    //Try to load the FXML file onto another dialogPane
    try {
      dialog.getDialogPane().setContent(loader.load());
    } catch (IOException e) {
      showErrorDialogBox("Loading error", "Error in loading dialog", "An error occurred"
          + "when loading the AddBudget window");
    }

    //Loads the controller for the dialog box that appears whenever one adds or edits a budget item
    AddBudgetController budgetController = loader.getController();

    //Sets the title of the dialog box
    if (event.getSource().equals(addBtn)) {
      dialogMode = DialogMode.ADD;
      dialogTitle = "New Budget";
    } else if (event.getSource().equals(editBtn)
        && budgetTableView.getSelectionModel().getSelectedItem() != null) {
      dialogMode = DialogMode.EDIT;
      dialogTitle = "Edit expense";
      //Gets the selected item from the table
      item = budgetTableView.getSelectionModel().getSelectedItem();
      //Binds the selected item to another item which is defined in the budgetcontroller
      budgetController.setBudget(item);
    } else if (event.getSource().equals(editBtn)
        && budgetTableView.getSelectionModel().getSelectedItem() == null) {
      showInformationDialog("No entry selected", "Please select the entry you wish"
          + " to edit", "Click on the entry you wish to edit, then press the edit button");
      return;
    } else {
      return;
    }

    dialog.setTitle(dialogTitle);
    // Show the Dialog and wait for the user to close it
    dialog.showAndWait();

    //Adds the new item to the register
    item = budgetController.getNewBudgetItem();
    if (item != null && dialogMode == DialogMode.ADD) {
      try {
        general.addToBudgetBudgetItem(item);
      } catch (IllegalArgumentException e) {
        showErrorDialogBox(e.getMessage(), e.getMessage(), e.getMessage());
      }
    }
    //Updates the tableview and pie chart using the register
    refreshTableView();
    refreshPieChart();
    refreshAmountLeft();
  }


  /**
   * Deletes an entry from the tableview, if an entry has been selected. The method brings up a
   * popup window, asking for confirmation for deleting the entry.
   *
   * @param event A button click on the delete button
   */
  @FXML
  public void handleDeleteBtn(ActionEvent event) {
    //Gets the selected item from the tableview
    BudgetItem item = budgetTableView.getSelectionModel().getSelectedItem();
    //Exits the method if nothing is selected
    if (item == null) {
      showInformationDialog("No entry selected", "Please select the entry you wish"
          + " to edit", "Click on the entry you wish to edit, then press the edit button");
      return;
    }
    //Brings up a confirmation popup
    String title = "Confirm Delete";
    String header = "Delete Confirmation";
    String content = "Are you sure you would like to delete the selected entry?";
    Optional<ButtonType> isConfirmed = showConfirmationDialog(title, header, content);
    if (isConfirmed.isPresent() && isConfirmed.get() == ButtonType.OK) {
      general.deleteItemFromBudget(item.getBudgetCategory());
      refreshTableView();
      refreshPieChart();
      refreshAmountLeft();
    }
  }

  /**
   * Displays an information dialog.
   *
   * @param title   The dialog box title.
   * @param header  The dialog box header.
   * @param content The dialog box content.
   */
  private void showInformationDialog(String title, String header, String content) {
    Alert alert = new Alert(AlertType.INFORMATION);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);

    alert.show();
  }

  /**
   * Method for synching the register with the tableview. The observable list to which the tableview
   * is set, is being refilled with all the entries in the register, keeping it updated with new
   * changes.
   */
  @Override
  public void refreshTableView() {
    this.budgetList.setAll(general.getBudgetItems());
  }

  /**
   * Method for synching the amount left to the sum of budget items in the table view.
   */
  private void refreshAmountLeft() {
    amountLeft.setText(String.valueOf(general.getMaxAmount() - general.totalSum()));
  }

  /**
   * Saves the changes made to the tableview by writing the information to a file.
   *
   * @throws IOException If an error occurs while writing to the file.
   */
  @Override
  public void saveDataToFile() throws IOException {
    FileHandlingBudget.writeGeneralBudgetToFile(
        filePath + "/" + FileHandlingSelectedBudget
            .readSelectedBudget(filePath + "/SelectedBudget") + "/Budget", general);
  }

  /**
   * Updates the contents of the budget registers by adding a given budget name.
   *
   * @param budgetFolderName The name of the budget project that has been newly created.
   */
  public void updateBudgetRegister(String budgetFolderName) {
    try {
      budgetRegister.addBudgetName(budgetFolderName);
      FileHandlingBudgetArchive
          .writeBudgetRegisterToArchive(budgetRegister, filePath + "/Archive");
    } catch (IOException ioe) {
      showErrorDialogBox(ioe.getMessage(), ioe.getMessage(), ioe.getMessage());
    }
  }

  /**
   * Switches scene, by loading a new FXML file and setting the scene to this location.
   *
   * @param event A button click on the return to main menu button
   */
  @Override
  public void switchScene(ActionEvent event) {
    FXMLLoader loader = new FXMLLoader();
    try {
      if (event.getSource() == returnToMainMenuBtn) {
        //Adds unused categories to the table
        general.addUnusedCategories();
        loader.setLocation(getClass().getResource("/view/MainMenuNew.fxml"));
        //Always saving the data when switching to main menu
        saveDataToFile();
      } else if (event.getSource() == continueBtn) {
        Optional<ButtonType> isConfirmed = showConfirmationDialog("Finish setup",
            "By pressing OK you will complete the budget setup?", ""
                + "Have you done all the changes you wished to make?");
        if (isConfirmed.isPresent() && isConfirmed.get() == ButtonType.OK) {
          //Adds unused categories to the table
          general.addUnusedCategories();
          updateBudgetRegister(FileHandlingSelectedBudget
              .readSelectedBudget(filePath + "/SelectedBudget"));
          //Always saving the data when switching to main menu
          saveDataToFile();

          loader.setLocation(getClass().getResource("/view/MainMenuNew.fxml"));

          //loader.setLocation(getClass().getResource("/view/dualList.fxml"));
        } else {
          return;
        }

      } else if (event.getSource() == backBtn) {
        Optional<ButtonType> isConfirmed = showConfirmationDialog("Return confirmation",
            "Are you sure you want to go back?", "The changes made in this and the previous"
                + " window will be reset");
        if (isConfirmed.isPresent() && isConfirmed.get() == ButtonType.OK) {
          loader.setLocation(getClass().getResource("/view/dualList.fxml"));
        } else {
          return;
        }
      }
      Parent root = loader.load();
      Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
      Scene scene = new Scene(root);
      stage.setScene(scene);
      stage.setResizable(false);

      //Centralize the new screen
      Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
      stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
      stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
      stage.show();

    } catch (Exception ioe) {
      showErrorDialogBox("Loading error", "Error in loading", "Could not load"
          + " FXML file in: " + loader.getLocation());
    }
  }
}
