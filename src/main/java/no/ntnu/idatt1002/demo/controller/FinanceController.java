package no.ntnu.idatt1002.demo.controller;

import java.io.IOException;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import no.ntnu.idatt1002.demo.data.budget.FileHandlingBudget;
import no.ntnu.idatt1002.demo.data.budget.GeneralBudget;
import no.ntnu.idatt1002.demo.data.economics.ExpenseRegister;
import no.ntnu.idatt1002.demo.data.economics.FileHandling;
import no.ntnu.idatt1002.demo.data.economics.IncomeRegister;

/**
 * Abstract class for controllers that control scenes with finance tableviews.
 *
 * @author Harry Linrui Xu
 * @since 29.3.2023
 */
public abstract class FinanceController {

  private ExpenseRegister expenseRegister;

  private IncomeRegister incomeRegister;

  private GeneralBudget general;


  /**
   * Method for handling the adding of new entries in the tableview.
   *
   * @param event A button click on the add button.
   */
  abstract void handleAddBtn(javafx.event.ActionEvent event);

  /**
   * Method for handling the editing of a chosen entry in the tableview.
   *
   * @param event A button click on the edit button.
   */
  abstract void handleEditBtn(javafx.event.ActionEvent event);

  /**
   * Deletes an entry from the tableview, if an entry has been selected. The method brings up a
   * popup window, asking for confirmation for deleting the entry.
   *
   * @param event A button click on the delete button
   */
  abstract void handleDeleteBtn(javafx.event.ActionEvent event);

  /**
   * Method for synching the register with the tableview. The observable list to which the tableview
   * is set, is being refilled with all the entries in the register, keeping it updated with new
   * changes.
   */
  abstract void refreshTableView();

  /**
   * Returns an optional, which is a popup alert box, asking for confirmation for deleting an
   * entry.
   *
   * @param title   The dialog box title.
   * @param header  The dialog box header.
   * @param content The dialog box content.
   * @return An alert box, asking for confirmation for deleting the selected entry of the tableview.
   */
  public Optional<ButtonType> showConfirmationDialog(String title, String header, String content) {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);

    return alert.showAndWait();
  }

  abstract void refreshPieChart();

  /**
   * Method for disabling the date picker, yet having its opacity at max.
   */
  abstract void formatDatePicker();

  /**
   * Displays an error message dialog box with a customizable title, header and content.
   *
   * @param title   The dialog title.
   * @param header  The dialog header.
   * @param content The dialog content.
   */
  public void showErrorDialogBox(String title, String header, String content) {
    Alert alert = new Alert(AlertType.ERROR);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);
    alert.showAndWait();
  }

  //TODO: May need to alter path?
  /**
   * Method that either reads data from a file with which it fills a budget register, if this is an
   * old budget, or instantiates a budget register if this is a new budget.
   *
   * @param fileDestination The name of the file that is being read from.
   * @return An object of type GeneralBudget.
   * @throws IOException If an error occurs while reading from the file.
   */
  public GeneralBudget loadBudgetDataFromFile(String fileDestination) throws IOException {
    //Instantiate new budget
    if (FileHandlingBudget.isEmpty(fileDestination)) {
      general = new GeneralBudget(1000);
    } else { //Load previous budget
      try {
        general = FileHandlingBudget.readGeneralBudgetFromFile(fileDestination);
      } catch (IOException e) {
        showErrorDialogBox("File error", "Error in reading from fil", "An error occurred"
            + "when reading GeneralBudget from the file");
      }
    }
    return general;
  }

  /**
   * Method that either reads data from a file with which it fills an income register, if older
   * changes exist, or instantiates an income register if the file is empty.
   *
   * @param fileDestination The name of the file that is being read from.
   * @return An object of type IncomeRegister.
   */
  public IncomeRegister loadIncomeDataFromFile(String fileDestination) {
    //Instantiate new incomeRegister
    try {
      if (FileHandling.isEmpty(fileDestination)) {
        incomeRegister = new IncomeRegister();
      } else { //Load previous income register
        incomeRegister = FileHandling.readIncomeRegisterFromFile(fileDestination);
      }
    } catch (IOException ioe) {
      showErrorDialogBox("File reading error", "Error in reading from file", "Could not"
          + "read the IncomeRegister from file");
    }
    return incomeRegister;
  }

  /**
   * Method that either reads data from a file with which it fills an expense register, if older
   * changes exist, or instantiates an expense register if the file is empty.
   *
   * @param fileDestination The name of the file that is being read from.
   * @return An object of type IncomeRegister.
   */
  public ExpenseRegister loadExpenseDataFromFile(String fileDestination) {
    //Instantiate expense register
    try {
      if (FileHandling.isEmpty(fileDestination)) {
        expenseRegister = new ExpenseRegister();
      } else { //Load previous income register
        expenseRegister = FileHandling.readExpenseRegisterFromFile(fileDestination);
      }
    } catch (IOException ioe) {
      showErrorDialogBox("File reading error", "Error in reading from file", "Could not"
          + "read the ExpenseRegister from file");
    }
    return expenseRegister;
  }


  /**
   * Saves the changes made to the tableview by writing the information to a file.
   *
   * @throws IOException If an error occurs while writing to the file.
   */
  abstract void saveDataToFile() throws IOException;

  /**
   * Switches scenes, by loading a new FXML file and setting the scene to this location.
   *
   * @param event A button click on the return to main menu button
   */
  abstract void switchScene(javafx.event.ActionEvent event);
}
