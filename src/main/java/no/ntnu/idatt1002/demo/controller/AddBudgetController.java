package no.ntnu.idatt1002.demo.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idatt1002.demo.data.budget.BudgetItem;
import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;

/**
 * Class that represents the popup dialog box that appears
 * whenever a budget item is to be added or edited.
 * The dialog contains various fields that are used to create a new item or edit an existing item.

 * @author Anders Emil Bergan
 * @since 24.3.2023
 *
 */
public class AddBudgetController {

  BudgetItem newBudgetItem = null;

  BudgetItem chosenBudgetItem = null;
  @FXML
  private ComboBox<ExpenseCategory> categoryVariable;
  @FXML
  private TextField amountVariable;
  @FXML
  private TextField descriptionVariable;

  @FXML
  private Button okBtn;

  /**
   * Initializes the category drop box by filling it with
   * all the values from the ExpenseCategory enum.
   * It then sets a prompt text on the box.
   */
  @FXML
  private Text errorMsg;

  /**
   * Initializes the category drop box by filling it with
   * all the values from the ExpenseCategory enum.
   * It then sets a prompt text on the box.
   */
  @FXML
  public void initialize() {
    //Set the possible values in a list.
    ObservableList<ExpenseCategory> expenseCategories = FXCollections.observableArrayList(
            ExpenseCategory.values());
    //Set the values inside the dropbox
    categoryVariable.setItems(expenseCategories);
    //Set default value
    categoryVariable.setValue(ExpenseCategory.FOOD);

    //Adding event filters to buttons
    addEventFilters();
  }

  /**
   * Gets the category value from the category combo box.

   * @return The category value
   */
  public ExpenseCategory getCategory() {
    return categoryVariable.getValue();
  }

  /**
   * Gets the newly created budget item.

   * @return The budget item
   */
  public BudgetItem getNewBudgetItem() {
    return this.newBudgetItem;
  }

  /**
   * Binds the item that is taken in as the argument with a
   * budget item declared in this class. The item of this class is instantiated
   * as a deep copy of the argument. Each attribute of their attributes
   * are then bounded. The text fields and category boxes
   * in the dialog window are then set to the values of the chosen item,
   * as to not display empty values.

   * @param item The item that is chosen to be edited.
   */
  @FXML
  public void setBudget(BudgetItem item) {
    //Deep copying item and then binding the two items
    chosenBudgetItem = new BudgetItem(item.getBudgetAmount(),
        item.getBudgetDescription(), item.getBudgetCategory());
    chosenBudgetItem.getAmountProperty().bindBidirectional(item.getAmountProperty());
    chosenBudgetItem.getDescriptionProperty().bindBidirectional(item.getDescriptionProperty());
    chosenBudgetItem.getCategoryProperty().bindBidirectional(item.getCategoryProperty());

    //Set the values of the input fields of the dialog box
    amountVariable.textProperty().set(String.valueOf(item.getBudgetAmount()));
    descriptionVariable.textProperty().set(item.getBudgetDescription());
    categoryVariable.setValue(item.getBudgetCategory());
  }

  /**
   * Adds a new to the budget tableview or edits an existing entry
   * in table if the OK button is pressed.
   * An entry is edited as the selected entry of the table is bounded
   * to another budget item in this class. If this budget item
   * is altered, the budget item in the tableview will automatically
   * respond with the same changes.

   * @param event If the OK button is pressed.
   */
  @FXML
  private void pressOkBtn(ActionEvent event) {
    //Instantiates a new budget item
    if (newBudgetItem == null) {
      ExpenseCategory category = getCategory();
      double amount = Double.parseDouble(amountVariable.getText());
      String description = descriptionVariable.getText();
      newBudgetItem = new BudgetItem(amount, description, category);
    }
    //Sets the value of the budget(chosenBudgetItem) that is bounded to the
    // chosen budget item (not chosenBudgetItem) in the tableview
    if (chosenBudgetItem != null) {
      chosenBudgetItem.setBudgetAmount(Double.parseDouble(amountVariable.getText()));
      chosenBudgetItem.setBudgetDescription(descriptionVariable.getText());
      chosenBudgetItem.setBudgetCategory(categoryVariable.getValue());
    }
    final Node source = (Node) event.getSource();
    final Stage stage = (Stage) source.getScene().getWindow();
    stage.close();
  }

  /**
   * Adds event filter to okBtn. If the budget item is invalid the event is consumed and
   * an error message is displayed.
   */
  private void addEventFilters() {
    okBtn.addEventFilter(
        ActionEvent.ACTION, event -> {
        try {
          validateInputs();
        } catch (IllegalArgumentException e) {
          event.consume();
          errorMsg.setOpacity(1);
        }
      }
    );
  }

  /**
   * Input validation for a budget item. Attempts to instantiate a new item
   * and passes only if the item is valid.

   * @return True, only if the item can be instantiated.
   * @throws IllegalArgumentException if the input fields contain invalid inputs.
   */
  private boolean validateInputs() throws IllegalArgumentException {
    try {
      BudgetItem item = new BudgetItem(
          Double.parseDouble(amountVariable.getText()),
          descriptionVariable.getText(),
          categoryVariable.getValue());
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Invalid inputs. Cannot instantiate item", e);
    }
    return true;
  }

  /**
   * Closes the dialog box.

   * @param actionEvent A button click on the close button.
   */
  @FXML
  private void pressCancelBtn(ActionEvent actionEvent) {
    final Node source = (Node) actionEvent.getSource();
    final Stage stage = (Stage) source.getScene().getWindow();
    stage.close();
  }
}
