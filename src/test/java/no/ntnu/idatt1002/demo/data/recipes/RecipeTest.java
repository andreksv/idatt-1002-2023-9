package no.ntnu.idatt1002.demo.data.recipes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecipeTest {

    Recipe recipe;

    @BeforeEach
    void beforeEach() {
        recipe = new Recipe("Meat, cheese and potatoes", "Instructions");

        recipe.addIngredient(FoodItem.MINCED_MEAT, 500, MeasuringUnit.GR);
        recipe.addIngredient(FoodItem.POTATO, 750, MeasuringUnit.GR);
        recipe.addIngredient(FoodItem.YELLOW_CHEESE, 2, MeasuringUnit.DL);
    }

    @Nested
    @DisplayName("Test functionality of the Recipe constructor.")
    class testConstructor {

        @Test
        @DisplayName("Constructor creates Recipe successfully.")
        void constructorCreatesRecipe() {
            // Two recipes are considered equal if their name's match.
            assertEquals(recipe, new Recipe("Meat, cheese and potatoes", "Instructions"));

            assertEquals("Meat, cheese and potatoes", recipe.getName());
            assertEquals(3, recipe.getIngredientList().size());
            assertEquals("Instructions", recipe.getInstructions());
        }

        @Test
        @DisplayName("Constructor throws exception for empty name or instruction.")
        void constructorThrowsException() {
            assertThrows(IllegalArgumentException.class, () -> new Recipe("", "Instructions"));
            assertThrows(IllegalArgumentException.class, () -> new Recipe("Name", ""));
        }

    }


    @Nested
    @DisplayName("Test methods to set name and instructions for recipe.")
    class testSetNameAndInstructions {

        @Test
        @DisplayName("Can change name of recipe successfully.")
        void setNameSuccessfully() {
            String newName = "Meat and cheese";
            recipe.setName(newName);
            assertEquals(newName, recipe.getName());
        }

        @Test
        @DisplayName("Throws exception when name set to empty string.")
        void setNameEmptyThrowsException() {
            assertThrows(IllegalArgumentException.class, () -> recipe.setName(" "));
        }

        @Test
        @DisplayName("Can change instructions of recipe successfully.")
        void setInstructionsSuccessfully() {
            String newInstruction = "New instructions";
            recipe.setInstructions(newInstruction);
            assertEquals(newInstruction, recipe.getInstructions());
        }

        @Test
        @DisplayName("Throws exception when Instructions set to empty string.")
        void setInstructionsEmptyThrowsException() {
            assertThrows(IllegalArgumentException.class, () -> recipe.setInstructions(""));
        }

    }


    @Nested
    @DisplayName("Test method to add or alter ingredients in the recipe.")
    class testAddIngredient {


        @Test
        @DisplayName("New Ingredient is added to recipe successfully.")
        void addNewIngredient() {
            int ingredientsInRecipe = recipe.getIngredientList().size();
            assertTrue(recipe.addIngredient(FoodItem.LEMON, 1, MeasuringUnit.PC));
            assertEquals(ingredientsInRecipe + 1, recipe.getIngredientList().size());
        }

        @Test
        @DisplayName("Ingredient already in recipe does not alter and returns true.")
        void addAlreadyIncludedIngredient() {
            int ingredientsInRecipe = recipe.getIngredientList().size();
            assertTrue(recipe.addIngredient(FoodItem.MINCED_MEAT, 500, MeasuringUnit.GR));
            assertEquals(ingredientsInRecipe, recipe.getIngredientList().size());
        }

        @Test
        @DisplayName("Ingredient already in recipe with different amount alters and returns true.")
        void addAlteredIngredient() {
            int ingredientsInRecipe = recipe.getIngredientList().size();
            assertEquals(500, recipe.getIngredient(FoodItem.MINCED_MEAT).getAmount());
            assertTrue(recipe.addIngredient(FoodItem.MINCED_MEAT, 10, MeasuringUnit.DL));
            assertEquals(ingredientsInRecipe, recipe.getIngredientList().size());
            assertEquals(10, recipe.getIngredient(FoodItem.MINCED_MEAT).getAmount());
        }

        @Test
        @DisplayName("Invalid ingredient change is not made and false is returned.")
        void addInvalidIngredientReturnsFalse() {
            int ingredientsInRecipe = recipe.getIngredientList().size();
            assertFalse(recipe.addIngredient(null, 500, MeasuringUnit.GR));
            assertEquals(ingredientsInRecipe, recipe.getIngredientList().size());
        }

    }

    @Nested
    @DisplayName("Test update method for ingredient status of Recipe class")
    class testUpdateIngredientStatus {


        @Test
        @DisplayName("Updating ingredient status works as expected.")
        void updateIngredientStatus() {
            IngredientsAtHand inFridge = new IngredientsAtHand();
            inFridge.addIngredient(FoodItem.MINCED_MEAT);
            inFridge.addIngredient(FoodItem.YELLOW_CHEESE);

            recipe.getIngredientList().forEach((ingredient) -> assertFalse(ingredient.isAtHand()));
            recipe.updateIngredientStatus(inFridge);

            assertTrue(recipe.getIngredient(FoodItem.MINCED_MEAT).isAtHand());
            assertTrue(recipe.getIngredient(FoodItem.YELLOW_CHEESE).isAtHand());
            assertFalse(recipe.getIngredient(FoodItem.POTATO).isAtHand());
        }

        @Test
        @DisplayName("Update of ingredient status based on empty or null ingredients at hand collection.")
        void nullOrEmptyIngredientsAtHand() {
            assertThrows(NullPointerException.class, () -> recipe.updateIngredientStatus(null));
        }

        @Test
        @DisplayName("Update of ingredients status when ingredients at hand is empty.")
        void emptyIngredientsAtHand() {
            IngredientsAtHand emptyFridge = new IngredientsAtHand();
            assertAll(() -> recipe.updateIngredientStatus(emptyFridge));

            assertFalse(recipe.getIngredient(FoodItem.MINCED_MEAT).isAtHand());
            assertFalse(recipe.getIngredient(FoodItem.YELLOW_CHEESE).isAtHand());
            assertFalse(recipe.getIngredient(FoodItem.POTATO).isAtHand());
        }
    }
}