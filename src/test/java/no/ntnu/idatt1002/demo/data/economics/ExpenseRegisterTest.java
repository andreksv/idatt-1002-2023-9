package no.ntnu.idatt1002.demo.data.economics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExpenseRegisterTest {
    ExpenseRegister expenseRegister = new ExpenseRegister();
    @Nested
    @DisplayName("Test addItem")
    class testAddItem {
        @Test
        @DisplayName("addItem method throws exception when it should")
        void addItemThrows() {
            Expense expense = new Expense("description", 59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
            expenseRegister.addItem(expense);
            assertThrows(IllegalArgumentException.class, () -> expenseRegister.addItem(expense));
        }

        @Test
        @DisplayName("addItem method does not throw exception when it should not")
        void addItemDoesNotThrow() {
            Expense expense1 = new Expense("description", 59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
            Expense expense2 = new Expense("anotherDescription", 6.5f, true, ExpenseCategory.BOOKS, LocalDate.of(2023, Month.MARCH, 3));
            assertDoesNotThrow(() -> expenseRegister.addItem(expense1));
            assertDoesNotThrow(() -> expenseRegister.addItem(expense2));
        }
    }

    @Nested
    @DisplayName("Test removeItem")
    class testRemoveItem{
        @Test
        @DisplayName("removeItem does throw exception when it should")
        void removeItemDoesThrow(){
            Expense expense1 = new Expense("description", 59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
            Expense notExpense1 = new Expense("anotherDescription", 6.5f, true, ExpenseCategory.BOOKS, LocalDate.of(2023, Month.MARCH, 2));
            expenseRegister.addItem(expense1);
            assertThrows(IllegalArgumentException.class, () -> expenseRegister.removeItem(notExpense1));
        }
        @Test
        @DisplayName("removeItem method does not throw exception when it should not")
        void removeItemDoesNotThrow(){
            Expense expense1 = new Expense("description", 59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
            Expense expense1Copy = new Expense("description", 59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
            expenseRegister.addItem(expense1);
            assertDoesNotThrow(() -> expenseRegister.removeItem(expense1Copy));
        }

    }

    @Nested
    @DisplayName("Test getTotalSum and getExpenseByCategory")
    class testGetExpenseByCategoryMethod {
        Expense expense1;
        Expense expense2;
        Expense expense3;
        Expense expense4;

        @BeforeEach
        void addExpensesToRegister() {
            expense1 = new Expense("description1", 59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
            expense2 = new Expense("description2", 62.4f, true, ExpenseCategory.FOOD, LocalDate.of(2021, Month.FEBRUARY, 1));
            expense3 = new Expense("description3", 9.81f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.JULY, 5));
            expense4 = new Expense("description3", 100.81f, false, ExpenseCategory.BOOKS, LocalDate.of(2023, Month.JULY, 12));

            expenseRegister.addItem(expense1);
            expenseRegister.addItem(expense2);
            expenseRegister.addItem(expense3);
            expenseRegister.addItem(expense4);
        }

        @Test
        @DisplayName("getTotalSum method gives correct amount")
        void getTotalSumCorrectAmount() {
            double totalIncome = 59.9f + 62.4f + 9.81f + 100.81f;
            assertEquals(Math.round(expenseRegister.getTotalSum()), Math.round(totalIncome));
        }

        @Test
        @DisplayName("getExpenseByCategory gives expected Expenses back")
        void getExpensesByCategoryGivesExpectedExpensesBack() {
            ExpenseRegister expenseSalary = expenseRegister.getExpenseByCategory(ExpenseCategory.CLOTHES);
            assertTrue(expenseSalary.getItems().contains(expense1));
            assertTrue(expenseSalary.getItems().contains(expense3));
        }

        @Test
        @DisplayName("getExpenseByMonth gives expected Expenses back")
        void getExpenseByMonthGivesCorrectExpenses() {
            YearMonth yearMonth = YearMonth.of(2023, 7);

            ExpenseRegister expenseJuly = expenseRegister.getExpenseByMonth(yearMonth);

            assertTrue(expenseJuly.getItems().contains(expense4));
            assertTrue(expenseJuly.getItems().contains(expense3));
            assertFalse(expenseJuly.getItems().contains(expense2));
        }
    }
}
