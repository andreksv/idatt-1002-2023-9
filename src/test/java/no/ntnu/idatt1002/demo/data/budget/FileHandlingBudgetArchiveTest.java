package no.ntnu.idatt1002.demo.data.budget;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class FileHandlingBudgetArchiveTest {

  BudgetRegister budgetRegister;

  @Nested
  @DisplayName("Test read/writeBudgetRegisterToArchive")
  class ReadAndWriteToFile {
    BudgetRegister newBudgetRegister;

    String name1, name2, name3, name4;
    String filePath;

    String toString;
    @BeforeEach
    void setUp() {
      budgetRegister = new BudgetRegister();
      filePath = "src/main/resources/testFiles/budget/Archive";

      name1 = "APRIL1";
      name2 = "APRIL2";
      name3 = "APRIL3";
      name4 = "APRIL5";

      newBudgetRegister = new BudgetRegister();
      newBudgetRegister.addBudgetName(name1);
      newBudgetRegister.addBudgetName(name2);
      newBudgetRegister.addBudgetName(name3);
      newBudgetRegister.addBudgetName(name4);

      toString = "APRIL1\nAPRIL2\nAPRIL3\nAPRIL5\n";
    }

    @Test
    @DisplayName("Test read and write to file")
    void testReadAndWrite() throws IOException {
      budgetRegister = FileHandlingBudgetArchive.readBudgetArchive(filePath);

      assertEquals(toString, budgetRegister.toString());

      FileHandlingBudgetArchive.writeBudgetRegisterToArchive(newBudgetRegister, filePath);
      newBudgetRegister = FileHandlingBudgetArchive.readBudgetArchive(filePath);

      assertEquals(budgetRegister.toString(), newBudgetRegister.toString());
    }

    @Test
    @DisplayName("Test read throws IOException for non-existent file")
    void testReadWithNonExistentFile() {
      assertThrows(IOException.class,
          () -> FileHandlingBudgetArchive.readBudgetArchive("sheesh"));
    }
  }

  @Nested
  @DisplayName("Test isBudgetRegisterEmpty")
  class IsBudgetRegisterEmpty {

    String filePath, filePath1;

    @BeforeEach
    void setUp() {
      budgetRegister = new BudgetRegister();
      filePath = "src/main/resources/testFiles/budget/emptyFile";
      filePath1 = "src/main/resources/testFiles/budget/notEmptyFile";
    }

    @Test
    @DisplayName("Test IsBudgetRegisterEmpty with empty file")
    void testEmptyFileWithEmptyFile() throws IOException {
      assertTrue(FileHandlingBudgetArchive.isBudgetRegisterEmpty(filePath));
    }

    @Test
    @DisplayName("Test IsBudgetRegisterEmpty with non-empty file")
    void testEmptyFileWithNonEmptyFile() throws IOException {
      assertFalse(FileHandlingBudgetArchive.isBudgetRegisterEmpty(filePath1));
    }

    @Test
    @DisplayName("Test IsBudgetRegisterEmpty with non-existent file throws IOException")
    void testEmptyFileWithNonExistentFile() {
      assertThrows(IOException.class, () ->
          FileHandlingBudgetArchive.isBudgetRegisterEmpty("sheesh"));
    }

  }
}
