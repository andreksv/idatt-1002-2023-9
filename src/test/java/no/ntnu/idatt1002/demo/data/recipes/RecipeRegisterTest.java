package no.ntnu.idatt1002.demo.data.recipes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecipeRegisterTest {

    RecipeRegister myRecipes;
    int numberOfRecipes;

    @BeforeEach
    void beforeEach() {
        myRecipes = new RecipeRegister();
        myRecipes.addRecipe(new Recipe("Lemon pasta", "Instructions"));
        numberOfRecipes = myRecipes.getRecipes().size();
    }


    @Nested
    @DisplayName("Test that addRecipe method works as intended.")
    class testAddRecipe {

        @Test
        @DisplayName("Adding recipe to register successfully.")
        void addRecipeToRegister() {
            assertAll(() -> myRecipes.addRecipe(new Recipe("Meat stew", "Instructions")));
            assertEquals(numberOfRecipes + 1, myRecipes.getRecipes().size());
        }

        @Test
        @DisplayName("When adding already registered recipe, replace the old one.")
        void recipeAlreadyInRegister() {
            assertAll(() -> myRecipes.addRecipe( new Recipe("Lemon pasta", "Instructions 2")));
            assertEquals(numberOfRecipes, myRecipes.getRecipes().size());
            assertEquals("Instructions 2", myRecipes.getRecipe("Lemon pasta").getInstructions());
        }

        @Test
        @DisplayName("Throw exception when adding null recipe.")
        void addNullRecipe() {
            assertThrows(NullPointerException.class, () -> myRecipes.addRecipe(null));
        }
    }


    @Nested
    @DisplayName("Test functionality of 'getRecipe' method of register.")
    class testGetRecipe {

        @Test
        @DisplayName("Returns correct recipe based on name.")
        void getRecipeByName() {
            assertEquals(new Recipe("Lemon pasta", "Instructions"), myRecipes.getRecipe("Lemon pasta"));
        }

        @Test
        @DisplayName("Returns null for empty string.")
        void getRecipeByNullName() {
            assertNull(myRecipes.getRecipe(null));
        }

        @Test
        @DisplayName("Returns null for recipe not present in register.")
        void getNoRecipeByName() {
            assertNull(myRecipes.getRecipe("Pancakes"));
        }
    }


    @Nested
    @DisplayName("Test functionality of 'pickBestFits' method of register class.")
    class testBestPick {

        IngredientsAtHand ingredientsAtHand;
        RecipeRegister newRecipes;
        Recipe recipe1;
        Recipe recipe2;
        Recipe recipe3;
        Recipe recipe4;


        @BeforeEach
        void setUpRecipes() {
            ingredientsAtHand = new IngredientsAtHand();
            ingredientsAtHand.addIngredient(FoodItem.LEMON);

            newRecipes = new RecipeRegister();

            recipe1 = new Recipe("Recipe with 2 ingredients", "Instructions");
            recipe1.addIngredient(FoodItem.LEMON, 2, MeasuringUnit.PC);
            recipe1.addIngredient(FoodItem.ONION, 2, MeasuringUnit.PC);

            recipe2 = new Recipe("Recipe with 3 ingredients", "Instructions");
            recipe2.addIngredient(FoodItem.POTATO, 2, MeasuringUnit.PC);
            recipe2.addIngredient(FoodItem.ONION, 2, MeasuringUnit.PC);
            recipe2.addIngredient(FoodItem.BROCCOLI, 2, MeasuringUnit.PC);

            recipe3 = new Recipe("Recipe with 4 ingredients", "Instructions");
            recipe3.addIngredient(FoodItem.POTATO, 2, MeasuringUnit.PC);
            recipe3.addIngredient(FoodItem.ONION, 2, MeasuringUnit.PC);
            recipe3.addIngredient(FoodItem.BROCCOLI, 2, MeasuringUnit.PC);
            recipe3.addIngredient(FoodItem.OIL, 2, MeasuringUnit.PC);

            recipe4 = new Recipe("Recipe with 5 ingredients", "Instructions");
            recipe4.addIngredient(FoodItem.POTATO, 2, MeasuringUnit.PC);
            recipe4.addIngredient(FoodItem.ONION, 2, MeasuringUnit.PC);
            recipe4.addIngredient(FoodItem.BROCCOLI, 2, MeasuringUnit.PC);
            recipe4.addIngredient(FoodItem.OIL, 2, MeasuringUnit.PC);
            recipe4.addIngredient(FoodItem.BELL_PEPPER, 2, MeasuringUnit.PC);

            newRecipes.addRecipe(recipe4);
            newRecipes.addRecipe(recipe3);
            newRecipes.addRecipe(recipe2);
            newRecipes.addRecipe(recipe1);
        }

        @Test
        @DisplayName("Pick one recipe correctly.")
        void pickOneBestRecipe() {
            assertEquals(recipe1, newRecipes.pickBestFits(1, ingredientsAtHand).get(0));
            assertEquals(1, newRecipes.pickBestFits(1, ingredientsAtHand).size());
        }

        @Test
        @DisplayName("Attempt at picking more recipes than in register. ")
        void pickMoreThanAvailableRecipes() {
            assertAll(() -> newRecipes.pickBestFits(6, ingredientsAtHand));
            assertEquals(4, newRecipes.pickBestFits(6, ingredientsAtHand).size());
        }

        @Test
        @DisplayName("Pick based on no ingredients at hand.")
        void pickWithNoIngredientsAtHand() {
            IngredientsAtHand noIngredientsAtHand = new IngredientsAtHand();
            assertAll(() -> newRecipes.pickBestFits(3, noIngredientsAtHand));
            assertEquals(recipe1, newRecipes.pickBestFits(3, noIngredientsAtHand).get(0) );
            assertEquals(3, newRecipes.pickBestFits(3, noIngredientsAtHand).size() );
        }

        @Test
        @DisplayName("Pick zero recipes returns empty ArrayList.")
        void pickNoRecipes() {
            assertAll(() -> newRecipes.pickBestFits(0, ingredientsAtHand));
            assertEquals(0, newRecipes.pickBestFits(0, ingredientsAtHand).size() );
        }

        @Test
        @DisplayName("Correct first suggestion after reorder of best fit.")
        void suggestionsInCorrectOrder() {
            assertAll(() -> newRecipes.pickBestFits(4, ingredientsAtHand));
            assertEquals(recipe1, newRecipes.pickBestFits(4, ingredientsAtHand).get(0));
            assertEquals(recipe2, newRecipes.pickBestFits(4, ingredientsAtHand).get(1));
            assertEquals(recipe3, newRecipes.pickBestFits(4, ingredientsAtHand).get(2));
            assertEquals(recipe4, newRecipes.pickBestFits(4, ingredientsAtHand).get(3));

            ingredientsAtHand.removeIngredient(FoodItem.LEMON);
            ingredientsAtHand.addIngredient(FoodItem.BROCCOLI);
            ingredientsAtHand.addIngredient(FoodItem.ONION);

            assertEquals(recipe2, newRecipes.pickBestFits(4, ingredientsAtHand).get(0));
            assertEquals(recipe4, newRecipes.pickBestFits(4, ingredientsAtHand).get(3));

        }
    }
}