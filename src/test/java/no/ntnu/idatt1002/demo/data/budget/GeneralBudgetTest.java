package no.ntnu.idatt1002.demo.data.budget;

import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class GeneralBudgetTest {
    @Test
    @DisplayName("Constructor throws exception when maxAmount is under zero")
    void constructor_throws_exception_when_maxAmount_under_zero(){
        List<BudgetItem> list = new ArrayList<>();

        assertThrows(IllegalArgumentException.class, () -> new GeneralBudget(list, -12));
    }

    @Test
    @DisplayName("Constructor with savings throws exception when maxAmount is under zero")
    void constructor_with_savings_throws_exception_when_maxAmount_under_zero(){
        List<BudgetItem> list = new ArrayList<>();
        Savings savings = new Savings(1000);

        assertThrows(IllegalArgumentException.class, () -> new GeneralBudget(list, -12, savings));
    }

    @Test
    @DisplayName("AddToBudget throws when totalSum is higher than maxAmount ")
    void add_to_budget_throws_when_totalSum_is_over_maxAmount(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);
        BudgetItem foodItem = new BudgetItem(1300, "Food", ExpenseCategory.FOOD);

        assertThrows(IllegalArgumentException.class, () ->  budget1.addToBudgetBudgetItem(foodItem));
    }

    @Test
    @DisplayName("addToBudget throws when a budget with same category already exists")
    void add_to_budget_throws_when_a_budget_with_same_category_already_exists(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);
        BudgetItem foodItem = new BudgetItem(500, "Food", ExpenseCategory.FOOD);
        budget1.addToBudgetBudgetItem(foodItem);
        BudgetItem alsoFoodItem = new BudgetItem(200, "Food", ExpenseCategory.FOOD);

        assertThrows(IllegalArgumentException.class, () ->  budget1.addToBudgetBudgetItem(alsoFoodItem));
    }

    @Test
    @DisplayName("Adds a budget to generalBudget")
    void add_budget_to_generalBudget(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);
        BudgetItem foodItem = new BudgetItem(500, "Food", ExpenseCategory.FOOD);
        budget1.addToBudgetBudgetItem(foodItem);

        assertEquals(1, list.size());
    }

    @Test
    @DisplayName("Checks if the list contains a item with a given category")
    void checks_if_the_list_contains_an_item_with_this_category_true(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);
        BudgetItem foodItem = new BudgetItem(500, "Food", ExpenseCategory.FOOD);
        budget1.addToBudgetBudgetItem(foodItem);

        assertTrue(budget1.hasBudgetCategory(ExpenseCategory.FOOD));
    }

    @Test
    @DisplayName("checks that the list does not contain an item with the given category")
    void checks_if_the_list_contains_an_item_with_this_category_false(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);
        BudgetItem bookItem = new BudgetItem(500, "Books", ExpenseCategory.BOOKS);
        budget1.addToBudgetBudgetItem(bookItem);

        assertFalse(budget1.hasBudgetCategory(ExpenseCategory.FOOD));
    }

    @Test
    @DisplayName("Checks that the getTotalSum gives the correct number")
    void get_total_sum_of_all_budgetItems_in_the_budget(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);
        BudgetItem bookItem = new BudgetItem(500, "Books", ExpenseCategory.BOOKS);
        BudgetItem foodItem = new BudgetItem(300, "Food", ExpenseCategory.FOOD);
        budget1.addToBudgetBudgetItem(bookItem);
        budget1.addToBudgetBudgetItem(foodItem);

        assertEquals(800, budget1.totalSum());
    }

    @Test
    @DisplayName("Checks that an item gets deleted from the budget")
    void delete_from_budget(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);
        BudgetItem bookItem = new BudgetItem(500, "Books", ExpenseCategory.BOOKS);
        budget1.addToBudgetBudgetItem(bookItem);
        budget1.deleteItemFromBudget(ExpenseCategory.BOOKS);

        assertTrue(list.isEmpty());
    }

    @Test
    @DisplayName("Test getBudgetItem when requesting a non-added budget item")
    void get_budget_item_for_non_added_budget() {
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(list, 1200);

        assertThrows(IllegalArgumentException.class, () -> budget1.getBudgetItem(ExpenseCategory.FOOD));
    }

    @Nested
    class UnusedCategories {
        GeneralBudget budget;
        List<BudgetItem> list;

        @BeforeEach
        void setUp() {
            list = new ArrayList<>();
            budget = new GeneralBudget(list, 1200);
            budget.addToBudget(10, "Books", ExpenseCategory.BOOKS);
        }

        @Test
        @DisplayName("Test addUnusedCategories adds unused categories")
        void has_Unused_Categories_with_full_budget() {
            assertEquals(1, budget.getBudgetItems().size());
            budget.addUnusedCategories();

            assertEquals(ExpenseCategory.values().length, budget.getBudgetItems().size());
        }

    }

    @Test
    @DisplayName("Add to savings throws illegal argument exception when it should")
    void add_To_Savings_Throws_Exception_When_It_Should() {
        GeneralBudget generalBudget = new GeneralBudget(1000);
        assertThrows(IllegalArgumentException.class, () -> generalBudget.addToSavings(1500));
    }

    @Test
    @DisplayName("Test getting chosen categories")
    void test_getting_chosen_categories() {
        List<ExpenseCategory> categories = new ArrayList<>();
        categories.add(ExpenseCategory.FOOD);
        categories.add(ExpenseCategory.CLOTHES);
        categories.add(ExpenseCategory.OTHER);

        GeneralBudget generalBudget = new GeneralBudget(1000);

        for (ExpenseCategory category : categories) {
            generalBudget.addToBudget(200, "", category);
        }

        assertEquals(categories, generalBudget.getChosenBudgetCategories());
    }


   /* @Test
    @DisplayName("Gets the number of days left in the month. 17 has to be changed to the actual number of days left in the month.")
    void get_days_left_of_the_month(){
        List<BudgetItem> list = new ArrayList<>();
        GeneralBudget budget1 = new GeneralBudget(12, list, 1200);

        assertEquals(17, budget1.getDaysLeftOfBudgetPeriod());
    }*/
}