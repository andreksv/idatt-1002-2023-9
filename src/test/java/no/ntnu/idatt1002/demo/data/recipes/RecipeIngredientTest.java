package no.ntnu.idatt1002.demo.data.recipes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecipeIngredientTest {

    RecipeIngredient testIngredient;

    @BeforeEach
    void beforeEach() {
        testIngredient = new RecipeIngredient(FoodItem.ONION, 0.5f, MeasuringUnit.KG);
    }

    @Test
    @DisplayName("Check that recipe ingredient is initiated not at hand")
    void initiateAsNotAtHand() {
        assertFalse(testIngredient.isAtHand());
    }

    @Test
    @DisplayName("AtHand correctly set")
    void setAtHandCorrectly() {
        assertFalse(testIngredient.isAtHand());
        testIngredient.setAtHand(false);
        assertFalse(testIngredient.isAtHand());
    }

    @Test
    @DisplayName("Validate that two ingredients are equal despite at hand status.")
    void equalIngredientsDespiteStatus() {
        RecipeIngredient secondTestIngredient = new RecipeIngredient(FoodItem.ONION, 0.5f, MeasuringUnit.KG);
        testIngredient.setAtHand(true);

        assertEquals(testIngredient, secondTestIngredient);
    }

}