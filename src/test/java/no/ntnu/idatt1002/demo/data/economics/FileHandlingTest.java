package no.ntnu.idatt1002.demo.data.economics;

import org.junit.jupiter.api.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlingTest {
    ExpenseRegister expenseRegister = new ExpenseRegister();
    IncomeRegister incomeRegister = new IncomeRegister();

    @Nested
    @DisplayName("Test isEmpty method")
    class testIsEmpty{
        @Test
        @DisplayName("isEmpty returns true if a file is empty")
        void isEmptyReturnsTrueIfAFileIsEmpty() throws IOException {
            FileHandling.writeItemRegisterToFile(incomeRegister, "src/main/resources/testFiles/economics/incomeRegisterTest");
            assertTrue(FileHandling.isEmpty("src/main/resources/testFiles/economics/incomeRegisterTest"));
        }

        @Test
        @DisplayName("isEmpty returns false if a file is not empty")
        void isEmptyReturnsFalseIfFileIsNotEmpty() throws IOException {
            Income income1 = new Income("description", 59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 3));
            incomeRegister.addItem(income1);
            FileHandling.writeItemRegisterToFile(incomeRegister, "src/main/resources/testFiles/economics/incomeRegisterTest");
            assertFalse(FileHandling.isEmpty("src/main/resources/testFiles/economics/incomeRegisterTest"));
        }
    }
    @Nested
    @DisplayName("FileHandling IncomeRegister to file")
    class fileHandlingIncomeRegisterToFile{
        String fileTitle = "src/main/resources/testFiles/economics/incomeRegisterTest";
        @Nested
        @DisplayName("FileHandling incomeRegister with income that has description")
        class fileHandlingIncomeRegisterWithIncomeThatHasDescription{
            @BeforeEach
            void addIncomeToIncomeRegister(){
                Income income1 = new Income("description", 59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 3));
                incomeRegister.addItem(income1);
            }
            @Test
            @DisplayName("Writing to file does not throw exception")
            void writingToFileDoesNotThrowException() {
                assertDoesNotThrow(()->FileHandling.writeItemRegisterToFile(incomeRegister, fileTitle));
            }

            @Test
            @DisplayName("Reading from file gives correct incomeRegister back")
            void readingFromFileGivesCorrectIncomeRegisterBack() throws IOException {
                assertEquals(incomeRegister.toString(), FileHandling.readIncomeRegisterFromFile(fileTitle).toString());
            }
        }
        @Nested
        @DisplayName("FileHandling incomeRegister with income that does not have description")
        class fileHandlingIncomeRegisterWithIncomeThatDoesNotHaveDescription{
            @BeforeEach
            void addIncomeToIncomeRegister(){
                Income income1 = new Income(59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 3));
                incomeRegister.addItem(income1);
            }
            @Test
            @DisplayName("Writing to file does not throw exception")
            void writingToFileDoesNotThrowException() {
                assertDoesNotThrow(()->FileHandling.writeItemRegisterToFile(incomeRegister, fileTitle));
            }

            @Test
            @DisplayName("Reading from file gives correct incomeRegister back")
            void readingFromFileGivesCorrectIncomeRegisterBack() throws IOException {
                assertEquals(incomeRegister.toString(), FileHandling.readIncomeRegisterFromFile(fileTitle).toString());
            }
        }
        @Nested
        @DisplayName("FileHandling incomeRegister with multiple Income")
        class fileHandlingIncomeRegisterWithMultipleIncome{
            @BeforeEach
            void addIncomeToIncomeRegister(){
                Income income1 = new Income(59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 3));
                Income income2 = new Income("description2", 62.4f, true, IncomeCategory.GIFT, LocalDate.of(2021, Month.FEBRUARY, 1));
                Income income3 = new Income("description3", 9.81f, false, IncomeCategory.SALARY, LocalDate.of(2023, Month.JULY, 5));
                incomeRegister.addItem(income1);
                incomeRegister.addItem(income2);
                incomeRegister.addItem(income3);
            }
            @Test
            @DisplayName("Writing to file does not throw exception")
            void writingToFileDoesNotThrowException() {
                assertDoesNotThrow(()->FileHandling.writeItemRegisterToFile(incomeRegister, fileTitle));
            }

            @Test
            @DisplayName("Reading from file gives correct incomeRegister back")
            void readingFromFileGivesCorrectIncomeRegisterBack() throws IOException {
                assertEquals(incomeRegister.toString(), FileHandling.readIncomeRegisterFromFile(fileTitle).toString());
            }
        }
    }
    @Nested
    @DisplayName("FileHandling ExpenseRegister to file")
    class fileHandlingExpenseRegisterToFile{
        String fileTitle = "src/main/resources/testFiles/economics/expenseRegisterTest";
        @Nested
        @DisplayName("FileHandling ExpenseRegister with Expense that has description")
        class fileHandlingExpenseRegisterWithExpenseThatHasDescription{
            @BeforeEach
            void addExpenseToExpenseRegister(){
                Expense expense1 = new Expense("description", 59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
                expenseRegister.addItem(expense1);
            }
            @Test
            @DisplayName("Writing to file does not throw exception")
            void writingToFileDoesNotThrowException() {
                assertDoesNotThrow(()->FileHandling.writeItemRegisterToFile(expenseRegister, fileTitle));
            }

            @Test
            @DisplayName("Reading from file gives correct expenseRegister back")
            void readingFromFileGivesCorrectIncomeRegisterBack() throws IOException {
                assertEquals(expenseRegister.toString(), FileHandling.readExpenseRegisterFromFile(fileTitle).toString());
            }
        }
        @Nested
        @DisplayName("FileHandling ExpenseRegister with Expense that does not have description")
        class fileHandlingExpenseRegisterWithExpenseThatDoesNotHaveDescription{
            @BeforeEach
            void addExpenseToExpenseRegister(){
                Expense expense1 = new Expense(59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
                expenseRegister.addItem(expense1);
            }
            @Test
            @DisplayName("Writing to file does not throw exception")
            void writingToFileDoesNotThrowException() {
                assertDoesNotThrow(()->FileHandling.writeItemRegisterToFile(expenseRegister, fileTitle));
            }

            @Test
            @DisplayName("Reading from file gives correct expenseRegister back")
            void readingFromFileGivesCorrectIncomeRegisterBack() throws IOException {
                assertEquals(expenseRegister.toString(), FileHandling.readExpenseRegisterFromFile(fileTitle).toString());
            }
        }
        @Nested
        @DisplayName("FileHandling ExpenseRegister with multiple Expense")
        class fileHandlingExpenseRegisterWithMultipleExpensen{
            @BeforeEach
            void addExpenseToExpenseRegister(){
                Expense expense1 = new Expense(59.9f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.MARCH, 3));
                Expense expense2 = new Expense("description2", 62.4f, true, ExpenseCategory.FOOD, LocalDate.of(2021, Month.FEBRUARY, 1));
                Expense expense3 = new Expense("description3", 9.81f, false, ExpenseCategory.CLOTHES, LocalDate.of(2023, Month.JULY, 5));

                expenseRegister.addItem(expense1);
                expenseRegister.addItem(expense2);
                expenseRegister.addItem(expense3);
            }
            @Test
            @DisplayName("Writing to file does not throw exception")
            void writingToFileDoesNotThrowException() {
                assertDoesNotThrow(()->FileHandling.writeItemRegisterToFile(expenseRegister, fileTitle));
            }

            @Test
            @DisplayName("Reading from file gives correct expenseRegister back")
            void readingFromFileGivesCorrectIncomeRegisterBack() throws IOException {
                assertEquals(expenseRegister.toString(), FileHandling.readExpenseRegisterFromFile(fileTitle).toString());
            }
        }
    }
}
