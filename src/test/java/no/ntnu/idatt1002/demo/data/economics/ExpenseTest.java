package no.ntnu.idatt1002.demo.data.economics;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class ExpenseTest {

    @Test
    @DisplayName("The Expense constructor throws exceptions when it should.")
    void constructorThrows(){
        assertThrows(IllegalArgumentException.class, () -> new Expense("description", 8.5f, false, null, LocalDate.of(2023, Month.MARCH, 3)));
        assertThrows(IllegalArgumentException.class, () -> new Expense("description", -10.0f, false, ExpenseCategory.BOOKS, LocalDate.of(2023, Month.MARCH, 3)));
    }

    @Test
    @DisplayName("The Expense constructor does not throw exceptions when it should not.")
    void constructorDoesThrow() {
        assertDoesNotThrow(() -> new Expense("description", 59.9f, false,  ExpenseCategory.BOOKS, LocalDate.of(2023, Month.MARCH, 3)));
    }

}