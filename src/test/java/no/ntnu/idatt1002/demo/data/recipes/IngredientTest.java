package no.ntnu.idatt1002.demo.data.recipes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IngredientTest {

    Ingredient testIngredient;

    @BeforeEach
    void beforeEach() {
        testIngredient = new Ingredient(FoodItem.ONION, 0.5f, MeasuringUnit.KG);
    }

    @Nested
    @DisplayName("Test creation of an Ingredient")
    class testCreation{

        // Indirectly tests equals method.
        @Test
        @DisplayName("The constructor creates an ingredient object successfully")
        void constructValidIngredient() {
            Ingredient validIngredient = new Ingredient(FoodItem.ONION, 1, MeasuringUnit.KG);
            assertEquals(validIngredient, new Ingredient(FoodItem.ONION, 1, MeasuringUnit.KG));
        }

        @Test
        @DisplayName("The constructor throws exceptions for illegal input.")
        void constructorThrowsExceptionsWhenItShould() {
            assertThrows(IllegalArgumentException.class, () -> new Ingredient(null, 2, MeasuringUnit.DL));
            assertThrows(IllegalArgumentException.class, () -> new Ingredient(FoodItem.ONION, -5, MeasuringUnit.DL));
            assertThrows(IllegalArgumentException.class, () -> new Ingredient(FoodItem.ONION, 0, MeasuringUnit.DL));
            assertThrows(IllegalArgumentException.class, () -> new Ingredient(FoodItem.ONION, 2, null));
        }
    }

    @Nested
    @DisplayName("Test alternation of Ingredient object")
    class testAlternations{

        @Nested
        @DisplayName("Valid alternations")
        class testValidAlternations {
            @Test
            @DisplayName("Change of food type works for valid food type.")
            void setFoodTypeWorksForValidType() {
                testIngredient.setFoodType(FoodItem.LEMON);
                assertEquals(new Ingredient(FoodItem.LEMON, 0.5f, MeasuringUnit.KG), testIngredient);
            }

            @Test
            @DisplayName("Change of food amount works for valid amount.")
            void setAmountWorksForValidAmount() {
                testIngredient.setAmount(2.5);
                assertEquals(new Ingredient(FoodItem.ONION, 2.5f, MeasuringUnit.KG), testIngredient);
            }

            @Test
            @DisplayName("Change of measuring unit works for valid unit.")
            void setUnitWorksForValidUnit() {
                testIngredient.setUnit(MeasuringUnit.TBS);
                assertEquals(new Ingredient(FoodItem.ONION, 0.5f, MeasuringUnit.TBS), testIngredient);
            }


        }

        @Nested
        @DisplayName("Invalid alternations")
        class testInvalidAlternations {
            @Test
            @DisplayName("Change of food type to invalid type throws exception.")
            void setFoodTypeThrowsException() {
                assertThrows(IllegalArgumentException.class, () -> testIngredient.setFoodType(null));
            }

            @Test
            @DisplayName("Change of food amount to invalid amount throws exception.")
            void setAmountThrowsException() {
                assertThrows(IllegalArgumentException.class, () -> testIngredient.setAmount(0));
                assertThrows(IllegalArgumentException.class, () -> testIngredient.setAmount(-1));
            }

            @Test
            @DisplayName("Change of measuring to invalid unit throws exception.")
            void setUnitThrowsException() {
                assertThrows(IllegalArgumentException.class, () -> testIngredient.setUnit(null));
            }
        }

    }



//TODO: Test for equals method?

}