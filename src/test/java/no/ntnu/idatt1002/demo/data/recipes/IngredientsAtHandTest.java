package no.ntnu.idatt1002.demo.data.recipes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IngredientsAtHandTest {

    IngredientsAtHand ingredientsAtHand = new IngredientsAtHand();

    @BeforeEach
    void beforeEach() {
        ingredientsAtHand.addIngredient(FoodItem.LEMON);
        ingredientsAtHand.addIngredient(FoodItem.MILK);
        ingredientsAtHand.addIngredient(FoodItem.MINCED_MEAT);
    }


    @Nested
    @DisplayName("Test addition of food types to the collection.")
    class testAddIngredient {
        int numberOfIngredientsBefore;

        @BeforeEach
        void getNumberOfIngredientsAtHand() {
            numberOfIngredientsBefore = ingredientsAtHand.getIngredientsAtHand().size();
        }

        @Test
        @DisplayName("A valid food type is added successfully.")
        void addValidFoodType() {
            ingredientsAtHand.addIngredient(FoodItem.POTATO);
            assertEquals(numberOfIngredientsBefore+1, ingredientsAtHand.getIngredientsAtHand().size());
        }

        @Test
        @DisplayName("An invalid(null) food type is not added to the collection.")
        void notAddNullFoodType() {
            ingredientsAtHand.addIngredient(null);
            assertEquals(numberOfIngredientsBefore, ingredientsAtHand.getIngredientsAtHand().size());
        }

        @Test
        @DisplayName("An duplicate food type is not added to the collection.")
        void notAddDuplicateFoodType() {
            ingredientsAtHand.addIngredient(FoodItem.LEMON);
            assertEquals(numberOfIngredientsBefore, ingredientsAtHand.getIngredientsAtHand().size());
        }
    }


    @Nested
    @DisplayName("Test the atHand method of the ingredients at hand class. ")
    class testAtHandMethod {
        @Test
        @DisplayName("The atHand method returns true correctly when the food type is at hand.")
        void atHandReturnsTrueSuccessfully() {
            assertTrue(ingredientsAtHand.atHand(FoodItem.LEMON));
        }

        @Test
        @DisplayName("The atHand method returns false correctly when the food type is not at hand.")
        void atHandReturnsFalseSuccessfully() {
            assertFalse(ingredientsAtHand.atHand(FoodItem.YELLOW_CHEESE));
        }
    }

    @Nested
    @DisplayName("Test the removeIngredient method of the ingredients at hand class. ")
    class testRemoveIngredientMethod {
        @Test
        @DisplayName("Ingredient is removed successfully and true is returned.")
        void removeIngredientSuccessfully() {
            int ingredientsAtStart = ingredientsAtHand.getIngredientsAtHand().size();
            assertTrue(ingredientsAtHand.removeIngredient(FoodItem.LEMON));
            assertEquals(ingredientsAtStart-1, ingredientsAtHand.getIngredientsAtHand().size());
        }

        @Test
        @DisplayName("Removing ingredient that is not in the collection, leaves it unchanged and returns false.")
        void removeIngredientNotInCollection() {
            assertFalse(ingredientsAtHand.removeIngredient(FoodItem.SALSA_SAUCE));
        }
    }
}