package no.ntnu.idatt1002.demo.data.recipes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlerTest {

    RecipeRegister recipeRegister = new RecipeRegister();
    IngredientsAtHand ingredientsAtHand = new IngredientsAtHand();
    Recipe recipe1;
    ArrayList<RecipeIngredient> ingredientsOfRecipe1 = new ArrayList<>();
    String testPath = "src/main/resources/testFiles/recipes/";

    @BeforeEach
    void beforeEach() {
        recipe1 = new Recipe("Meat, cheese and potatoes", "Instructions");

        ingredientsOfRecipe1.add(new RecipeIngredient(FoodItem.MINCED_MEAT, 500, MeasuringUnit.GR));
        ingredientsOfRecipe1.add(new RecipeIngredient(FoodItem.POTATO, 750, MeasuringUnit.GR));
        ingredientsOfRecipe1.add(new RecipeIngredient(FoodItem.YELLOW_CHEESE, 2, MeasuringUnit.DL));
        recipe1.addIngredient(FoodItem.MINCED_MEAT, 500, MeasuringUnit.GR);
        recipe1.addIngredient(FoodItem.POTATO, 750, MeasuringUnit.GR);
        recipe1.addIngredient(FoodItem.YELLOW_CHEESE, 2, MeasuringUnit.DL);
    }

    @Nested
    @DisplayName("Test supporting methods for wring register to file.")
    class testFormattingClasses {

        @Test
        @DisplayName("Test that ingredients are correctly formatted before writing to file. ")
        void testWritingIngredients() {
            String correctFormat = "- MINCED_MEAT | 500.0 | GR\n- POTATO | 750.0 | GR\n- YELLOW_CHEESE | 2.0 | DL\n";
            assertEquals(correctFormat, String.valueOf(FileHandler.formatIngredientList(ingredientsOfRecipe1)));
        }

        @Test
        @DisplayName("Test behavior for empty ingredient list.")
        void testWritingEmptyIngredientList() {
            ArrayList<RecipeIngredient> emptyList = new ArrayList<>();
            assertEquals("", String.valueOf(FileHandler.formatIngredientList(emptyList)));
        }


        @Test
        @DisplayName("Test that recipes are formatted correctly before writing to file.")
        void testFormatRecipe() {

            String correctFormat = """
                    # Meat, cheese and potatoes
                    - MINCED_MEAT | 500.0 | GR
                    - POTATO | 750.0 | GR
                    - YELLOW_CHEESE | 2.0 | DL

                    Instructions

                    """;
            assertEquals(correctFormat, String.valueOf(FileHandler.formatRecipe(recipe1)));
        }

    }


    @Nested
    @DisplayName("Test writing a recipe register to file.")
    class testWritingRecipeRegister {

        @Test
        @DisplayName("Test that writing empty register to file creates blank file.")
        void writeEmptyRegister() {
            RecipeRegister emptyRegister = new RecipeRegister();
            assertAll(() -> FileHandler.writeRegister(emptyRegister, testPath + "emptyRegister"));
        }

        @Test
        @DisplayName("Test that writing null register to file throws exception.")
        void writeNullRegister() {
            assertThrows(IllegalArgumentException.class, () -> FileHandler.writeRegister(null, testPath + "noRegister"));
        }

        @Test
        @DisplayName("Write recipe register correctly to file as text.")
        void writeRecipeRegisterToFile() {
            assertAll(() -> FileHandler.writeRegister(recipeRegister, testPath + "RecipeRegister"));
        }
    }



    @Nested
    @DisplayName("Test reading a recipe register from file.")
    class testReadRecipeRegister {

        @Test
        @DisplayName("Read recipe register correctly from file to object.")
        void readRecipeRegisterFromFile() throws IOException {
            assertAll(() -> FileHandler.readRecipeRegister(testPath + "/testReadRegisterFromFile"));

            RecipeRegister registerReadFromTestFile = FileHandler.readRecipeRegister(testPath + "/testReadRegisterFromFile");
            assert registerReadFromTestFile != null;

            assertEquals(new RecipeIngredient(FoodItem.MINCED_MEAT, 500, MeasuringUnit.GR),
                    registerReadFromTestFile.getRecipe("Meat, cheese and potatoes").getIngredient(FoodItem.MINCED_MEAT));
            assertEquals(new RecipeIngredient(FoodItem.LEMON, 750, MeasuringUnit.GR),
                    registerReadFromTestFile.getRecipe("Meat, cheese and lemons").getIngredient(FoodItem.LEMON));
        }
    }



    @Nested
    @DisplayName("Test writing and reading an IngredientsAtHand object to and from file.")
    class testIngredientsAtHandStorage {

        @BeforeEach
        void setUpIngredientsAtHand() {
            ingredientsAtHand.addIngredient(FoodItem.POTATO);
            ingredientsAtHand.addIngredient(FoodItem.MILK);
            ingredientsAtHand.addIngredient(FoodItem.LEMON);
            ingredientsAtHand.addIngredient(FoodItem.MINCED_MEAT);
        }

        @Test
        @DisplayName("Write  empty ingredients at hand to file.")
        void writeEmptyIngredientsAtHandToFile() throws IOException {
            IngredientsAtHand emptyAtHand = new IngredientsAtHand();
            assertAll(() -> FileHandler.writeIngredientsAtHand(emptyAtHand, testPath +  "EmptyAtHandRegister"));
            assertEquals(0, Objects.requireNonNull(FileHandler.readIngredientsAtHand(testPath +  "EmptyAtHandRegister")).getIngredientsAtHand().size());

        }

        @Test
        @DisplayName("Write ingredients at hand to file.")
        void writeIngredientsAtHandToFile() {
            assertAll(() -> FileHandler.writeIngredientsAtHand(ingredientsAtHand, testPath + "AtHandRegister"));
        }

        @Test
        @DisplayName("Read ingredients at hand from file.")
        void readIngredientsAtHandFromFile() throws IOException {
            assertAll(() -> FileHandler.writeIngredientsAtHand(ingredientsAtHand, testPath + "AtHandRegister"));
            assertEquals(ingredientsAtHand.getIngredientsAtHand().size(),
                    Objects.requireNonNull(FileHandler.readIngredientsAtHand( testPath + "AtHandRegister")).getIngredientsAtHand().size());
        }
    }
}