package no.ntnu.idatt1002.demo.data.budget;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BudgetRegisterTest {

   @Nested
   @DisplayName("Test addBudgetName")
   class AddBudgetName {
     BudgetRegister budgetRegister;

     String name, nameCopy, blankName;

     @BeforeEach
     void setUp() {
       budgetRegister = new BudgetRegister();
       name = "Name";
       nameCopy = "Name";
       blankName = "";
     }

     @Test
     @DisplayName("Test adding valid name to budget register")
     void testAddingValidName() {
       budgetRegister.addBudgetName(name);
       assertEquals(1, budgetRegister.getBudgetNames().size());
     }

     @Test
     @DisplayName("Test adding blank name throws Illegal Argument exception")
     void testAddingBlankString() {
       assertThrows(IllegalArgumentException.class, () ->
           budgetRegister.addBudgetName(blankName));
     }

     @Test
     @DisplayName("Test adding null throws Illegal Argument exception")
     void testAddingNull() {
       assertThrows(IllegalArgumentException.class, () ->
           budgetRegister.addBudgetName(null));
     }

     @Test
     @DisplayName("Test adding duplicate name throws Illegal Argument exception")
     void testAddingDuplicate() {
       budgetRegister.addBudgetName(name);
       assertThrows(IllegalArgumentException.class, () ->
           budgetRegister.addBudgetName(nameCopy));
     }
   }

  @Nested
  @DisplayName("Test removeBudgetName")
  class RemoveBudgetName {

    BudgetRegister budgetRegister;

    String name, blankName;

    @BeforeEach
    void setUp() {
      budgetRegister = new BudgetRegister();
      name = "Name";
      blankName = "";
      budgetRegister.addBudgetName(name);
    }

    @Test
    @DisplayName("Test removing valid name from budget register")
    void testRemovingValidName() {
      budgetRegister.removeBudgetName(name);
      assertEquals(0, budgetRegister.getBudgetNames().size());
    }

    @Test
    @DisplayName("Test removing blank name throws Illegal Argument exception")
    void testRemovingBlankString() {
      assertThrows(IllegalArgumentException.class, () ->
          budgetRegister.removeBudgetName(blankName));
    }

    @Test
    @DisplayName("Test removing null throws Illegal Argument exception")
    void testRemovingNull() {
      assertThrows(IllegalArgumentException.class, () ->
          budgetRegister.removeBudgetName(null));
    }
  }

}
