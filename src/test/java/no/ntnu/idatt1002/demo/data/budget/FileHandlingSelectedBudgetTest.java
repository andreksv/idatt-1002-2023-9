package no.ntnu.idatt1002.demo.data.budget;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FileHandlingSelectedBudgetTest {

  @Nested
  @DisplayName("Test read/writeSelectedBudget")
  class ReadAndWriteSelectedBudget {

    String filePath, emptyFilePath, toString, budgetName1, budgetName2;

    @BeforeEach
    void setUp() {
      filePath = "src/main/resources/testFiles/budget/";
      emptyFilePath = filePath + "emptyFile";
      toString = "APRIL1";
      budgetName1 = "";
      budgetName2 = "APRIL1";
    }

    @Test
    @DisplayName("Test read and write to file")
    void testReadAndWriteToFile() throws IOException {
      System.out.println(filePath);
      budgetName1 = FileHandlingSelectedBudget.readSelectedBudget(filePath + "SelectedBudget");


      assertEquals(toString, budgetName1);

      FileHandlingSelectedBudget.updateSelectedBudget(budgetName2, filePath);
      budgetName2 = FileHandlingSelectedBudget.readSelectedBudget(filePath);

      assertEquals(budgetName1, budgetName2);
    }

    @Test
    @DisplayName("Test readSelectedBudget returns null for empty file")
    void testReadSelectedBudgetWithEmptyFile() throws IOException {
      assertNull(null, FileHandlingSelectedBudget.readSelectedBudget(emptyFilePath));
    }

    @Test
    @DisplayName("Test readSelectedBudget for non-existent file throws IOException")
    void testReadSelectedBudgetForNonExistentFile() {
      assertThrows(IOException.class,
          () -> FileHandlingSelectedBudget.readSelectedBudget("sheesh"));
    }
  }

  @Test
  @DisplayName("Test clearSelectedBudget")
  void testClearSelectedBudget() throws IOException {
    String filePath = "src/main/resources/testFiles/budget/fileForClearing";
    FileHandlingSelectedBudget.clearSelectedBudget(filePath);
    assertNull(FileHandlingSelectedBudget.readSelectedBudget(filePath));
  }

  @Nested
  @DisplayName("Test isSelectedBudgetEmpty")
  class testIsEmpty {

    String filePath;

    @BeforeEach
    void setUp() {
      filePath = "src/main/resources/testFiles/budget/emptyFile";
    }

    @Test
    @DisplayName("Test isSelectedBudget with non-empty file")
    void testIsSelectedBudgetWithNonEmptyFile() throws IOException {
      FileHandlingSelectedBudget.updateSelectedBudget("notempty", filePath);

      assertFalse(FileHandlingSelectedBudget.isSelectedBudgetEmpty(filePath));

      FileHandlingSelectedBudget.clearSelectedBudget(filePath);
    }

    @Test
    @DisplayName("Test isSelectedBudget with empty file")
    void testIsSelectedBudgetWithEmptyFile() throws IOException {
      assertTrue(FileHandlingSelectedBudget.isSelectedBudgetEmpty(filePath));
    }
  }

  @Nested
  @DisplayName("Test create budget directory")
  class CreateBudgetDirectory {

    String filePath;

    private static File directory;
    @BeforeAll
    public static void setup() {
      directory = new File("src/main/resources/testFiles/budget/testDirectory");
      if(!directory.exists()){
        directory.mkdir();
      }
    }

    @AfterAll
    public static void teardown() throws IOException {
      if(directory.exists()){
        FileUtils.deleteDirectory(directory);
      }
    }

    @BeforeEach
    void setUp() {
      filePath = "src/main/resources/testFiles/budget/testDirectory";
    }

    @Test
    @DisplayName("Test create existing then new budget directory")
    void testCreateExistingAndNewBudgetDirectory() {
      assertFalse(FileHandlingSelectedBudget.createBudgetDirectory(filePath));

      FileHandlingSelectedBudget.deleteBudgetDirectory(filePath);

      assertTrue(FileHandlingSelectedBudget.createBudgetDirectory(filePath));
    }

    @Test
    @DisplayName("Test create new and existing files")
    void testCreateNewAndExistingFiles() throws IOException {
      assertTrue(FileHandlingSelectedBudget.createNewBudgetFile(filePath, "Budget"));
      assertTrue(FileHandlingSelectedBudget.createNewIncomeFile(filePath, "Income"));
      assertTrue(FileHandlingSelectedBudget.createNewExpenseFile(filePath, "Expense"));

      assertFalse(FileHandlingSelectedBudget.createNewBudgetFile(filePath, "Budget"));
      assertFalse(FileHandlingSelectedBudget.createNewIncomeFile(filePath, "Income"));
      assertFalse(FileHandlingSelectedBudget.createNewExpenseFile(filePath, "Expense"));
    }
  }

  @Nested
  @DisplayName("Test delete budget directory")
  class DeleteBudgetDirectory {

    String filePath;

    private static File directory;

    @BeforeAll
    public static void setup() {
      directory = new File("src/main/resources/testFiles/budget/testDirectory");
      if (!directory.exists()) {
        directory.mkdir();
      }
    }

    @AfterAll
    public static void teardown() throws IOException {
      if (directory.exists()) {
        FileUtils.deleteDirectory(directory);
      }
    }

    @BeforeEach
    void setUp() {
      filePath = "src/main/resources/testFiles/budget/testDirectory";
    }

    @Test
    @DisplayName("Test remove existing budget directory")
    void testCreateExistingBudgetDirectory() {
      assertTrue(FileHandlingSelectedBudget.deleteBudgetDirectory(filePath));

      FileHandlingSelectedBudget.createBudgetDirectory(filePath);
    }
  }
}
