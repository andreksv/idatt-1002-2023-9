package no.ntnu.idatt1002.demo.data.economics;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class IncomeTest {

    @Test
    @DisplayName("The Income constructor throws exceptions when it should.")
    void constructorThrows(){
        assertThrows(IllegalArgumentException.class, () -> new Income("description", 8.5f, false, null, LocalDate.of(2023, Month.MARCH, 3)));
        assertThrows(IllegalArgumentException.class, () -> new Income("description", -10.0f, false, IncomeCategory.SALARY, LocalDate.of(2023, Month.MARCH, 3)));
    };

    @Test
    @DisplayName("The Income constructor does not throw exceptions when it should not.")
    void constructorDoesThrow() {
        assertDoesNotThrow(() -> new Income("description", 59.9f, false, IncomeCategory.SALARY, LocalDate.of(2023, Month.MARCH, 3)));
    }
}

/**
 *  package no.ntnu.idatt1002.demo.data.Economics;
 *
 * import no.ntnu.idatt1002.demo.data.Economics.Item;
 * import org.junit.jupiter.api.DisplayName;
 * import org.junit.jupiter.api.Test;
 *
 * import static org.junit.jupiter.api.Assertions.*;
 *
 * class ItemTest {
 *
 *     @Test
 *     @DisplayName("The Item constructor throws exceptions when it should.")
 *     void constructorThrows(){
 *         assertThrows(IllegalArgumentException.class, () -> new Item("description", 0f, false, "03.03.23"));
 *         assertThrows(IllegalArgumentException.class, () -> new Item("description", -10.0f, false, "03.03.23"));
 *         assertThrows(IllegalArgumentException.class, () -> new Item("description", -10.0f, false, ""));
 *     };
 *
 *     @Test
 *     @DisplayName("The Item constructor does not throw exceptions when it should not.")
 *     void constructorDoesThrow() {
 *         assertDoesNotThrow(() -> new Item("description", 59.9f, false, "03.03.23"));
 *     }
 *
 * }
 */