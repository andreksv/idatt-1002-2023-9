package no.ntnu.idatt1002.demo.data.budget;

import no.ntnu.idatt1002.demo.data.economics.ExpenseCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlingBudgetTest {
    BudgetItem foodItem;
    BudgetItem bookItem;
    BudgetItem clothesItem;

    @Nested
    @DisplayName("FileHandling budget with one BudgetItem does not throw exception")
    class fileHandlingBudgetWithOneBudgetItemDoesNotThrowException{
        GeneralBudget generalBudget = new GeneralBudget(1200);
        String fileTitle = "src/main/resources/testFiles/budget/oneBudgetItemTest";

        @BeforeEach
        void createGeneralBudget(){
            foodItem = new BudgetItem(500, "description", ExpenseCategory.FOOD);
            generalBudget.addToBudgetBudgetItem(foodItem);
        }
        @Test
        @DisplayName("Writing to file does not throw exception")
        void writeGeneralBudgetToFileDoesNotThrowException() throws IOException {
            assertDoesNotThrow(() -> FileHandlingBudget.writeGeneralBudgetToFile(fileTitle,generalBudget));
        }

        @Test
        @DisplayName("Reading from file gives correct budget back")
        void readingGeneralBudgetFromFileDoesNotThrowException()throws IOException{
            assertEquals(generalBudget,FileHandlingBudget.readGeneralBudgetFromFile(fileTitle));
        }
    }

    @Nested
    @DisplayName("FileHandling budget with multiple BudgetItems does not throw exception")
    class FileHandlingBudgetWithMultipleBudgetItemsDoesNotThrowException{
        GeneralBudget generalBudget = new GeneralBudget(1200);
        String fileTitle = "src/main/resources/testFiles/budget/multipleBudgetItem";
        @BeforeEach
        void createGeneralBudget(){
            foodItem = new BudgetItem(500, "description", ExpenseCategory.FOOD);
            bookItem = new BudgetItem(200, "anotherDescription", ExpenseCategory.BOOKS);
            clothesItem = new BudgetItem(150, "anotheranotherdescription", ExpenseCategory.CLOTHES);
            generalBudget.addToBudgetBudgetItem(foodItem);
            generalBudget.addToBudgetBudgetItem(bookItem);
            generalBudget.addToBudgetBudgetItem(clothesItem);
        }
        @Test
        @DisplayName("Writing to file does not throw exception")
        void writeGeneralBudgetToFileDoesNotThrowException() throws IOException {
            assertDoesNotThrow(()->FileHandlingBudget.writeGeneralBudgetToFile(fileTitle,generalBudget));
        }

        @Test
        @DisplayName("Reading from file gives correct budget back")
        void readingGeneralBudgetFromFileDoesNotThrowException()throws IOException{
            assertEquals(generalBudget,FileHandlingBudget.readGeneralBudgetFromFile(fileTitle));
        }
    }

    @Nested
    @DisplayName("Test isEmpty")
    class IsEmpty {

        String emptyFileFilePath = "src/main/resources/testFiles/budget/emptyFile";
        String nonEmptyFileFilePath = "src/main/resources/testFiles/budget/multipleBudgetItem";

        @Test
        @DisplayName("Test isEmpty with empty file")
        void testIsEmptyWithEmptyFile() throws IOException {
            assertTrue(FileHandlingBudget.isEmpty(emptyFileFilePath));
        }

        @Test
        @DisplayName("Test isEmpty with non-empty file")
        void testIsEmptyWithNonEmptyFile() throws IOException {
            assertFalse(FileHandlingBudget.isEmpty(nonEmptyFileFilePath));
        }
    }

    @Nested
    @DisplayName("Test isNewBudget")
    class IsNewBudget {

        String newBudgetFilePath = "src/main/resources/testFiles/budget/newBudget";

        String oldBudgetFilePath = "src/main/resources/testFiles/budget/oneBudgetItemTest";


        @Test
        @DisplayName("Test isNewBudget with new budget")
        void testIsNewBudgetWithNewBudget() throws IOException {
            assertTrue(FileHandlingBudget.isNewBudget(newBudgetFilePath));
        }

        @Test
        @DisplayName("Test isNewBudget with old budget")
        void testIsNewBudgetWithOldBudget() throws IOException {
            assertFalse(FileHandlingBudget.isNewBudget(oldBudgetFilePath));
        }
    }

    @Test
    @DisplayName("Test writing max budget amount to file")
    void testWriteMaxMountToFile() throws IOException {
        String fileDestination = "src/main/resources/testFiles/budget/writeNewBudget";
        String maxAmount = "100";
        FileHandlingBudget.writeMaxAmountToFile(fileDestination, maxAmount);

        GeneralBudget generalBudget = FileHandlingBudget.readGeneralBudgetFromFile(fileDestination);

        assertEquals(Double.parseDouble(maxAmount), generalBudget.getMaxAmount());
    }
}
