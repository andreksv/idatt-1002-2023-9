package no.ntnu.idatt1002.demo.data.budget;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
public class SavingsTest {
    SavingsGoal savingsGoal1;
    SavingsGoal savingsGoal2;
    SavingsGoal savingsGoal3;
    Savings savings;
    @BeforeEach
    void addSavingsGoalToSavings() {
        savingsGoal1 = new SavingsGoal("", 50, 20);
        savingsGoal2 = new SavingsGoal("", 75, 50);
        savingsGoal3 = new SavingsGoal("", 20, 100);

        savings = new Savings(100);
        savings.addSavingsGoal(savingsGoal1);
        savings.addSavingsGoal(savingsGoal2);
        savings.addSavingsGoal(savingsGoal3);
    }
    @Test
    void getTotalSavingsGivesExpectedAmount() {
        assertEquals(245, savings.getTotalSavings());
    }

    @Test
    void getAchievedSavingsGoalGivesExpected() {
        List<SavingsGoal> savingsGoalsAchieved = new ArrayList<>();
        savingsGoalsAchieved.add(savingsGoal1);
        savingsGoalsAchieved.add(savingsGoal2);

        assertEquals(savingsGoalsAchieved, savings.getAchievedSavingsGoal());
    }

    @Test
    void addToSavingsGoalThrowsExceptionWhenTryingToAddMoreThanIsAvailable() {
        assertThrows(IllegalArgumentException.class, () -> savings.addToSavingsGoal(1000, savingsGoal1));
    }

    @Test
    void addToSavingsGoalThrowsExceptionWhenTryingToAddToASavingGoalThatIsNotAddedToSavings() {
        assertThrows(IllegalArgumentException.class, () -> savings.addToSavingsGoal(0, new SavingsGoal("not exist",50)));
    }
}

