package no.ntnu.idatt1002.demo.data.budget;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SavingsGoalTest {

    @Test
    void constructorSetsAmountToZeroIfAmountInvestedIsNegative() {
        SavingsGoal savingsGoal = new SavingsGoal("",-50,0);
        assertEquals(0, savingsGoal.getAmountInvested());
    }

    @Test
    void constructorSetsAmountToZeroIfAmountGoalIsNegative() {
        SavingsGoal savingsGoal = new SavingsGoal("",0,-50);
        assertEquals(0, savingsGoal.getAmountGoal());
    }

    @Test
    void setAmountGoalWillSetToZeroIfItGetsANegativeValue() {
        SavingsGoal savingsGoal = new SavingsGoal("",50);
        savingsGoal.setAmountGoal(-50);
        assertEquals(0, savingsGoal.getAmountGoal());
    }

    @Nested
    class testIfSavingsGoalIsAchievedWhenItsSupposedTo{
        SavingsGoal savingsGoalNotAchieved;
        SavingsGoal savingsGoalAchieved;
        @BeforeEach
        void createSavingsGoals() {
            savingsGoalNotAchieved = new SavingsGoal("", 50, 100);
            savingsGoalAchieved = new SavingsGoal("", 100, 50);
        }


        @Test
        void savingsGoalIsAchievedWhenItsSupposedTo() {
            assertTrue(savingsGoalAchieved.isAchieved());
        }

        @Test
        void savingsGoalIsNotAchievedWhenItsSupposedTo() {
            assertFalse(savingsGoalNotAchieved.isAchieved());
        }

        @Test
        void addAmountInvestedSoItsHigherThanAmountGoalWillMakeItAchieved() {
            savingsGoalNotAchieved.addAmountInvested(100);
            assertTrue(savingsGoalNotAchieved.isAchieved());
        }

        @Test
        void setAmountGoalLowerThanAmountInvestedWillMakeItAchieved() {
            savingsGoalNotAchieved.setAmountGoal(25);
            assertTrue(savingsGoalNotAchieved.isAchieved());
        }

        @Test
        void addAmountInvestedSoItsLowerThanAmountGoalWillMakeItNotAchieved() {
            savingsGoalAchieved.addAmountInvested(-75);
            assertFalse(savingsGoalAchieved.isAchieved());
        }

        @Test
        void setAmountGoalHigherThanAmountInvestedWillMakeItNotAchieved() {
            savingsGoalAchieved.setAmountGoal(150);
            assertFalse(savingsGoalAchieved.isAchieved());
        }

    }
}
