package no.ntnu.idatt1002.demo.data.economics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;

import static org.junit.jupiter.api.Assertions.*;

public class IncomeRegisterTest {
    IncomeRegister incomeRegister = new IncomeRegister();

    @Nested
    @DisplayName("Test addItem")
    class testAddItem{
        @Test
        @DisplayName("addItem method throws exception when it should")
        void addItemThrows() {
            Income income = new Income("description", 59.9f, false, IncomeCategory.SALARY, LocalDate.of(2023, Month.MARCH, 3));
            incomeRegister.addItem(income);
            assertThrows(IllegalArgumentException.class, () -> incomeRegister.addItem(income));
        }

        @Test
        @DisplayName("addItem method does not throw exception when it should not")
        void addItemDoesNotThrow() {
            Income income1 = new Income("description", 59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 3));
            Income income2 = new Income("anotherDescription", 6.5f, true, IncomeCategory.SALARY, LocalDate.of(2023, Month.MARCH, 2));
            assertDoesNotThrow(() -> incomeRegister.addItem(income1));
            assertDoesNotThrow(() -> incomeRegister.addItem(income2));
        }
    }

    @Nested
    @DisplayName("Test removeItem")
    class testRemoveItem{
        @Test
        @DisplayName("removeItem does throw exception when it should")
        void removeItemDoesThrow(){
            Income income1 = new Income("description", 59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 3));
            Income notIncome1 = new Income("anotherDescription", 6.5f, true, IncomeCategory.SALARY, LocalDate.of(2023, Month.MARCH, 2));
            incomeRegister.addItem(income1);
            assertThrows(IllegalArgumentException.class, () -> incomeRegister.removeItem(notIncome1));
        }
        @Test
        @DisplayName("removeItem method does not throw exception when it should not")
        void removeItemDoesNotThrow(){
            Income income1 = new Income("description", 59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 2));
            Income income1Copy = new Income("description", 59.9f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.MARCH, 2));
            incomeRegister.addItem(income1);
            assertDoesNotThrow(() -> incomeRegister.removeItem(income1Copy));
        }

    }
    @Nested
    @DisplayName("Test getTotalSum and getIncomeByCategory")
    class testGetTotalSumAndGetIncomeByCategoryMethods {
        Income income1;
        Income income2;
        Income income3;
        Income income4;

        @BeforeEach
        void addIncomeToRegister() {
            income1 = new Income("description1", 59.9f, false, IncomeCategory.SALARY, LocalDate.of(2023, Month.MARCH, 3));
            income2 = new Income("description2", 62.4f, true, IncomeCategory.GIFT,LocalDate.of(2021, Month.FEBRUARY, 1));
            income3 = new Income("description3", 9.81f, false, IncomeCategory.SALARY, LocalDate.of(2023, Month.JULY, 5));
            income4 = new Income("description3", 100.81f, false, IncomeCategory.GIFT, LocalDate.of(2023, Month.JULY, 12));
            incomeRegister.addItem(income1);
            incomeRegister.addItem(income2);
            incomeRegister.addItem(income3);
            incomeRegister.addItem(income4);
        }

        @Test
        @DisplayName("getTotalSum method gives correct amount")
        void getTotalSumCorrectAmount() {
            double totalIncome = 59.9f + 62.4f + 9.81f + 100.81f;
            assertEquals(Math.round(incomeRegister.getTotalSum()), Math.round(totalIncome));
        }

        @Test
        @DisplayName("getItemsByCategory gives expected Income back")
        void getIncomeByCategoryGivesCorrectIncome() {
            IncomeRegister incomeSalary = incomeRegister.getIncomeByCategory(IncomeCategory.SALARY);
            assertTrue(incomeSalary.getItems().contains(income1));
            assertTrue(incomeSalary.getItems().contains(income3));
            assertFalse(incomeSalary.getItems().contains(income4));
        }

        @Test
        @DisplayName("getItemsByMonth gives expected Income back")
        void getIncomeByMonthGivesCorrectIncomes() {
            YearMonth yearMonth = YearMonth.of(2023, 7);

            IncomeRegister incomeJuly = incomeRegister.getIncomeByMonth(yearMonth);
            assertTrue(incomeJuly.getItems().contains(income4));
            assertTrue(incomeJuly.getItems().contains(income3));
            assertFalse(incomeJuly.getItems().contains(income2));
        }
    }
}

